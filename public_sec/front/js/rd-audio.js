/*
 *  RD-Audio - v0.2
 *  Easy as hell Audio Player Jquery plugin.
 *
 *  Made by Rafael Shayvolodyan (raffa)
 *
 *  Under MIT License
 */

;
(function ($) {
    'use strict'

    var def_settings = {
        playlistClass: 'playlist',
        fixed: true,
        fixedClass: 'fixed-player',
        fixedBG: '#000',
        nav: true,
        navClass: ['fa fa-backward', 'fa fa-forward'],
        showTitle: true
    }

    $.fn.rdAudio = function (settings) {

        settings = $.extend(true, {}, def_settings, settings);
        var fixedCnt = $('.' + settings.fixedClass);
        var players = audiojs.createAll({
            trackEnded: function () {

                var active;
                for (i in players) {
                    if ($('.' + settings.playlistClass + ' li.playing').parent().data('player-id') == $(players[i].wrapper).find('audio').attr('id')) {
                        active = i;
                    }
                }

                var activeTrack = $('.' + settings.playlistClass + '[data-player-id="' + $(players[active].wrapper).find('audio').attr('id') + '"] li.playing');

                var next = activeTrack.next();
                if (!next.length) next = activeTrack.parent().find('li').first();
                next.addClass('playing').siblings().removeClass('playing');
                players[active].load(next.attr('data-src'));
                players[active].play();
            }
        });

        if (settings.showTitle) {
            $('.audiojs .play-pause').before('<div class="rdaudio-title"></div>');
            var title = $('.rdaudio-title');
        }

        function changeTitle(name) {
            title.html(name);
        }


        if (settings.nav) {
            $('.audiojs .play-pause').after('<div class="rdaudio-nav"><i class="rdaudio-prev ' + settings.navClass[0] + '"></i><i class="rdaudio-next ' + settings.navClass[1] + '"></i></div>');
            $('.rdaudio-prev').click(function () {
                var curr;
                var prev;
                var status;
                if ($('.' + settings.playlistClass + ' .playing').length > 0) {
                    curr = $('.' + settings.playlistClass).find('.playing');
                    status = 1;
                } else if ($('.' + settings.playlistClass + ' .stopped').length > 0) {
                    curr = $('.' + settings.playlistClass).find('.stopped');
                    status = 0;
                }
                curr.prev().length ? prev = curr.prev() : prev = curr;
                if (status == 1) {
                    curr.removeClass('playing');
                    prev.addClass('playing');
                } else {
                    curr.removeClass('stopped');
                    prev.addClass('stopped');
                }
                changeTitle(prev.find('.name').text());

                for (var i = 0; i < players.length; i++) {
                    players[i].pause();
                    players[i].load(prev.attr('data-src'));
                    if (status == 1) {
                        players[i].play();
                    }
                }
            });

            $('.rdaudio-next').click(function () {
                var curr;
                var next;
                var status;
                if ($('.' + settings.playlistClass + ' .playing').length > 0) {
                    curr = $('.' + settings.playlistClass).find('.playing');
                    status = 1;
                } else if ($('.' + settings.playlistClass + ' .stopped').length > 0) {
                    curr = $('.' + settings.playlistClass).find('.stopped');
                    status = 0;
                }
                curr.next().length ? next = curr.next() : next = curr;
                if (status == 1) {
                    curr.removeClass('playing');
                    next.addClass('playing');
                } else {
                    curr.removeClass('stopped');
                    next.addClass('stopped');
                }
                changeTitle(next.find('.name').text());

                for (var i = 0; i < players.length; i++) {
                    players[i].pause();
                    players[i].load(next.attr('data-src'));
                    if (status == 1) {
                        players[i].play();
                    }
                }
            });
        }

        if ($('.' + settings.playlistClass).length > 0) {
            for (var i = 0; i < players.length; i++) {
                var first = $("." + settings.playlistClass + "[data-player-id='" + $(players[i].wrapper).find('audio').attr('id') + "'] li").first();
                first.addClass('stopped');
                changeTitle(first.find('.name').text());
                players[i].load(first.data('src'));
            }
        }


        $('.play-pause').click(function (i) {
            var playerId = $(this).parent().find('audio').attr('id');
            var player;

            for (var i in players) {
                if (playerId == $(players[i].wrapper).find('audio').attr('id')) {
                    player = i;
                }
            }

            if ($(this).parent().hasClass('playing')) {
                $('.' + settings.playlistClass + '[data-player-id="' + playerId + '"] li.stopped').addClass('playing').removeClass('stopped');
            }
            else {
                $('.' + settings.playlistClass + '[data-player-id="' + playerId + '"] li.playing').addClass('stopped').removeClass('playing');
            }

            for (i in players) {
                if (playerId != $(players[i].wrapper).find('audio').attr('id')) {
                    $('.' + settings.playlistClass + '[data-player-id="' + $(players[i].wrapper).find('audio').attr('id') + '"] li.playing').removeClass('playing').addClass('stopped');
                    players[i].pause();
                }
            }
        });


        $('.' + settings.playlistClass + ' li').click(function (e) {
            var id = $(this).parent().data('player-id');
            var player;
            var existPlayer = false;

            var target = $(e.target);
            if (target.is("a")) {
                window.location = $(target, this).attr("href");
                return false;
            }

            for (var i in players) {
                if (id == $(players[i].wrapper).find('audio').attr('id')) {
                    if ($(this).hasClass('playing')) {
                        players[i].playPause();
                        $(this).removeClass('playing').addClass('stopped');
                        return;
                    } else {
                        if ($(this).hasClass('stopped')) {
                            players[i].playPause();
                            $(this).removeClass('stopped').addClass('playing');
                            return;
                        }
                    }
                }
            }


            $('.' + settings.playlistClass + '[data-player-id="' + id + '"] li.stopped').removeClass('stopped');
            for (var i in players) {
                if (id != $(players[i].wrapper).find('audio').attr('id')) {
                    $('.' + settings.playlistClass + '[data-player-id="' + $(players[i].wrapper).find('audio').attr('id') + '"] li.playing').removeClass('playing').addClass('stopped');
                }
            }


            for (i in players) {
                if (id == $(players[i].wrapper).find('audio').attr('id')) {
                    existPlayer = true;
                }
            }

            if (existPlayer) {
                for (i in players) {
                    if (id == $(players[i].wrapper).find('audio').attr('id')) {
                        $('.' + settings.playlistClass + ' li').removeClass('playing');
                        $(this).addClass('playing').siblings().removeClass('playing');
                        changeTitle($(this).find('.name').text());
                        players[i].load($(this).attr('data-src'));
                        players[i].play();
                    }
                    else {
                        players[i].pause();
                    }
                }
            }

            if (settings.fixed) {
                fixedCnt.fadeIn(500);
            }
        });

        if (settings.fixed) {
            fixedCnt.css({
                'display': 'none',
                'position': 'fixed',
                'bottom': '0',
                'left': '0',
                'right': '0',
                'background': settings.fixedBG
            });
            fixedCnt.append('<span class="close-button fa fa-times"></span>');
            $('.close-button').click(function (e) {
                e.preventDefault();
                for (var i in players) {
                    players[i].pause();
                    $('.' + settings.playlistClass + ' li.playing').removeClass('playing');
                    $('.' + settings.playlistClass + ' li.stopped').removeClass('stopped');
                }

                fixedCnt.fadeOut(300);
            })
        }
    }
})(jQuery);

