<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => 'api_login'], function () {
    Route::get('/MembersList/', 'ListMembers@index');

});
Route::get('/AttractionAPI', ['as'=>'AttractionAPI','uses'=>'FrontendController@AttractionAPI']);
Route::post('/MoreAttractionAPI', ['as'=>'MoreAttractionAPI','uses'=>'FrontendController@MoreAttractionAPI']);
Route::get('/PropertyMobile', ['as'=>'PropertyMobile','uses'=>'BackendController@PropertyMobile']);
Route::post('/PropertyRange', ['as'=>'PropertyRange','uses'=>'BackendController@PropertyRange']);
Route::post('/PropertyMore', ['as'=>'PropertyMore','uses'=>'BackendController@PropertyMore']);
Route::post('/PropertyGallery', ['as'=>'PropertyGallery','uses'=>'BackendController@PropertyGallery']);
//Route::get('/AttractionAPI', ['as' => 'AttractionAPI', 'uses' => 'FrontendController@AttractionAPI']);
Route::get('/TestUssd', ['as'=>'TestUssd','uses'=>'BackendController@USSDTest']);
//Route::get('/USSDTestV2', ['as'=>'USSDTestV2','uses'=>'BackendController@USSDTestV2']);
Route::post('/USSDTestV2', ['as'=>'USSDTestV2','uses'=>'BackendController@USSDTestV2']);
Route::get('/FilterDates', ['as'=>'FilterDates','uses'=>'BackendController@FilterDates']);
Route::get('/CheckVotes', ['as'=>'Home','uses'=>'FrontendController@Home']);
Route::post('/VoteNowNumberTest',['as'=>'VoteNowNumberTest','uses'=>'FrontendController@VoteNowNumberTest']);
Route::post('/VoteNowNumber',['as'=>'VoteNowNumber','uses'=>'FrontendController@VoteNowNumber']);
Route::post('/CallBackMomo',['as'=>'CallBackMomo','uses'=>'FrontendController@CallBackMomo']);
Route::post('/VoteNowNumberUnlimited',['as'=>'VoteNowNumberUnlimited','uses'=>'FrontendController@VoteNowNumberUnlimited']);
Route::post('/VoteNowCard',['as'=>'VoteNowCard','uses'=>'FrontendController@VoteNowCard']);

Route::post('/AllSongsAjax',['as'=>'AllSongsAjax','uses'=>'BackendController@AllSongsAjax']);
Route::post('/VotingStatus',['as'=>'VotingStatus','uses'=>'BackendController@VotingStatus']);
Route::post('/CommentStatus',['as'=>'CommentStatus','uses'=>'BackendController@CommentStatus']);
Route::post('/DonatePayment',['as'=>'DonatePayment','uses'=>'FrontendController@DonatePayment']);
Route::post('/ContestantPayment',['as'=>'ContestantPayment','uses'=>'FrontendController@ContestantPayment']);
Route::post('/CallBackFundMomo',['as'=>'CallBackFundMomo','uses'=>'FrontendController@CallBackFundMomo']);
Route::post('/CompetitionCallBackMomo',['as'=>'CompetitionCallBackMomo','uses'=>'FrontendController@CompetitionCallBackMomo']);
Route::post('/RefreshMomo',['as'=>'RefreshMomo','uses'=>'BackendController@RefreshMomo']);
Route::post('/UpdateVotingStatus',['as'=>'UpdateVotingStatus','uses'=>'BackendController@UpdateVotingStatus']);

//Route::post('HospitalDashboardStatus', 'TestingController@HospitalDashboardStatus');
Route::post('SearchSong', ['as'=>'SearchSong','uses'=>'BackendController@SearchSong']);
Route::post('/PromoteStatus', ['as' => 'backend.PromoteStatus', 'uses' => 'BackendController@PromoteStatus']);
Route::post('/CallBackPromotedMomo',['as'=>'backend.CallBackPromotedMomo','uses'=>'BackendController@CallBackPromotedMomo']);
Route::post('/ChartsData',['as'=>'ChartsData','uses'=>'FrontendController@ChartsData']);