<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/login', function () {
//    return view('auth.login');
//});

Route::get('/login', ['as' => 'login', 'uses' => 'BackendController@ArtistLogin']);

Route::get('/HomeV2',['as'=>'HomeV2','uses'=>'FrontendController@HomeV2']);
Route::get('/CheckCron',['as'=>'CheckCron','uses'=>'FrontendController@CheckCron']);
Route::get('/CheckCronV2',['as'=>'CheckCronV2','uses'=>'FrontendController@CheckCronV2']);

Route::get('/TestNUmber',['as'=>'TestNUmber','uses'=>'FrontendController@TestNUmber']);
Route::get('/Discover',['as'=>'Discover','uses'=>'FrontendController@Discover']);
Route::get('/VoteNowV2',['as'=>'VoteNowV2','uses'=>'FrontendController@VoteNowV2']);
Route::get('/Charts',['as'=>'Charts','uses'=>'FrontendController@Charts']);
Route::get('/Broadcast',['as'=>'Broadcast','uses'=>'FrontendController@BroadcastV2']);
Route::get('/BroadcastV2',['as'=>'V2.BroadCastV2','uses'=>'FrontendController@BroadCastV2']);
Route::post('/AddBroadcastComment',['as'=>'V2.AddBroadcastComment','uses'=>'FrontendController@AddBroadcastComment']);
Route::get('/StakeHolders',['as'=>'StakeHolders','uses'=>'FrontendController@StakeHolders']);
Route::get('/StakeHoldersProfile',['as'=>'StakeHoldersProfile','uses'=>'FrontendController@StakeHoldersProfile']);
Route::get('/StakeHoldersProfileV2',['as'=>'StakeHoldersProfileV2','uses'=>'FrontendController@StakeHoldersProfileV2']);
Route::get('/StakeHoldersRegister',['as'=>'StakeHoldersRegister','uses'=>'FrontendController@StakeHoldersRegister']);
Route::get('/StakeHoldersThankYou',['as'=>'StakeHoldersThankYou','uses'=>'FrontendController@StakeHoldersThankYou']);
Route::get('/StakeHoldersLogin',['as'=>'StakeHoldersLogin','uses'=>'FrontendController@StakeHoldersLogin']);

Route::get('/Fundraising',['as'=>'Fundraising','uses'=>'FrontendController@Fundraising']);
Route::get('/FundraisingMore',['as'=>'FundraisingMore','uses'=>'FrontendController@FundraisingMore']);
Route::get('/Donate',['as'=>'V2.Donate','uses'=>'FrontendController@Donate']);
Route::get('/TermsAndConditions',['as'=>'V2.TermsAndConditions','uses'=>'FrontendController@TermsAndConditions']);
Route::get('/Faq',['as'=>'V2.Faq','uses'=>'FrontendController@Faq']);
Route::get('/SalesAndMarketing',['as'=>'V2.SalesAndMarketing','uses'=>'FrontendController@SalesAndMarketing']);
Route::get('/TermsAndConditionsStakeHolders',['as'=>'V2.TermsAndConditionsStakeHolders','uses'=>'FrontendController@TermsAndConditionsStakeHolders']);

Route::get('/Competitions',['as'=>'V2.Competitions','uses'=>'FrontendController@Competitions']);
Route::get('/Competition_Groups',['as'=>'V2.Groups','uses'=>'FrontendController@Groups']);
Route::get('/Competition_Contestants',['as'=>'V2.Contestants','uses'=>'FrontendController@Contestants']);
Route::get('/ContestantVote',['as'=>'V2.ContestantVote','uses'=>'FrontendController@ContestantVote']);
//Route::get('/NewContestant',['as'=>'V2.NewContestant','uses'=>'FrontendController@NewContestant']);





Route::get('/',['as'=>'welcome','uses'=>'FrontendController@HomeV2']);
Route::get('/AboutUs',['as'=>'V2.AboutUs','uses'=>'FrontendController@AboutUs']);

//Route::get('/Umuhanzi',['as'=>'Umuhanzi','uses'=>'FrontendController@Umuhanzi']);
Route::get('/Project',['as'=>'Project','uses'=>'FrontendController@Project']);
//Route::get('/TermsandConditions',['as'=>'TermsandConditions','uses'=>'FrontendController@TermsandConditions']);
Route::get('/PrivacyPolicy',['as'=>'V2.PrivacyPolicy','uses'=>'FrontendController@PrivacyPolicy']);
//Route::get('/Faq',['as'=>'Faq','uses'=>'FrontendController@Faq']);
//Route::get('/google8cfbd5f33b23c417.html',['as'=>'google8cfbd5f33b23c417.html','uses' =>'FrontendController@Google']);
Route::get('/googleede4b21874c85b08.html',['as'=>'googleede4b21874c85b08.html','uses' =>'FrontendController@Google']);
Route::get('/sitemap.txt',['as'=>'sitemap.txt','uses' =>'FrontendController@SiteMap']);
//Route::get('/AbahanziTermsandConditions',['as'=>'AbahanziTermsandConditions','uses'=>'FrontendController@AbahanziTermsandConditions']);
Route::get('/.well-known/pki-validation/godaddy.html',['as'=>'.well-known/pki-validation/godaddy.html','uses' =>'FrontendController@Godaddy']);

Route::get('/Urutonde',['as'=>'Urutonde','uses'=>'FrontendController@Urutonde']);
//Route::get('/Archives',['as'=>'Archives','uses'=>'FrontendController@Archives']);
Route::get('/Archives',['as'=>'V2.Archives','uses'=>'FrontendController@Archives']);
Route::get('/TestTime',['as'=>'V2.TestTime','uses'=>'FrontendController@TestTime']);
//Route::get('/ListArchives',['as'=>'ListArchives','uses'=>'FrontendController@ListArchives']);
Route::get('/ListArchives',['as'=>'V2.ListArchives','uses'=>'FrontendController@ListArchives']);
Route::get('/Nominee',['as'=>'Nominee','uses'=>'FrontendController@Nominee']);
//Route::get('/VoteNow',['as'=>'VoteNow','uses'=>'FrontendController@VoteNow']);
Route::post('/VoteNowNumber',['as'=>'VoteNowNumber','uses'=>'FrontendController@VoteNowNumber']);
Route::get('/VotePayError',['as'=>'VotePayError','uses'=>'FrontendController@VotePayError']);
Route::get('/VotePayError',['as'=>'VotePayError','uses'=>'FrontendController@VotePayError']);
//Route::get('/Thankyou', ['as'=>'Thankyou','uses'=>'FrontendController@Thankyou']);
Route::get('/ThankYou', ['as'=>'V2.ThankYou','uses'=>'FrontendController@ThankYou']);
Route::get('/FundsThankYou', ['as'=>'FundsThankYou','uses'=>'FrontendController@FundsThankYou']);
Route::get('/CompetitionThankYou', ['as'=>'CompetitionThankYou','uses'=>'FrontendController@CompetitionThankYou']);

Route::get('/ContactUs', ['as'=>'ContactUs','uses'=>'FrontendController@ContactUs']);
Route::get('/Momo', ['as'=>'Momo','uses'=>'BackendController@Momo']);
Route::get('/CheckStatus', ['as'=>'CheckStatus','uses'=>'BackendController@CheckStatus']);
Route::post('/AddNominee', ['as'=>'AddNominee','uses'=>'FrontendController@AddNominee']);
Route::post('/SearchNomineeG', ['as'=>'SearchNomineeG','uses'=>'FrontendController@SearchNomineeG']);
Route::post('/SearchNomineeM', ['as'=>'SearchNomineeM','uses'=>'FrontendController@SearchNomineeM']);
Route::post('/SearchNomineeT', ['as'=>'SearchNomineeT','uses'=>'FrontendController@SearchNomineeT']);

Route::post('/SearchVotingG', ['as'=>'SearchVotingG','uses'=>'FrontendController@SearchVotingG']);
Route::post('/SearchVotingT', ['as'=>'SearchVotingT','uses'=>'FrontendController@SearchVotingT']);

//Route::get('/ArtistRegistration', ['as'=>'ArtistRegistration','uses'=>'FrontendController@ArtistRegistration']);
//Route::post('/AddArtistRegistration', ['as'=>'AddArtistRegistration','uses'=>'FrontendController@AddArtistRegistration']);
Route::get('/RegistrationThankyou', ['as'=>'RegistrationThankyou','uses'=>'FrontendController@RegistrationThankyou']);
//Route::get('/ArtistLogin', ['as' => 'ArtistLogin', 'uses' => 'BackendController@ArtistLogin']);
Route::get('/ForgetPassword', ['as' => 'ForgetPassword', 'uses' => 'FrontendController@ForgetPassword']);
Route::post('/ForgetPasswordCheck', ['as' => 'ForgetPasswordCheck', 'uses' => 'FrontendController@ForgetPasswordCheck']);
Route::get('/ForgetPasswordEmail', ['as' => 'ForgetPasswordEmail', 'uses' => 'FrontendController@ForgetPasswordEmail']);
Route::post('/NewPassword', ['as' => 'NewPassword', 'uses' => 'FrontendController@NewPassword']);
Route::get('/ForgetPasswordReset', ['as' => 'ForgetPasswordReset', 'uses' => 'FrontendController@ForgetPasswordReset']);
Route::post('/SignIn_', ['as' => 'SignIn_', 'uses' => 'BackendController@SignIn_']);
Route::get('/testdates', ['as' => 'testdates', 'uses' => 'FrontendController@testdates']);

//Route::get('/AttractionAPI', ['as' => 'AttractionAPI', 'uses' => 'FrontendController@AttractionAPI']);
Route::get('/AttractionAPI', ['as'=>'AttractionAPI','uses'=>'FrontendController@AttractionAPI']);
Route::get('/Test', ['as'=>'Test','uses'=>'FrontendController@AttractionAPI']);
Route::get('/TestUssd', ['as'=>'TestUssd','uses'=>'BackendController@USSDTest']);
Route::get('/TestEmail', ['as'=>'TestEmail','uses'=>'BackendController@TestEmail']);
Route::get('/FilterDates', ['as'=>'FilterDates','uses'=>'BackendController@FilterDates']);
Route::get('/CheckPendingVotes', ['as'=>'CheckPendingVotes','uses'=>'FrontendController@CheckPendingVotes']);
Route::get('/CheckPending', ['as'=>'CheckPending','uses'=>'FrontendController@CheckPending']);
Route::get('/HomePage', ['as'=>'HomePage','uses'=>'FrontendController@HomePage']);
Route::get('NewVoteNow', ['as'=>'NewVoteNow','uses'=>'FrontendController@NewVoteNow']);
Route::post('/VoteNowNumberUnlimited',['as'=>'VoteNowNumberUnlimited','uses'=>'FrontendController@VoteNowNumberUnlimited']);
//Route::post('/DonatePayment',['as'=>'DonatePayment','uses'=>'FrontendController@DonatePayment']);
//Route::get('/VoteNow',['as'=>'VoteNow','uses'=>'FrontendController@VoteNow']);


Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::group(['middleware' => 'disablepreventback'],function(){
        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('/Dashboard', ['as' => 'backend.Dashboard', 'uses' => 'BackendController@Dashboard']);
        Route::get('/DashboardUser', ['as' => 'backend.DashboardUser', 'uses' => 'BackendController@DashboardUser']);
        Route::get('/MobileMoney', ['as' => 'backend.MobileMoney', 'uses' => 'BackendController@MobileMoney']);
        Route::get('/CompetitionMobileMoney', ['as' => 'backend.CompetitionMobileMoney', 'uses' => 'BackendController@CompetitionMobileMoney']);
        Route::post('/CompetitionMobileMoneyFilter', ['as' => 'backend.CompetitionMobileMoneyFilter', 'uses' => 'BackendController@CompetitionMobileMoneyFilter']);
        Route::post('/MobileMoneyFilter', ['as' => 'backend.MobileMoneyFilter', 'uses' => 'BackendController@MobileMoneyFilter']);
        Route::get('/FundMobileMoney', ['as' => 'backend.FundMobileMoney', 'uses' => 'BackendController@FundMobileMoney']);
        Route::post('/FundMobileMoneyFilter', ['as' => 'backend.FundMobileMoneyFilter', 'uses' => 'BackendController@FundMobileMoneyFilter']);

        Route::get('/Playlist', ['as' => 'backend.Playlist', 'uses' => 'BackendController@Playlist']);
        Route::get('/AllSongs', ['as' => 'backend.AllSongs', 'uses' => 'BackendController@AllSongs']);
        Route::get('/PromoteSong', ['as' => 'backend.PromoteSong', 'uses' => 'BackendController@PromoteSong']);
        Route::post('/PromoteStatus', ['as' => 'backend.PromoteStatus', 'uses' => 'BackendController@PromoteStatus']);
        Route::post('/AddArchivesSong', ['as' => 'backend.AddArchivesSong', 'uses' => 'BackendController@AddArchivesSong']);
        Route::post('/FilterArchived', ['as' => 'backend.FilterArchived', 'uses' => 'BackendController@FilterArchived']);
        Route::get('/AllArchived', ['as' => 'backend.AllArchived', 'uses' => 'BackendController@AllArchived']);
        Route::post('/AddArchives', ['as' => 'backend.AddArchives', 'uses' => 'BackendController@AddArchives']);
        Route::post('/AllSongsFilter', ['as' => 'backend.AllSongsFilter', 'uses' => 'BackendController@AllSongsFilter']);
        Route::get('/PlaylistRejected', ['as' => 'backend.PlaylistRejected', 'uses' => 'BackendController@PlaylistRejected']);
        Route::get('/PlaylistCompleted', ['as' => 'backend.PlaylistCompleted', 'uses' => 'BackendController@PlaylistCompleted']);
        Route::post('/AddPlaylist', ['as' => 'backend.AddPlaylist', 'uses' => 'BackendController@AddPlaylist']);
        Route::post('/EditPlaylist', ['as' => 'backend.EditPlaylist', 'uses' => 'BackendController@EditPlaylist']);
        Route::get('/SongPlaylist', ['as' => 'backend.SongPlaylist', 'uses' => 'BackendController@SongPlaylist']);
        Route::get('/DeletePlaylist', ['as' => 'backend.DeletePlaylist', 'uses' => 'BackendController@DeletePlaylist']);
        Route::get('/DeleteSongs', ['as' => 'backend.DeleteSongs', 'uses' => 'BackendController@DeleteSongs']);
        Route::get('/PlaylistCategory', ['as' => 'backend.PlaylistCategory', 'uses' => 'BackendController@PlaylistCategory']);
        Route::post('/AddPlaylistCategory', ['as' => 'backend.AddPlaylistCategory', 'uses' => 'BackendController@AddPlaylistCategory']);
        Route::post('/EditPlaylistCategory', ['as' => 'backend.EditPlaylistCategory', 'uses' => 'BackendController@EditPlaylistCategory']);
        Route::get('/DeletePlaylistCategory', ['as' => 'backend.DeletePlaylistCategory', 'uses' => 'BackendController@DeletePlaylistCategory']);

        Route::get('/AdminHomeSlider', ['as' => 'backend.HomeSlider', 'uses' => 'BackendController@HomeSlider']);
        Route::post('/AddHomeSlider', ['as' => 'backend.AddHomeSlider', 'uses' => 'BackendController@AddHomeSlider']);
        Route::post('/EditFundsSlider', ['as' => 'backend.EditFundsSlider', 'uses' => 'BackendController@EditFundsSlider']);
        Route::get('/DeleteFundsSlider', ['as' => 'backend.DeleteFundsSlider', 'uses' => 'BackendController@DeleteFundsSlider']);

        Route::get('/FundsSlider', ['as' => 'backend.FundsSlider', 'uses' => 'BackendController@FundsSlider']);
        Route::post('/AddFundsSlider', ['as' => 'backend.AddFundsSlider', 'uses' => 'BackendController@AddFundsSlider']);
        Route::post('/EditHomeSlider', ['as' => 'backend.EditHomeSlider', 'uses' => 'BackendController@EditHomeSlider']);
        Route::get('/DeleteHomeSlider', ['as' => 'backend.DeleteHomeSlider', 'uses' => 'BackendController@DeleteHomeSlider']);


        Route::get('/BroadCast', ['as' => 'backend.BroadCast', 'uses' => 'BackendController@BroadCast']);
        Route::post('/AddBroadCast', ['as' => 'backend.AddBroadCast', 'uses' => 'BackendController@AddBroadCast']);
        Route::post('/EditBroadCast', ['as' => 'backend.EditBroadCast', 'uses' => 'BackendController@EditBroadCast']);
        Route::get('/DeleteBroadCast', ['as' => 'backend.DeleteBroadCast', 'uses' => 'BackendController@DeleteBroadCast']);
//

        Route::get('/BroadCastTopic', ['as' => 'backend.BroadCastTopic', 'uses' => 'BackendController@BroadCastTopic']);
        Route::post('/AddBroadCastTopic', ['as' => 'backend.AddBroadCastTopic', 'uses' => 'BackendController@AddBroadCastTopic']);
        Route::post('/UpdatedBroadCastTopic', ['as' => 'backend.UpdatedBroadCastTopic', 'uses' => 'BackendController@UpdatedBroadCastTopic']);
        Route::get('/DeleteBroadCastTopic', ['as' => 'backend.DeleteBroadCastTopic', 'uses' => 'BackendController@DeleteBroadCastTopic']);

        Route::get('/BroadCastTopicComment', ['as' => 'backend.BroadCastTopicComment', 'uses' => 'BackendController@BroadCastTopicComment']);


        Route::get('/MusicStakeHolders', ['as' => 'backend.MusicStakeHolders', 'uses' => 'BackendController@MusicStakeHolders']);
        Route::post('/AddMusicStakeHolders', ['as' => 'backend.AddMusicStakeHolders', 'uses' => 'BackendController@AddMusicStakeHolders']);
        Route::post('/EditMusicStakeHolders', ['as' => 'backend.EditMusicStakeHolders', 'uses' => 'BackendController@EditMusicStakeHolders']);
        Route::get('/DeleteMusicStakeHolders', ['as' => 'backend.DeleteMusicStakeHolders', 'uses' => 'BackendController@DeleteMusicStakeHolders']);

        Route::get('/Fundings', ['as' => 'backend.Fundings', 'uses' => 'BackendController@Fundings']);
        Route::post('/AddFund', ['as' => 'backend.AddFund', 'uses' => 'BackendController@AddFund']);
        Route::post('/UpdateFundStatus', ['as' => 'backend.UpdateFundStatus', 'uses' => 'BackendController@UpdateFundStatus']);
        Route::post('/UpdateFund', ['as' => 'backend.UpdateFund', 'uses' => 'BackendController@UpdateFund']);
        Route::get('/DeleteFund', ['as' => 'backend.DeleteFund', 'uses' => 'BackendController@DeleteFund']);

        Route::get('/FundingsUser', ['as' => 'backend.FundingsUser', 'uses' => 'BackendController@FundingsUser']);


        Route::get('/ListNominee', ['as' => 'backend.ListNominee', 'uses' => 'BackendController@ListNominee']);
        Route::post('/EditListNominee', ['as' => 'backend.EditListNominee', 'uses' => 'BackendController@EditListNominee']);
        Route::get('/DeleteListNominee', ['as' => 'backend.DeleteListNominee', 'uses' => 'BackendController@DeleteListNominee']);

        Route::get('/ArtistsAccounts', ['as' => 'backend.ArtistsAccounts', 'uses' => 'BackendController@ArtistsAccounts']);
        Route::get('/ApproveSendSMS', ['as' => 'backend.ApproveSendSMS', 'uses' => 'BackendController@ApproveSendSMS']);
        Route::get('/RejectSendSMS', ['as' => 'backend.RejectSendSMS', 'uses' => 'BackendController@RejectSendSMS']);
        Route::get('/ArtistAddSong', ['as' => 'backend.ArtistAddSong', 'uses' => 'BackendController@ArtistAddSong']);

        Route::get('/ArtistDashboard', ['as' => 'backend.ArtistDashboard', 'uses' => 'BackendController@ArtistDashboard']);
        Route::get('/ArtistsAccountsInfo', ['as' => 'backend.ArtistsAccountsInfo', 'uses' => 'BackendController@ArtistsAccountsInfo']);

        Route::get('/ArtistsAccountsPending', ['as' => 'backend.ArtistsAccountsPending', 'uses' => 'BackendController@ArtistsAccountsPending']);
        Route::get('/ArtistsAccountsRejected', ['as' => 'backend.ArtistsAccountsRejected', 'uses' => 'BackendController@ArtistsAccountsRejected']);
        Route::get('/DeleteArtistAccount', ['as' => 'backend.DeleteArtistAccount', 'uses' => 'BackendController@DeleteArtistAccount']);

        Route::get('/ArtistRegistrationUpdate', ['as' => 'backend.ArtistRegistrationUpdate', 'uses' => 'BackendController@ArtistRegistrationUpdate']);
        Route::post('/UpdateArtistInfo', ['as' => 'backend.UpdateArtistInfo', 'uses' => 'BackendController@UpdateArtistInfo']);


        Route::get('/AccountList', ['as' => 'backend.AccountList', 'uses' => 'BackendController@AccountList']);
        Route::get('/CreateAccount', ['as' => 'backend.CreateAccount', 'uses' => 'BackendController@CreateAccount']);
        Route::post('/CreateAccount_', ['as' => 'backend.CreateAccount', 'uses' => 'BackendController@CreateAccount_']);
        Route::get('/DeleteAccount', ['as' => 'backend.DeleteAccount', 'uses' => 'BackendController@DeleteAccount']);
        Route::get('/ClaimPayment', ['as' => 'backend.ClaimPayment', 'uses' => 'BackendController@ClaimPayment']);
        Route::post('/ClaimPaymentAdmin', ['as' => 'backend.ClaimPaymentAdmin', 'uses' => 'BackendController@ClaimPaymentAdmin']);

        Route::get('/ListOfMembers', ['as' => 'backend.ListOfMembers', 'uses' => 'BackendController@ListOfMembers']);
        Route::get('/FilterMembers', ['as' => 'backend.FilterMembers', 'uses' => 'BackendController@FilterMembers']);
        Route::post('/FilterMembers_', ['as' => 'backend.FilterMembers_', 'uses' => 'BackendController@FilterMembers_']);
        Route::get('/ApproveMember', ['as' => 'backend.ApproveMember', 'uses' => 'BackendController@ApproveMember']);
        Route::get('/EditJoinMember', ['as' => 'backend.EditJoinMember', 'uses' => 'BackendController@EditJoinMember']);
        Route::post('/EditJoinMember_', ['as' => 'backend.EditJoinMember_', 'uses' => 'BackendController@EditJoinMember_']);

        Route::get('/AddAttractions', ['as' => 'backend.AddAttractions', 'uses' => 'BackendController@AddAttractions']);
        Route::get('/AddNews', ['as' => 'backend.AddNews', 'uses' => 'BackendController@AddNews']);
        Route::post('/AddNews_', ['as' => 'backend.AddNews_', 'uses' => 'BackendController@AddNews_']);
        Route::get('/NewsList', ['as' => 'backend.NewsList', 'uses' => 'BackendController@NewsList']);
        Route::get('/EditNews', ['as' => 'backend.EditNews', 'uses' => 'BackendController@EditNews']);
        Route::post('/EditNews_', ['as' => 'backend.EditNews_', 'uses' => 'BackendController@EditNews_']);
        Route::get('/DeleteNews', ['as' => 'backend.DeleteNews', 'uses' => 'BackendController@DeleteNews']);
        Route::get('/GuidesList', ['as' => 'backend.GuidesRequest', 'uses' => 'BackendController@GuidesList']);
        Route::get('/BankSlip', ['as' => 'backend.BankSlip', 'uses' => 'BackendController@BankSlip']);
        Route::get('/AddBankSlip', ['as' => 'backend.AddBankSlip', 'uses' => 'BackendController@AddBankSlip']);
        Route::post('/BankSlip_', ['as' => 'backend.BankSlip_', 'uses' => 'BackendController@BankSlip_']);
        Route::post('/AddAttractions_', ['as' => 'backend.AddAttractions_', 'uses' => 'BackendController@AddAttractions_']);
        Route::get('/SendSMS', ['as' => 'backend.SendSMS', 'uses' => 'BackendController@SendSMS']);
        Route::post('/SendSMS_', ['as' => 'backend.SendSMS_', 'uses' => 'BackendController@SendSMS_']);
        Route::get('/SendEmail', ['as' => 'backend.SendEmail', 'uses' => 'BackendController@SendEmail']);
        Route::post('/SendEmail_', ['as' => 'backend.SendEmail_', 'uses' => 'BackendController@SendEmail_']);

        Route::get('/CompanyCategory', ['as' => 'backend.CompanyCategory', 'uses' => 'BackendController@CompanyCategory']);
        Route::post('/CompanyCategory_', ['as' => 'backend.CompanyCategory_', 'uses' => 'BackendController@CompanyCategory_']);
        Route::get('/EditCompanyCategory', ['as' => 'backend.EditCompanyCategory', 'uses' => 'BackendController@EditCompanyCategory']);
        Route::post('/EditCompanyCategory_', ['as' => 'backend.EditCompanyCategory_', 'uses' => 'BackendController@EditCompanyCategory_']);
        Route::get('/DeleteCompanycategory', ['as' => 'backend.DeleteCompanycategory', 'uses' => 'BackendController@DeleteCompanycategory']);
        Route::post('/VotingStatus',['as'=>'VotingStatus','uses'=>'BackendController@VotingStatus']);
        Route::post('/RefreshMomo',['as'=>'RefreshMomo','uses'=>'BackendController@RefreshMomo']);


//        Route::post('/SearchVotingM', ['as'=>'SearchVotingM','uses'=>'BackendController@SearchVotingM']);
        Route::post('/SearchVotingM', ['as'=>'SearchVotingM','uses'=>'BackendController@SearchSong']);

        Route::get('/CompetitionDashboard', ['as'=>'CompetitionDashboard','uses'=>'BackendController@CompetitionDashboard']);
        Route::get('/AddCompetition', ['as'=>'AddCompetition','uses'=>'BackendController@AddCompetition']);
        Route::post('/UploadCompetition', ['as'=>'UploadCompetition','uses'=>'BackendController@UploadCompetition']);
        Route::post('/EditCompetition', ['as'=>'EditCompetition','uses'=>'BackendController@EditCompetition']);
        Route::get('/DeleteCompetition', ['as'=>'backend.DeleteCompetition','uses'=>'BackendController@DeleteCompetition']);

        Route::get('/CompetitionSlider', ['as'=>'CompetitionSlider','uses'=>'BackendController@CompetitionSlider']);
        Route::post('/AddCompetition_Slider', ['as'=>'AddCompetition_Slider','uses'=>'BackendController@AddCompetition_Slider']);
        Route::post('/EditCompetition_Slider', ['as'=>'EditCompetition_Slider','uses'=>'BackendController@EditCompetition_Slider']);
        Route::get('/DeleteCompetition_Slider', ['as'=>'backend.DeleteCompetition_Slider','uses'=>'BackendController@DeleteCompetition_Slider']);

        Route::get('/Contestants', ['as'=>'backend.Contestants','uses'=>'BackendController@Contestants']);
        Route::post('/AddContestants', ['as'=>'AddContestants','uses'=>'BackendController@AddContestants']);
        Route::post('/EditContestants', ['as'=>'EditContestants','uses'=>'BackendController@EditContestants']);
        Route::get('/DeleteContestant', ['as'=>'backend.DeleteContestant','uses'=>'BackendController@DeleteContestant']);

        Route::get('/Groups', ['as'=>'backend.Groups','uses'=>'BackendController@Groups']);
        Route::post('/AddGroup', ['as'=>'backend.AddGroup','uses'=>'BackendController@AddGroup']);

    });
});





