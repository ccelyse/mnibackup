<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlaylistInfo extends Model
{
    protected $table = "playlistinfo";
    protected $fillable = ['id','user_id','starting_date','ending_date','playlist_name','playlist_cate','playlist_payment_status','playlist_code'];
}
