<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class archivessongs extends Model
{
    protected $table = "archivessongs";
    protected $fillable = ['id','song_id','archive_id'];
}
