<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BroadCast extends Model
{
    protected $table = "broad_casts";
    protected $fillable = ['slider_title','slider_description','slider_photo','slider_status'];
}
