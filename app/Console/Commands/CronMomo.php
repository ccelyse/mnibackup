<?php

namespace App\Console\Commands;

use App\MomoTransaction;
use App\Votes;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class CronMomo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Momo:checkstatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checking Momo Status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $stat='PENDING';

        $dt = Carbon::now()->addDay();
        $yesterdaydt = Carbon::now()->subDays(1);
//        $yesterdaydt = Carbon::now()->subDays(5);
        $tomorrow = $dt->toDateString();
        $yesterday = $yesterdaydt->toDateString();

        $transactions = MomoTransaction::get()->count();

//        $transacs = MomoTransaction::where('status','like','%'.$stat.'%')
//            ->whereBetween('created_at', [$yesterday,$tomorrow])
//            ->get();

        $transacs = \App\MomoTransaction::select('status','transactionid')
            ->where('status','like','%'.$stat.'%')
//            ->whereBetween('created_at', [$yesterday,$tomorrow])
            ->get();

//        $transacs = MomoTransaction::where('status','PENDING')
//            ->whereBetween('transactionid', ["2351698482", "2355178810"])
//            ->get();
        $num = count((array)$transactions);

        $nums = array(count((array)$transactions)=>$num);

        //dd($transacs);
        //return view('backend.dashboard')->with(['num'=>$num]);

        foreach($transacs as $transac){

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$transac->transactionid",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Accept: */*",
                    "Cache-Control: no-cache",
                    "Connection: keep-alive",
                    "Host: akokanya.com",
                    "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                    "User-Agent: PostmanRuntime/7.11.0",
                    "accept-encoding: gzip, deflate",
                    "cache-control: no-cache",
                    "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                    "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                //echo $response;

                $responsedata=json_decode($response);
//                dd($responsedata);
                //Update to database Momo payments
                //echo $responsedata[0]->payment_status;

                $deletedat=0;

                $Momopayment = \App\MomoTransaction::where('transactionid',$responsedata[0]->external_payment_code)
                    ->update([
                        'transactionid'=> $responsedata[0]->external_payment_code,
                        'status'=> $responsedata[0]->payment_status,
                        'assignedid'=> $responsedata[0]->id,
                        'company_name'=> $responsedata[0]->company_name,
                        'code'=> $responsedata[0]->code,
                        // 'amount'=> $responsedata[0]->amount,
                        'payment_code'=> $responsedata[0]->payment_code,
                        'external_payment_code'=> $responsedata[0]->external_payment_code,
                        'payment_status'=> $responsedata[0]->payment_status,
                        'payment_type'=> $responsedata[0]->payment_type,
                        'callback_url'=> $responsedata[0]->callback_url,
                        'momodeleted_at'=> $deletedat,
                        'momocreated_at'=> $responsedata[0]->created_at,
                        'momoupdated_at'=> $responsedata[0]->updated_at,

                    ]);
                if($responsedata[0]->payment_status ==  "SUCCESSFUL"){
                    $updateverstatus = Votes::where('id',$transac->voter_id)->update(['voter_status'=> $responsedata[0]->payment_status]);
                    $this->info('Your request has been executed and updated votes');
                }else{
                    $this->info('Your request has not been executed but status has not changed');
                }

                //end Update to database Momo payments
            }
            //End Status showing
        }
        $this->info('all request has been executed');
//        end cron test
    }
}
