<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Groups extends Model
{
    protected $table = "groups";
    protected $fillable = ['competition_id','group_name','user_id'];
}
