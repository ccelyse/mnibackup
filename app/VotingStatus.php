<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VotingStatus extends Model
{
    protected $table = "voting_statuses";
    protected $fillable = ['id','voting_status'];
}
