<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archives extends Model
{
    protected $table = "archives";
    protected $fillable = ['id','archive_name','archive_starting_date','archive_ending_date'];
}
