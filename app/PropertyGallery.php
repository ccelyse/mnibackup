<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyGallery extends Model
{
    protected $table = "propertygallery";
    protected $fillable = ['id','property_id','property_pic_name'];
}
