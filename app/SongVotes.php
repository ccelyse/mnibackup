<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SongVotes extends Model
{
    protected $table = "SongVotes";
    protected $fillable = ['id','song_id','vote_id','created_at'];
}
