<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtistAccounts extends Model
{
    protected $table = "artist_accounts";
    protected $fillable = ['id','names','artistic_names','artist_number','artist_email','artist_ID','artist_modeofpayment','artist_mobilemoney_number','artist_mobilemoney_names',
        'artist_bankaccount_number','artist_bankaccount_names','artist_bank_name','artist_facebook','artist_instagram',
        'artist_twitter','picturename','artist_account_status','artist_youtube','artist_description','artist_category'];
}
