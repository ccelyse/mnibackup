<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BroadCastComment extends Model
{
    protected $table = "broad_cast_comments";
    protected $fillable = ['names','broadcast_id','email','comment'];
}
