<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FundsSliders extends Model
{
    protected $table = "funds_sliders";
    protected $fillable = ['slider_title','slider_description','slider_photo'];
}
