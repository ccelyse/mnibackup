<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nominee extends Model
{
    //
    protected $table = "nominee";
    protected $fillable = ['id','artist_name','nominee_category','artist_song','nominee_ipaddress','nominee_country'];
}
