<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MusicStakeHolders extends Model
{
    protected $table = "music_stake_holders";
    protected $fillable = ['slider_title','slider_description','slider_photo','slider_moto'];
}
