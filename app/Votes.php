<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Votes extends Model
{
    protected $table = "votes";
    protected $fillable = ['id','starting_date','ending_date','voterphonenumber','vote','voter_artist','voter_artist_song','voter_status'];
}
