<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Competitions extends Model
{
    use SoftDeletes;
    protected $table = "competitions";
    protected $fillable = ['competition_title','competition_youtube_link','competition_image','starting_date','end_date',
        'competition_code','competition_description','user_id','competition_id'];
}
