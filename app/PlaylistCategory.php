<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlaylistCategory extends Model
{
    protected $table = "playlistcategory";
    protected $fillable = ['id','category'];
}
