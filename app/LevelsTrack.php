<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LevelsTrack extends Model
{
    protected $table = "levels_track";
    protected $fillable = ['id','phonenumber','user_level','user_session','user_choice','song_nominated','artist_nominated','category_track'];
}
