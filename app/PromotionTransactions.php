<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromotionTransactions extends Model
{
    protected $table = "promotion_transactions";
    protected $fillable = ['phonenumber','song_id','promotion_period','transactionid', 'status', 'assignedid', 'company_name', 'code', 'amount','payment_code',
        'external_payment_code', 'payment_status', 'payment_type', 'callback_url', 'momodeleted_at', 'momocreated_at', 'momoupdated_at',
    ];
}
