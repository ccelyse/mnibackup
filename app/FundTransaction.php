<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FundTransaction extends Model
{
    protected $table = "fund_transactions";
    protected $fillable = ['phonenumber','names','fund_id','transactionid', 'status', 'assignedid', 'company_name', 'code', 'amount','payment_code',
        'external_payment_code', 'payment_status', 'payment_type', 'callback_url', 'momodeleted_at', 'momocreated_at', 'momoupdated_at',
    ];
}
