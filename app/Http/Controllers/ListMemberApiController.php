<?php

namespace App\Http\Controllers;

use App\ListMemberApi;
use Illuminate\Http\Request;

class ListMemberApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ListMemberApi  $listMemberApi
     * @return \Illuminate\Http\Response
     */
    public function show(ListMemberApi $listMemberApi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ListMemberApi  $listMemberApi
     * @return \Illuminate\Http\Response
     */
    public function edit(ListMemberApi $listMemberApi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ListMemberApi  $listMemberApi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ListMemberApi $listMemberApi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ListMemberApi  $listMemberApi
     * @return \Illuminate\Http\Response
     */
    public function destroy(ListMemberApi $listMemberApi)
    {
        //
    }
}
