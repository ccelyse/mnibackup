@extends('layoutsv2.master')

@section('title', 'Fundraising')

@section('content')
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
    .m-campaign-byline-meta {
        border-bottom: 1px solid #ddd;
        border-top: 1px solid #ddd;
        font-size: 1rem!important;
        padding-bottom: 1rem;
        padding-top: 1rem;
    }

    .list-unstyled {
        list-style-type: none;
        padding-left: 0;
        margin-top: 0;
        margin-bottom: 0;
    }
    .m-meta-list {
        color: currentColor;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-wrap: wrap;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
    }
    .m-meta-list-item {
        -webkit-align-items: center;
        -ms-flex-align: center;
        align-items: center;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        /*margin-right: 1rem;*/
        color: #000;
        width: 100%;
        font-family: "Roboto Slab", serif;
    }
    p, span{
        font-family: "Roboto Slab", serif !important;
        margin-top: 15px;
    }

    /*.custom_wr .wr h3, .custom_wr .wr h6 {*/
    /*    color: #ffffff !important;*/
    /*}*/
    @media print, screen and (min-width: 60em){
        .p-campaign-sidebar .o-campaign-sidebar {
            position: -webkit-sticky;
            position: sticky;
            top: 1rem;
        }
    }

    @media print, screen and (min-width: 60em){
        .o-campaign-sidebar {
            background: #fff;
            border-radius: .25rem;
            box-shadow: 0 0.3125rem 1rem -0.1875rem rgba(0,0,0,.2);
            padding-bottom: 1.5rem;
            padding-top: 1.5rem;
        }
    }


    @media screen and (max-width:59.99875em){
        .m-create-share-list--arrow .m-create-share-list-item{
            -webkit-align-items:center;
            -ms-flex-align:center;
            align-items:center;
            display:-webkit-flex;
            display:-ms-flexbox;
            display:flex;
            -webkit-justify-content:space-between;
            -ms-flex-pack:justify;
            justify-content:space-between
        }
    }
    .m-donation-meta .m-meta-list-item,.m-donation-meta .m-meta-list-item:not(:first-child):before{
        margin-right:.5rem
    }
    .m-donation-prompt{
        background-color:#fbf8f6;
        margin:-.75rem -1rem;
        padding:1rem 2rem
    }
    @media print,screen and (min-width:60em){
        .m-donation-prompt{
            margin:-.75rem -2rem
        }
    }
    .m-donation-velocity{
        -webkit-align-items:center;
        -ms-flex-align:center;
        align-items:center;
        border-bottom:1px solid #ddd;
        display:-webkit-flex;
        display:-ms-flexbox;
        display:flex;
        height:0;
        opacity:0;
        -webkit-animation-fill-mode:forwards;
        animation-fill-mode:forwards;
        -webkit-animation-delay:3s;
        animation-delay:3s;
        -webkit-animation-duration:1s;
        animation-duration:1s;
        -webkit-animation-name:slideIn;
        animation-name:slideIn;
        -webkit-animation-timing-function:ease-in-out;
        animation-timing-function:ease-in-out
    }
    @media screen and (max-width:47.99875em){
        .m-donation-velocity{
            margin-bottom:.75rem
        }
    }
    .m-donation-velocity-icon{
        -webkit-flex-shrink:0;
        -ms-flex-negative:0;
        flex-shrink:0
    }
    @-webkit-keyframes slideIn{
        25%{
            opacity:0
        }
        50%{
            height:55px;
            padding-bottom:.75rem
        }
        to{
            height:55px;
            opacity:1;
            padding-bottom:.75rem
        }
    }
    @keyframes slideIn{
        25%{
            opacity:0
        }
        50%{
            height:55px;
            padding-bottom:.75rem
        }
        to{
            height:55px;
            opacity:1;
            padding-bottom:.75rem
        }
    }

    .m-guidance-card{
        background-color:#fff;
        border:.5px solid #ddd;
        border-radius:.125rem;
        -webkit-flex-direction:column;
        -ms-flex-direction:column;
        flex-direction:column;
        margin-right:1.5rem;
        padding:1.5rem;
        text-decoration:none;
        width:19.75rem
    }
    @media screen and (max-width:59.99875em){
        .m-guidance-card{
            margin-right:.75rem;
            margin-left:.75rem;
            padding:1rem
        }
    }
    .m-guidance-card-content{
        -webkit-align-items:flex-start;
        -ms-flex-align:start;
        align-items:flex-start
    }
    @media print,screen and (min-width:60em){
        .m-guidance-card-content{
            margin-bottom:1rem
        }
    }
    .m-guidance-button-container{
        -webkit-align-items:flex-end;
        -ms-flex-align:end;
        align-items:flex-end;
        -webkit-flex-grow:1;
        -ms-flex-positive:1;
        flex-grow:1
    }
    .m-guidance-card-icon{
        width:2rem;
        margin-right:.75rem
    }
    @media print,screen and (min-width:60em){
        .m-guidance-card-icon{
            width:3rem;
            margin-right:1.5rem
        }
    }

    @-webkit-keyframes slidein{
        0%{
            margin-bottom:-100%
        }
        to{
            margin-bottom:0
        }
    }
    @keyframes slidein{
        0%{
            margin-bottom:-100%
        }
        to{
            margin-bottom:0
        }
    }
    .m-page-card{
        margin-top:1rem
    }
    @media print,screen and (min-width:60em){
        .m-page-card{
            margin-left:auto;
            margin-top:3rem;
            margin-right:auto;
            width:35rem
        }
    }
    @media print,screen and (min-width:60em){
        .m-page-card--inline{
            margin-left:0;
            margin-right:0
        }
    }
    @media print,screen and (min-width:60em){
        .m-page-card-content{
            background:#fff;
            border-radius:.25rem;
            box-shadow:0 4px 10px rgba(0,0,0,.1);
            padding:2rem
        }
    }

    .m-person-loading-avatar{
        -webkit-align-content:center;
        -ms-flex-line-pack:center;
        align-content:center;
        background-color:#ddd;
        border-radius:50%;
        -webkit-flex-shrink:0;
        -ms-flex-negative:0;
        flex-shrink:0;
        height:2.5rem;
        width:2.5rem
    }
    .m-person-loading-content{
        width:100%
    }
    .m-person-loading-content-child{
        background-color:#ddd;
        height:1rem
    }
    .m-person-loading-content-child:first-child{
        width:80%
    }
    .m-person-loading-content-child:last-child{
        margin-top:.5rem;
        width:30%
    }
    .m-progress-meter{
        display:grid
    }
    .m-progress-meter .a-progress-bar{
        margin-bottom:.5rem
    }
    .m-progress-meter-heading{
        line-height:.8;
        margin-bottom:.25rem;
        margin-top:.25rem
    }
    .m-select-field-label,.m-upload-field-label{
        display:block;
        line-height:1.3;
        padding-bottom:.25rem;
        padding-left:.25rem
    }
    .m-search-suggestions{
        background:#fff;
        border-top:1px solid #ddd;
        padding-bottom:1rem;
        padding-top:1rem
    }
    .m-search-suggestions-link{
        -webkit-align-items:center;
        -ms-flex-align:center;
        align-items:center;
        color:#767676;
        display:-webkit-flex;
        display:-ms-flexbox;
        display:flex;
        font-weight:300;
        padding-bottom:.5rem;
        padding-top:.5rem;
        text-decoration:none;
        width:100%
    }
    .m-search-suggestions-link:hover{
        background-color:#f8f8f8;
        color:#767676
    }
    .m-search-suggestions-link-text{
        font-size:.875rem
    }
    .m-search-suggestions-link:hover .m-search-suggestions-link-text{
        color:#00b964
    }
    @media print,screen and (min-width:48em){
        .m-search-suggestions-link-text{
            font-size:1.5rem
        }
    }
    .m-share-list{
        -webkit-align-items:center;
        -ms-flex-align:center;
        align-items:center;
        border-bottom:1px solid #ddd;
        border-top:1px solid #ddd
    }
    @media screen and (max-width:59.99875em){
        .m-share-list .m-share-list-item{
            padding-bottom:.75rem;
            padding-top:.75rem
        }
        .m-share-list .m-share-list-item:not(:last-child){
            border-bottom:1px solid #ddd
        }
    }
    @media print,screen and (min-width:60em){
        .m-share-list{
            display:-webkit-flex;
            display:-ms-flexbox;
            display:flex;
            -webkit-flex-wrap:wrap;
            -ms-flex-wrap:wrap;
            flex-wrap:wrap;
            margin-top:1.5rem!important;
            padding-top:1.5rem
        }
        .m-share-list .m-share-list-item{
            padding-bottom:1.5rem;
            width:25%
        }
    }
    @media screen and (max-width:59.99875em){
        .m-share-list--arrow .m-share-list-item{
            -webkit-align-items:center;
            -ms-flex-align:center;
            align-items:center;
            display:-webkit-flex;
            display:-ms-flexbox;
            display:flex;
            -webkit-justify-content:space-between;
            -ms-flex-pack:justify;
            justify-content:space-between
        }
    }
    @media screen and (max-width:59.99875em){
        .m-sheet-samp{
            text-align:center
        }
        .m-sheet-samp-image{
            display:-webkit-flex;
            display:-ms-flexbox;
            display:flex;
            -webkit-justify-content:center;
            -ms-flex-pack:center;
            justify-content:center
        }
    }
    .m-sheet-samp-title{
        margin-bottom:.25rem
    }
    @media screen and (max-width:59.99875em){
        .m-social-stats{
            border-top:1px solid #ddd;
            padding-top:.5rem
        }
    }
    .m-social-stats-tan{
        margin-bottom:3rem
    }
    .m-social-stats-tan .m-meta-list{
        -webkit-justify-content:space-between;
        -ms-flex-pack:justify;
        justify-content:space-between
    }
    @media screen and (max-width:59.99875em){
        .m-social-stats-tan{
            border-top:none
        }
        .m-social-stats-tan .m-meta-list{
            padding-top:.5rem
        }
    }
    @media print,screen and (min-width:60em){
        .m-social-stats-tan{
            -webkit-align-items:center;
            -ms-flex-align:center;
            align-items:center;
            display:-webkit-flex;
            display:-ms-flexbox;
            display:flex
        }
        .m-social-stats-tan .m-meta-list{
            -webkit-flex-grow:1;
            -ms-flex-positive:1;
            flex-grow:1;
            padding-left:3rem
        }
        .m-social-stats-tan .m-meta-list-item{
            border-left:1px solid #ddd;
            -webkit-flex-grow:1;
            -ms-flex-positive:1;
            flex-grow:1;
            margin-right:0
        }
        .m-social-stats-tan .text-stat{
            margin-left:auto;
            margin-right:auto
        }
    }

    .o-donation-list-item:not(:last-child){
        padding-bottom:.75rem
    }
    .o-donation-list-item:not(:first-child){
        border-top:1px solid #ddd;
        padding-top:.75rem
    }
    .o-campaign-sidebar-donations.o-donation-list-animation .o-donation-list-item:last-child,.p-campaign-page-donations.o-donation-list-animation .o-donation-list-item:last-child{
        -webkit-animation-fill-mode:forwards;
        animation-fill-mode:forwards;
        -webkit-animation-delay:3s;
        animation-delay:3s;
        -webkit-animation-duration:1s;
        animation-duration:1s;
        -webkit-animation-name:slideOut;
        animation-name:slideOut;
        -webkit-animation-timing-function:ease-in-out;
        animation-timing-function:ease-in-out;
        pointer-events:none
    }
    @-webkit-keyframes slideOut{
        0%{
            opacity:1
        }
        25%{
            opacity:0
        }
        75%{
            height:0
        }
        to{
            height:0;
            opacity:0;
            padding-top:0
        }
    }
    @keyframes slideOut{
        0%{
            opacity:1
        }
        25%{
            opacity:0
        }
        75%{
            height:0
        }
        to{
            height:0;
            opacity:0;
            padding-top:0
        }
    }
    @media print,screen and (min-width:60em){
        .o-campaign-sidebar{
            background:#fff;
            border-radius:.25rem;
            box-shadow:0 .3125rem 1rem -.1875rem rgba(0,0,0,.2);
            padding-bottom:1.5rem;
            padding-top:1.5rem
        }
    }
    .o-campaign-sidebar .m-social-stats{
        margin-bottom:1rem
    }
    @media print,screen and (min-width:60em){
        .o-campaign-sidebar .m-social-stats{
            margin-top:1rem
        }
    }
    .o-campaign-sidebar-progress-meter .m-progress-meter-heading{
        margin-bottom:.75rem
    }
    @media print,screen and (min-width:60em){
        .o-campaign-sidebar-progress-meter .a-progress-bar{
            grid-row:2/3;
            margin-bottom:0
        }
        .o-campaign-sidebar-progress-meter .m-progress-meter-heading{
            font-size:1.5rem;
            grid-row:1/2;
            font-weight: bold;
            line-height: 1;
        }
    }
    .o-campaign-sidebar-donation-message{
        color:#767676;
        margin-top:2rem;
        margin-bottom:0;
        text-align:center
    }
    .o-campaign-sidebar--with-donations-list{
        padding-bottom:2rem
    }
    .o-campaign-sidebar-donations{
        margin-top:1rem!important
    }
    @media screen and (min-width:60em)and (max-width:71.99875em){
        .o-campaign-sidebar-donations .m-person-info-avatar{
            display:none
        }
    }
    @media print,screen and (min-width:60em){
        .o-campaign-sidebar-wrapper{
            padding-left:1.5rem;
            padding-right:1.5rem
        }
    }
    .o-campaign-sidebar-notification{
        background-color:#fbf8f6;
        font-size:.875rem;
        margin-bottom:1rem;
        padding:1rem
    }
    @media screen and (max-width:59.99875em){
        .o-campaign-sidebar-notification{
            margin-left:-1rem;
            margin-right:-1rem
        }
    }
    @media print,screen and (min-width:60em){
        .o-campaign-sidebar-notification{
            border-radius:.25rem;
            margin-top:1rem
        }
    }
    .u-pointer{
        cursor:pointer
    }
    @media print,screen and (min-width:60em){
        .o-card-list-item{
            margin-bottom:1rem
        }
    }
    @media screen and (max-width:59.99875em){
        .o-card-list-container{
            height:9rem;
            overflow:hidden;
            position:relative
        }
        .o-card-list-items{
            display:-webkit-flex;
            display:-ms-flexbox;
            display:flex;
            position:absolute;
            -webkit-transition:transform .3s ease-in;
            transition:transform .3s ease-in
        }
        .o-card-list-item{
            margin-right:1rem;
            width:16.125rem
        }
    }
    .o-comment-creation-avatar-container{
        -webkit-align-items:center;
        -ms-flex-align:center;
        align-items:center;
        display:-webkit-flex;
        display:-ms-flexbox;
        display:flex
    }
    .o-comment-creation-avatar{
        margin-right:1rem
    }
    .o-comment-creation-text-area{
        border-bottom:0;
        border-bottom-left-radius:0;
        border-bottom-right-radius:0;
        min-height:49.31px;
        padding-right:3rem
    }
    .o-comment-creation-input{
        position:relative
    }
    .o-comment-creation-count{
        bottom:0;
        color:#767676;
        padding:.75rem 1rem;
        position:absolute;
        right:0
    }
    .o-comment-creation-add-photo{
        border-bottom-left-radius:.25rem;
        border-bottom-right-radius:.25rem;
        border:1px solid #ddd;
        cursor:pointer;
        display:-webkit-flex;
        display:-ms-flexbox;
        display:flex;
        padding:.75rem 1rem
    }
    .o-comment-form-upload-button--disabled{
        background-color:transparent!important;
        border:0;
        color:#919191!important
    }
    .o-dropzone-outline-reset{
        outline:none
    }
    .o-expansion-list-header{
        -webkit-align-items:center;
        -ms-flex-align:center;
        align-items:center;
        display:-webkit-flex;
        display:-ms-flexbox;
        display:flex;
        -webkit-flex-direction:row;
        -ms-flex-direction:row;
        flex-direction:row;
        -webkit-justify-content:space-between;
        -ms-flex-pack:justify;
        justify-content:space-between
    }
    .o-expansion-list-filter{
        border-radius:1rem
    }
    .o-comments-list-item:not(:last-child){
        border-bottom:1px solid #ddd;
        margin-bottom:2rem;
        padding-bottom:2rem
    }
    .o-contact-support-options{
        font-weight:700;
        grid-column-gap:4rem!important
    }
    .o-contact-support-item:first-child{
        position:relative
    }
    .o-contact-support-item:first-child:before{
        border-right:1px solid #ddd;
        content:"";
        display:block;
        height:100%;
        position:absolute;
        right:-2rem;
        top:0
    }
    .o-contact-support-label{
        display:block
    }
    .o-bene-phone{
        -webkit-align-items:flex-end;
        -ms-flex-align:end;
        align-items:flex-end;
        display:-webkit-flex;
        display:-ms-flexbox;
        display:flex
    }
    .o-bene-phone-prefix{
        max-width:12rem
    }
    .o-bene-phone-value{
        -webkit-flex-grow:1;
        -ms-flex-positive:1;
        flex-grow:1;
        margin-left:1rem
    }
    .o-bene-money-received{
        -webkit-align-items:flex-end;
        -ms-flex-align:end;
        align-items:flex-end;
        display:-webkit-flex;
        display:-ms-flexbox;
        display:flex
    }
    .o-bene-money-received-currency{
        min-width:7rem
    }
    .o-bene-money-received-value{
        -webkit-flex-grow:1;
        -ms-flex-positive:1;
        flex-grow:1;
        margin-left:1rem
    }
    .o-donor-phone{
        -webkit-align-items:flex-end;
        -ms-flex-align:end;
        align-items:flex-end;
        display:-webkit-flex;
        display:-ms-flexbox;
        display:flex
    }
    .o-donor-phone-prefix{
        max-width:12rem
    }
    .o-donor-phone-value{
        -webkit-flex-grow:1;
        -ms-flex-positive:1;
        flex-grow:1;
        margin-left:1rem
    }
    .o-fraud-phone{
        -webkit-align-items:flex-end;
        -ms-flex-align:end;
        align-items:flex-end;
        display:-webkit-flex;
        display:-ms-flexbox;
        display:flex
    }
    .o-fraud-phone-prefix{
        max-width:12rem
    }
    .o-fraud-phone-value{
        -webkit-flex-grow:1;
        -ms-flex-positive:1;
        flex-grow:1;
        margin-left:1rem
    }
    .o-contact-wrapper{
        display:grid;
        grid-gap:1rem;
        grid-template-columns:1fr;
        grid-template-rows:auto;
        grid-template-areas:"breadcrumbs" "sidebar" "header" "content";
        padding-bottom:2rem;
        padding-top:1rem
    }


    .text-stat-title {
        color: #767676;
        font-weight: 400;
    }
    .text-stat {
        font-size: .875rem;
        line-height: 1.21;
    }
    .o-campaign-sidebar-wrapper a{
        background: #22706c;
        padding: 10px;
        width: 100%;
        display: inline-block;
        text-align: center;
        color:#fff;
        margin-top: 15px;
        border-radius: 8px;
    }
    .mr {
        margin-right: 1rem!important;
    }
    .m-person-info {
        -webkit-align-items: flex-start;
        -ms-flex-align: start;
        align-items: flex-start;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
    }
    .a-avatar {
        border-radius: 50%;
        color: #fff;
        -webkit-flex-shrink: 0;
        -ms-flex-negative: 0;
        flex-shrink: 0;
        height: 2.5em;
        width: 2.5em;
    }
    .m-person-info-name {
        overflow-wrap: break-word;
        word-break: break-word;
        color: #f18d04;
    }
    .mt {
        margin-top: 1rem!important;
    }
    .a-button--hollow-green {
        border: 1px solid #00b964;
        color: #00b964;
    }
    .flex-container {
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
    }

    .align-center {
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
    }
    .a-button--hollow-gray, .a-button--hollow-green, .a-button--hollow-yellow, .a-button--solid-facebook, .a-button--solid-green, .a-button--solid-yellow {
        border-radius: .25rem;
        font-weight: 900;
        letter-spacing: .02em;
    }

    .a-button--small {
        min-height: 2rem;
        padding-bottom: .25rem;
        padding-top: .25rem;
    }
    .a-button--medium, .a-button--small {
        font-size: .875rem;
        line-height: 1.4;
    }
    .a-button--large, .a-button--medium, .a-button--small {
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
        padding-left: 1rem;
        padding-right: 1rem;
        text-align: center;
    }
    .a-button {
        -webkit-appearance: none;
        -webkit-align-items: center;
        -ms-flex-align: center;
        align-items: center;
        background-color: transparent;
        color: inherit;
        cursor: pointer;
        display: -webkit-inline-flex;
        display: -ms-inline-flexbox;
        display: inline-flex;
        font-size: 1em;
        text-decoration: none;
        color:#000;
    }
</style>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    @foreach($more as $details)
    <header>
        @include('layoutsv2.upmenu')
        <div class="camera_container">
            <div class="row">
                <div class="col-lg-6">
                    <div id="camera" class="camera_wrap">
                            <div data-src="Funds/{{$details->fund_image}}"></div>
                            <div data-src="Funds/{{$details->fund_image}}"></div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
        <section class="well" style="padding-bottom: 15px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h2>{{$details->fund_title}}</h2>
                        <ul class="m-campaign-byline-meta list-unstyled m-meta-list m-meta-list--pipe">
                            <li class="m-meta-list-item">
                                <span class="m-campaign-byline-created a-created-date">
                                    <?php
                                    $created_at =  $details->created_at;
                                    $lastTimeLoggedOut = \Illuminate\Support\Carbon::parse($created_at)->diffForHumans();
                                    echo $lastTimeLoggedOut;
                                    ?>
                                </span>
                            </li>
                            <li class="m-meta-list-item">
                                <a class="" href="#">
                                    {{$details->name}} is organizing this fundraiser.
                                </a>
                            </li>
                        </ul>
                        <p>
                            <?php
                                $text = $details->fund_description;
                                echo $text;
                            ?>
                        </p>
                    </div>
                    <div class="col-md-4">
                        <div class="p-campaign-sidebar">
                            <aside class="o-campaign-sidebar">
                                <div class="o-campaign-sidebar-wrapper">
                                    <div class="o-campaign-sidebar-progress-meter m-progress-meter">
                                        <h2 class="m-progress-meter-heading">{{$details->CheckRaisedAmount}} Rwf
                                            <span class="text-stat text-stat-title">raised of {{$details->fund_amount}} Rwf goal</span>
                                        </h2>
                                    </div>

                                    <a href="#">
                                        <i class="fas fa-share"></i> Share
                                    </a>
                                    <a href="{{ route('V2.Donate',['id'=> $details->id])}}">
                                        Donate now
                                    </a>
                                </div>
                                <div class="show-for-large">
                                    <div class="o-campaign-sidebar-wrapper">
                                        <ul class="o-campaign-sidebar-donations list-unstyled o-donation-list">

                                            @foreach($details->CheckRaisedPeople as $funds)

                                            <li class="o-donation-list-item">
                                                <div class="m-donation m-person-info">
                                                    <img class="a-default-avatar m-person-info-avatar mr a-avatar a-base-image" alt="" src="https://www.gofundme.com/static/media/DefaultAvatar.65712475de0674c9f775ae3b64e7c69f.svg">
                                                    <div>
                                                        <div class="m-person-info-name">{{$funds['names']}}
                                                            <!-- -->&nbsp;
                                                        </div>
                                                        <div class="m-person-info-content">
                                                            <ul class="m-donation-meta list-unstyled m-meta-list m-meta-list--bullet">
                                                                <li class="m-meta-list-item">
                                                                <span class="weight-900">
                                                                   {{$funds['amount']}} Rwf
                                                                </span>
                                                                </li>
                                                                <li class="m-meta-list-item">
                                                                    <span class="color-gray">
                                                                         <?php
                                                                        $created_at =  $funds['created_at'];
                                                                        $lastTimeLoggedOut = \Illuminate\Support\Carbon::parse($created_at)->diffForHumans();
                                                                        echo $lastTimeLoggedOut;
                                                                        ?>
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>

                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="flex-container align-center">
                                        <a class="mt a-button a-button--inline a-button--small a-button--hollow-green a-link" href="#">See all</a>
                                    </div>
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    @endforeach
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
</div>
@endsection
