@extends('layoutsv2.master')

@section('title', 'FAQ')

@section('content')
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
    .terms p{
        line-height: 2;
        color: #000;
        font-family: "Roboto Slab", serif;
    }
</style>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
        <section class="well" style="padding-bottom: 100px;">
            <div class="container">
                <div class="row">
                    <div class="terms">
                        <h1>F.A.Q</h1>
                        <h4 style="padding: 15px 0">1. How does a song get a lot of votes?</h4>
                        <p>
                            A song gets a lot of votes when music fans vote for the song on the MNI Selection Music Chart.
                        </p>

                        <h4 style="padding: 15px 0">2. When does the month of MNI start and end?</h4>
                        <p>
                            According to MNI, the month starts on the first day of the month and ends on the last day of the month.
                        </p>

                        <h4 style="padding: 15px 0">3. When does the musician receive royalties from his MNI Selection song?</h4>
                        <p>
                            The royalties of the song are paid no later than the second week of the following month to the musician.
                        </p>

                        <h4 style="padding: 15px 0">4. How do you vote for a song?</h4>
                        <p>
                            1. Go to the browser and search for www.mni.rw<br>
                            2. Click the vote button or scroll down to reach the MNI Selection Music Chart. You can also click on, chart, and choose a monthly chart.<br>
                            3. Click the voting button next to the song you want to vote<br>
                            4. If you see that your choice is correct, enter your mobile money or bank card number.<br>
                            5. Choose the money, then press the vote<br>
                            6.If you choose the mobile money option to vote, you will receive a pop-up message to confirm your payment on your phone screen, enter your mobile money pin to confirm the transaction. Or you are going to follow the steps required by paying with a bank card.<br>
                            7. Click home to check the total vote of the song by scrolling down to the MNI Selection<br>
                            8.If you would like to keep voting, redo procedures 1-6.<br>

                            N.B: This method is only used on the internet and provides more information about the song you want to vote on.

                        </p>
                        <p>
                            Using our website, www.mni.rw, requires you to have access to the internet, which means that when you use cellular networks, you will need airtime for data,
                            and when you use wifi, you will not need airtime.
                        </p>
                        <h4 style="padding: 15px 0">5. How many times a person is allowed to vote?</h4>
                        <p>
                            A person is allowed to vote as many times as he or she wants, as long as the song being voted is on the MNI Selection Music Chart.
                        </p>
                        <h4 style="padding: 15px 0">6. How can I know the songs that can be voted on?</h4>
                        <p>
                            You can find out which song you can vote by going to our website www.mni.rw and searching the MNI Selection Music Chart. You can also contact us through our contacts, and we'll tell you if the song is on the chart, or if you can contact the artist himself.
                        </p>

                        <h4 style="padding: 15px 0">7. Are all the songs allowed to go to MNI Selection?</h4>
                        <p>
                            With the exception of the songs prohibited by the responsible institutions, all other songs by Rwandan musicians can be displayed on the MNI Selection.
                        </p>
                        <h4 style="padding: 15px 0">8. Who is allowed to vote</h4>
                        <p>
                            Anyone who is able to vote in the manner indicated above may vote
                        </p>
                        <h4 style="padding: 15px 0">9. How much do you pay for your vote?</h4>
                        <p>
                            One vote is equivalent to 50 RWF but transactions should not be less than 100 RWF which is charged via mobile money, while if you vote by bank card you are charged $5 and the money is divided into votes by the exchange rate (RWF) of that period.
                        </p>
                        <h4 style="padding: 15px 0">10. Who can display a song to the MNI Selection Music Chart?</h4>
                        <p>
                            He is a musician or his management team whose account has been approved on MNI platforms and has the right to obtain revenue from that song..
                        </p>
                        <h4 style="padding: 15px 0">11. Can I listen to the music on the MNI platforms?</h4>
                        <p>
                            Yes, you can listen to music via MNI platforms through our project called MNI Broadcast on our website, which has a daily playlist. (www.mni.rw/broadcast)
                        </p>
                        <h4 style="padding: 15px 0">12. Who is allowed to apply for funds through the MNI Fund?</h4>
                        <p>
                            It is anyone whose account has been approved on our platforms and the fund is the result of a project that they have and should be legal.
                        </p>
                        <p>
                            NB: These are the frequently asked questions, and if you have any further questions you would like to ask, you can reach us through our contacts below.
                        </p>
                        <p>
                            Email: info@mni.rw<br>
                            Tel: +250 780 008 883<br>
                            Social media: @aboutmni<br>

                        </p>

                    </div>

                </div>
            </div>
        </section>
    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
</div>
@endsection
