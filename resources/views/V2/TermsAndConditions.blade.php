@extends('layoutsv2.master')

@section('title', 'Terms And Conditions')

@section('content')
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
    .terms p{
        line-height: 2;
        color: #000;
        font-family: "Roboto Slab", serif;
    }
</style>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
        <section class="well" style="padding-bottom: 100px;">
            <div class="container">
                <div class="row">
                    <div class="terms">
                        <h1>Terms and conditions</h1>
                        <h4 style="padding: 15px 0">Updated 01/07/2020</h4>
                        <p>MNI Ltd is a private company that has been granted the right to operate by the responsible
                            government department (RDB) classified under Company Code: 108135642. It has a document of
                            partnership with the Rwandan Music Federation (RMF) on behalf of all Rwandan musicians dated
                            22/07/2019 and also one from the Rwandan Academy of Language and Culture (RALC) registered 280
                            / RALC/2019. MNI Visitor is a person who uses MNI platforms on the Internet or not on the Internet.
                            Using MNI services, either on the website or on any other platform made by MNI Ltd, means that you
                            have entered into an agreement with MNI Ltd on the terms of use and privacy policy that you find <a href="#">here.</a>
                        </p>

                        <h4 style="padding: 15px 0">Updates</h4>
                        <p>
                            Even if it is our responsibility to inform you of any changes to these Terms of Use, we encourage you to visit
                            our Terms of Use at least once a month if you do not receive our communication regarding the changes.
                        </p>

                        <h4 style="padding: 15px 0">Data collected from the visitor to the MNI platforms and its security</h4>
                        <p>
                            MNI collects the mobile phone number of the visitor who votes for a song on the MNI Selection Music Chart but is kept
                            private because it can not be viewed on the website or the song that is voted on by the mobile phone number, instead
                            only the votes that you vote are displayed. The collected phone numbers can be used to update owners on MNI updates,
                            in research, to monitor better performance of MNI Selection voting in order to provide better service.
                        </p>
                        <p>
                            For those who use bank cards to vote for the MNI Selection song, MNI does not collect the data for the bank card,
                            but instead collects the data that confirms that the transaction is successful. Bank card data shall be kept secure
                            by the responsible parties.
                        </p>
                        <p>
                            MNI ensures that the data we collect from visitors is kept safe and that it is not allowed to be shared with other people.
                        </p>

                        <h4 style="padding: 15px 0">How to vote a song on the MNI Selection Music Chart and how much is charged.</h4>
                        <p>
                            Voting takes place on the website of MNI, www.mni.rw, where you can vote for a song of your choice on the MNI Selection music chart.
                            When you vote, you are charged 100 RWF from your mobile money account and  $5 from your bank card, and this is
                            a source of revenue for the owner of the song you vote for.
                        </p>
                        <p>
                            Using our website, www.mni.rw, requires you to have access to the internet, which means that when you use cellular networks, you will need airtime for data,
                            and when you use wifi, you will not need airtime.
                        </p>
                        <h4 style="padding: 15px 0">Using MNI Fund</h4>
                        <p>
                            Anyone has the right to support any music stakeholder as soon as they have requested funding and to do so in their ability. The mobile phone number or bank card used
                            to give the money is not used in any other way.
                        </p>
                        <h4 style="padding: 15px 0">The rights of the person using the MNI platform</h4>
                        <p>
                            Everyone is welcome to use our services willingly, in case they don't feel like using our services,
                            they are free not to use our services. MNI does not have a subscription system that requires you to pay
                            when you use or do not use our services. Instead, you pay willingly if you want to use the above mentioned services.
                        </p>
                        <p>
                            MNI is ready to welcome people's ideas and perspectives in a polite manner, but it is not allowed to use abusive language.
                        </p>
                        <h4 style="padding: 15px 0">How song are used</h4>
                        <p>
                            Songs shown on the MNI will be used in the MNI Selection project and may be promoted on various platforms and media to encourage people to support them (songs) either in visuals or not in visuals. They will also be seen on all of our platforms as a way to promote them. Songs will not be used to generate revenue in any other way than that mentioned here, and if there is any change we will inform you about it.
                        </p>
                        <h4 style="padding: 15px 0">Who is considered to be a musician MNI Ltd.</h4>
                        <p>
                            It's someone who released at least one song in a legal way and has the right to get royalties from that song.
                        </p>
                        <h4 style="padding: 15px 0">Who is considered to be a musician MNI Ltd.</h4>
                        <p>
                            It's someone who released at least one song in a legal way and has the right to get royalties from that song.
                        </p>
                        <h4 style="padding: 15px 0">Songs that are allowed on MNI platforms</h4>
                        <p>
                            Even if the aim of MNI Ltd is to participate in the development of the Rwandan music industry, any song that has been banned by the responsible institution will not be put on MNI platforms.
                        </p>
                        <h4 style="padding: 15px 0">Programs of third parties</h4>
                        <p>
                            In line with the performance of MNI services, MNI uses third party programs to provide good services, but we encourage you to read their terms of use and privacy policies to ensure that you agree with them. In the event that you do not agree with the terms of use and privacy policies of third party programs MNI will not be responsible for this.
                        </p>
                        <h4 style="padding: 15px 0">Conclusions</h4>
                        <p>
                            These are the terms and conditions that we follow, when there is a change to them we inform you, and everyone using our services is encouraged to visit our platforms to check if there is any change.

                            If you have any questions or ideas regarding these terms and conditions, please contact us by email: info@mni.rw or by telephone: +250780008883.
                        </p>

                    </div>

                </div>
            </div>
        </section>
    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
</div>
@endsection
