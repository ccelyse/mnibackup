@extends('layoutsv2.master')

@section('title', 'Sales And Marketing')

@section('content')
    <script src="https://unpkg.com/scrollreveal@4.0.0/dist/scrollreveal.min.js"></script>
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
    .terms p{
        line-height: 2;
        color: #000;
        font-family: "Roboto Slab", serif;
    }





    .button-block {
        display:flex;
    }

    .site-header {
        position:relative;
        z-index:2;
        padding:24px 0;
    }

    .site-header::before {
        content:'';
        position:absolute;
        top:-140px;
        right:-100px;
        width:1440px;
        height:324px;
        background-image:url(../images/header-illustration-light.svg);
    }

    .lights-off .site-header::before {
        background-image:url(../images/header-illustration-dark.svg);
    }

    .site-header-inner {
        position:relative;
        display:flex;
        justify-content:space-between;
        align-items:center;
    }

    .header-links a:not(.button) {
        font-size:16px;
        letter-spacing:-.1px;
        font-weight:600;
        color:#8595AE;
        text-transform:uppercase;
        text-decoration:none;
        line-height:16px;
        padding:8px 24px;
    }

    .header-links a:not(.button):hover,.header-links a:not(.button):active {
        color:#fff;
    }

    .hero {
        text-align:center;
        padding-top:48px;
        padding-bottom:88px;
    }

    .hero-cta {
        max-width:400px;
        margin-left:auto;
        margin-right:auto;
        margin-bottom:80px;
    }

    .lights-toggle {
        color:rgba(107,122,144,0.64);
    }

    .hero-media-illustration {
        position:absolute;
        top:-10%;
        left:-15px;
    }

    .hero-media-illustration img,.hero-media-illustration svg {
        max-width:136%;
    }

    .hero-media-image {
        box-shadow:48px 16px 48px rgba(24,37,56,0.12);
        border-radius:4px;
        -webkit-transform:perspective(1000px) rotateY(16deg) rotateX(2deg) rotateZ(-7deg) scaleY(0.95) translatex(2%);
        transform:perspective(1000px) rotateY(16deg) rotateX(2deg) rotateZ(-7deg) scaleY(0.95) translatex(2%);
        margin:0 auto;
    }

    .features-wrap {
        max-width:540px;
        margin:0 auto;
    }

    .features-image {
        position:relative;
        margin-top:64px;
        margin-bottom:112px;
    }

    .features-illustration {
        position:absolute;
        top:-2%;
        left:50%;
        -webkit-transform:translateX(-50%);
        transform:translateX(-50%);
        max-width:136%;
    }

    .features-box {
        box-shadow:48px 16px 48px rgba(24,37,56,0.12);
        -webkit-transform:perspective(1000px) rotateY(10deg) translateY(2%);
        transform:perspective(1000px) rotateY(10deg) translateY(2%);
        margin:0 auto;
    }

    .feature {
        text-align:center;
        margin-bottom:48px;
    }

    .feature-icon {
        display:inline-flex;
        margin-bottom:16px;
    }

    .feature-title {
        position:relative;
        margin-bottom:26px;
    }

    .feature-title::after {
        content:'';
        width:32px;
        height:2px;
        position:absolute;
        bottom:-14px;
        left:calc(50%-16px);
        background:#E9EDF3;
    }

    .lights-off .feature-title::after {
        background:#304057;
    }

    .cta::before {
        content:'';
        position:absolute;
        bottom:-32px;
        left:calc(50%-720px);
        height:263px;
        width:1440px;
        background-image:url(../images/cta-illustration-light.svg);
    }

    .lights-off .cta::before {
        background-image:url(../images/cta-illustration-dark.svg);
    }

    .cta-cta {
        max-width:400px;
        margin-left:auto;
        margin-right:auto;
    }

    .is-boxed {
        background:#E9EDF3;
    }

    .body-wrap {
        background:#fff;
        overflow:hidden;
        display:flex;
        flex-direction:column;
        min-height:100vh;
    }

    .boxed-container {
        max-width:1440px;
        box-shadow:0 16px 48px rgba(255,255,255,0.5);
        transition:box-shadow .15s ease;
        margin:0 auto;
    }

    main {
        flex:1 0 auto;
    }

    .section-inner {
        position:relative;
        padding-top:48px;
        padding-bottom:48px;
    }

    .site-footer {
        font-size:14px;
        line-height:20px;
        letter-spacing:0;
        background:#182538;
        color:#6B7A90;
    }

    .site-footer a {
        color:#6B7A90;
        text-decoration:none;
    }

    .site-footer a:hover,.site-footer a:active {
        text-decoration:underline;
    }

    .site-footer-inner {
        position:relative;
        display:flex;
        flex-wrap:wrap;
        padding-top:48px;
        padding-bottom:48px;
    }

    .footer-brand,.footer-links,.footer-social-links,.footer-copyright {
        flex:none;
        width:100%;
        display:inline-flex;
        justify-content:center;
    }

    .asset-light,.asset-dark {
        visibility:hidden;
        opacity:0;
    }

    .is-loaded .asset-light,.is-loaded .asset-dark {
        visibility:visible;
        opacity:1;
    }

    .lights-off.is-boxed {
        background:#202d3f;
    }

    .lights-off .body-wrap {
        background:#182538;
    }

    .lights-off .boxed-container {
        box-shadow:0 16px 48px rgba(0,0,0,0.2);
    }

    .lights-off hr {
        display:block;
        height:1px;
        background:#304057;
    }

    .lights-off .has-top-divider::before {
        content:'';
        position:absolute;
        top:0;
        left:0;
        width:100%;
        display:block;
        height:1px;
        background:#304057;
    }

    .lights-off .has-bottom-divider::after {
        content:'';
        position:absolute;
        bottom:0;
        left:0;
        width:100%;
        display:block;
        height:1px;
        background:#304057;
    }

    body,a,h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6 {
        transition:color .25s ease;
    }

    .is-boxed,.body-wrap,.has-top-divider,.has-bottom-divider::after,hr::after,.feature-title::after {
        transition:background .25s ease;
    }

    30% {
        opacity:1;
    }

    .is-loaded .header-logo-image {
        -webkit-animation:fadeInLogo .35s both cubic-bezier(0.3,0,0.2,1);
        animation:fadeInLogo .35s both cubic-bezier(0.3,0,0.2,1);
        will-change:transform;
    }

    .is-loaded .hero-media-image {
        -webkit-animation:fadeInLeftMedia 1s .2s both cubic-bezier(0.3,0,0.2,1);
        animation:fadeInLeftMedia 1s .2s both cubic-bezier(0.3,0,0.2,1);
        will-change:transform;
    }

    .is-loaded .hero-media-illustration-image {
        -webkit-animation:fadeInLeftIllustration 1s .2s both cubic-bezier(0.3,0,0.2,1);
        animation:fadeInLeftIllustration 1s .2s both cubic-bezier(0.3,0,0.2,1);
        will-change:transform;
    }

    .is-loaded .features-box {
        -webkit-animation:fadeUpBox 1s .2s both cubic-bezier(0.3,0,0.2,1);
        animation:fadeUpBox 1s .2s both cubic-bezier(0.3,0,0.2,1);
        will-change:transform;
    }

    .is-loaded .header-illustration-image {
        -webkit-animation:fadeIn 1s both cubic-bezier(0.3,0,0.2,1);
        animation:fadeIn 1s both cubic-bezier(0.3,0,0.2,1);
        will-change:transform;
    }

    .is-loaded .features-illustration {
        -webkit-animation:fadeIn 1s .2s both cubic-bezier(0.3,0,0.2,1);
        animation:fadeIn 1s .2s both cubic-bezier(0.3,0,0.2,1);
        will-change:transform;
    }

    article,aside,footer,header,nav,section,figcaption,figure,main,details,menu,img,svg,.lights-off .site-footer::before,.lights-off .asset-dark {
        display:block;
    }

    b,strong,dt {
        font-weight:600;
    }

    dfn,dfn,cite,em,i {
        font-style:italic;
    }

    small,.text-sm {
        font-size:18px;
        line-height:27px;
        letter-spacing:-.1px;
    }

    audio,video,canvas {
        display:inline-block;
    }

    template,[hidden],.header-illustration,.header-illustration-image,.site-footer::before,.asset-dark,.lights-off .asset-light {
        display:none;
    }

    li>ul,li>ol,.mb-0,.feature:last-of-type {
        margin-bottom:0;
    }

    dl,p {
        margin-top:0;
        margin-bottom:24px;
    }

    th,.text-left {
        text-align:left;
    }

    th:first-child,td:first-child,.pl-0 {
        padding-left:0;
    }

    th:last-child,td:last-child,.pr-0 {
        padding-right:0;
    }

    .text-light,.text-light a,.lights-off,.lights-off a {
        color:#8595AE;
    }

    .text-light h1,.text-light h2,.text-light h3,.text-light h4,.text-light h5,.text-light h6,.text-light .h1,.text-light .h2,.text-light .h3,.text-light .h4,.text-light .h5,.text-light .h6,.lights-off h1,.lights-off h2,.lights-off h3,.lights-off h4,.lights-off h5,.lights-off h6,.lights-off .h1,.lights-off .h2,.lights-off .h3,.lights-off .h4,.lights-off .h5,.lights-off .h6 {
        color:#fff!important;
    }

    .has-top-divider,.has-bottom-divider,.button-shadow,.hero-copy,.hero-media,.hero-media-container,.cta,.lights-off .has-top-divider,.lights-off .has-bottom-divider {
        position:relative;
    }

    .mb-8,.field-grouped>.control:not(:last-child) {
        margin-bottom:8px;
    }

    .ml-16,.footer-links li+li,.footer-social-links li+li {
        margin-left:16px;
    }

    .mb-24,.footer-brand,.footer-links,.footer-social-links {
        margin-bottom:24px;
    }

    .mb-32,.hero-paragraph,.cta .section-paragraph {
        margin-top: 65px;
        margin-bottom:32px;
        /*font-size: 17px;*/
        font-size: 30px;
        font-family: "Roboto Slab", serif;
        color:#000;
        line-height: 2;
    }

    .p-8,.footer-social-links li a {
        padding:8px;
    }

    .pb-64,.cta .section-inner {
        padding-bottom:64px;
    }

    .input::-webkit-input-placeholder,.textarea::-webkit-input-placeholder,.input:-ms-input-placeholder,.textarea:-ms-input-placeholder,.input::-ms-input-placeholder,.textarea::-ms-input-placeholder,.input::placeholder,.textarea::placeholder {
        color:#6B7A90;
    }

    .input .inline-input,.textarea .inline-textarea {
        display:inline;
        width:auto;
    }

    .header-links,.header-links li,.footer-social-links li {
        display:inline-flex;
    }

    .header-illustration img,.header-illustration svg,.feature-icon img,.feature-icon svg {
        max-width:none;
    }

    .lights-off .hero-media-image,.lights-off .features-box {
        box-shadow:48px 16px 48px rgba(0,0,0,0.2);
    }

    @media min-width 641px{
        h1,.h1 {
            font-size:44px;
            line-height:54px;
            letter-spacing:0;
        }

        h2,.h2 {
            font-size:38px;
            line-height:48px;
            letter-spacing:-.1px;
        }

        .field-grouped>.control {
            flex-shrink:0;
        }

        .field-grouped>.control.control-expanded {
            flex-grow:1;
            flex-shrink:1;
        }

        .field-grouped>.control:not(:last-child) {
            margin-bottom:0;
            margin-right:8px;
        }

        .hero {
            text-align:left;
            padding-top:88px;
            padding-bottom:120px;
        }

        .hero-inner {
            display:flex;
            justify-content:space-between;
        }

        .hero-copy {
            padding-top:40px;
            padding-right:48px;
            min-width:448px;
            max-width:512px;
            z-index:1;
        }

        .hero-title {
            margin-bottom:16px;
        }

        .hero-paragraph {
            margin-bottom:32px;
        }

        .hero-cta {
            display:flex;
            align-items:center;
            margin:0;
        }

        .hero-cta .button:first-child {
            margin-right:32px;
        }

        .header-illustration {
            display:block;
        }

        .hero-media {
            z-index:0;
        }

        .hero-media img,.hero-media svg {
            max-width:none;
        }

        .header-illustration-image {
            display:block;
            position:absolute;
            top:-168px;
            left:-722px;
            width:1440px;
            height:324px;
        }

        .features {
            position:relative;
        }

        .features .section-inner {
            padding-bottom:100px;
        }

        .features .section-paragraph {
            padding-left:72px;
            padding-right:72px;
        }

        .features::before {
            content:'';
            width:100%;
            height:300px;
            position:absolute;
            left:0;
            top:168px;
            background:linear-gradient(tobottom,rgba(83,95,215,0), rgba(83,95,215,0.04));
        }

        .feature {
            text-align:left;
        }

        .feature-icon {
            display:block;
            margin-top:8px;
            margin-right:32px;
            margin-bottom:0;
        }

        .feature-title::after {
            left:0;
        }

        .cta .section-inner {
            padding-bottom:128px;
        }

        .cta .section-paragraph {
            margin-bottom:40px;
            padding-left:72px;
            padding-right:72px;
        }

        .cta::before {
            bottom:0;
        }

        .section-inner {
            padding-top:88px;
            padding-bottom:88px;
        }

        .site-footer-inner {
            justify-content:space-between;
            padding-top:72px;
            padding-bottom:72px;
        }

        .footer-brand,.footer-links,.footer-social-links,.footer-copyright {
            flex:50%;
        }

        .footer-brand,.footer-copyright {
            justify-content:flex-start;
        }

        .footer-links,.footer-social-links {
            justify-content:flex-end;
        }

        .footer-links {
            order:1;
            margin-bottom:0;
        }

        .field-grouped,.feature-inner {
            display:flex;
        }

        .site-header::before,.lights-off .features::before {
            display:none;
        }

        .hero-cta .button,.cta-cta .button {
            min-width:170px;
        }
    }

    @media max-width 640px{
        .h1-mobile {
            font-size:38px;
            line-height:48px;
            letter-spacing:-.1px;
        }

        .h2-mobile {
            font-size:32px;
            line-height:42px;
            letter-spacing:-.1px;
        }

        .h3-mobile {
            font-size:24px;
            line-height:34px;
            letter-spacing:-.1px;
        }

        .h4-mobile,.h5-mobile,.h6-mobile {
            font-size:20px;
            line-height:30px;
            letter-spacing:-.1px;
        }
    }

    @media min-width 481px{
        .container,.container-sm {
            padding-left:24px;
            padding-right:24px;
        }
    }

    @media max-width 639px{
        .hero-cta>*+* {
            margin-top:32px;
        }

        .lights-toggle {
            justify-content:center;
        }

        .hero-cta>*,.cta-cta .button {
            display:flex;
        }
    }
</style>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
        <section class="hero" style="background: #fff;padding: 120px 0px;">
            <div class="container">
                <div class="hero-inner">
                    <div class="hero-copy">
                        <h1 class="hero-title">SALES & MARKETING</h1>
                        <p class="hero-paragraph">Your business is important to us because your growth is our growth</p>
                        <a href="#contact_us" class="btn2">Contact us</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="hero" style="background: #22706d;padding: 100px 0px;">
            <div class="container">
                <div class="hero-inner">
                    <div class="hero-copy">
                        <p class="hero-paragraph" style="color:#fff;">MNI is a round point for entertainment in Rwanda. It has a pick traffic of followers at it’s internet based services such as MNI Selection, MNI Broadcast and it’s social media accompanied by the quarterly entertainment event which is followed by thousands all over the country.</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="hero" style="background: #fff;padding: 100px 0px;">
            <div class="container">
                <div class="hero-inner">
                    <div class="hero-copy">
                        <h1 class="hero-title mt-0">Opportunities</h1>
                        <p class="hero-paragraph">
                             Advertising on website and social media<br>
                            Sponsoring and partnering in our entertainment events<br>
                            Investment<br>
                            Promoting your business<br>
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="hero" style="background: #fd9337;padding: 50px 0px;" id="contact_us">
            <div class="container">
                <div class="hero-inner">
                    <div class="hero-copy">
                        <p class="hero-paragraph" style="color: #fff;">
                            Let us keep in touch with you.
                        </p>
                        <p class="hero-paragraph" style="color: #fff;">
                            Email: info@mni.rw<br>
                            Tel:+250789813910/+250780008883<br>
                        </p>
                    </div>
                </div>
            </div>
        </section>


    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
</div>
@endsection
