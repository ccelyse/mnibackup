@extends('layouts.newmaster')

@section('title', 'MNI Selection - Gutora')


@section('content')
<style>
    .spb-asset-content p{
        color:#fff !important;
    }
    .title-wrap h3{
        color: #fff !important;
    }
    .chart-page .chart-detail-header__chart-name {
        padding-top: 50px !important;
    }
    .modal-backdrop{
        position: relative !important;
    }
    /*.modal-backdrop.show {*/
        /*opacity: .5;*/
    /*}*/

    .modal-dialog {
        max-width: 800px !important;
        margin: 30px auto;
    }
    iframe{
        width: 100% !important;
    }
    #contact-form button {
        /*margin-top: 35px;*/
        width: 100%;
    }
    .social-icon li a i {
         bottom: 0px;
        position: relative;
    }
    #contact-form .form-group input, #contact-form .form-group textarea, #contact-form .form-group select {
        border: 1px solid #fff !important;
        color: #fff !important;
    }
    .single-blog-post a,.single-blog-post p{
        color: #fff;
    }
    .bottom-title{
        font-size: 24px;
        line-height: 34px;
        color: #fff;
        padding: 45px 0 35px;
    }
    .title-box{
        display: inline-block;
        line-height: 35px;
        font-size: 13px;
        text-transform: uppercase;
        color: #fff;
        padding: 0 30px;
        border: 1px solid rgba(255,255,255,0.2);
        border-radius: 3px;
    }
    .alert-success {
        color: #155724;
        background-color: #d4edda;
        border-color: #c3e6cb;
        width: 100%;
        text-align: center;
        margin: 15px;
    }
    .solid-inner-banner .page-title {
        font-size: 18px !important;
        padding-bottom:0px !important;
    }
    .voteshare li a {
        width: 40px;
        line-height: 36px;
        border: 2px solid #fff !important;
        border-radius: 50%;
        text-align: center;
        font-size: 18px;
        color: #fff !important;
        margin-right: 5px;
    }
    .contact-us-section .contact-info ul li a {
        height: 40px;
    }
    #contact-form .form-group input, #contact-form .form-group textarea, #contact-form .form-group select {
        background: #15605d !important;
    }
.page-title{
    color: #fff !important;
}
    .card {
        background-color: transparent !important;
    }
    .md-tabs {
        position: relative;
        z-index: 1;
        padding: .7rem;
        margin-right: 1rem;
        margin-bottom: -20px;
        margin-left: 1rem;
        background-color: #1b706f;
        border: 0;
        border-radius: .25rem;
        -webkit-box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18), 0 4px 15px 0 rgba(0,0,0,0.15);
        box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18), 0 4px 15px 0 rgba(0,0,0,0.15);
        margin-top: 25px;
    }
    .md-tabs .nav-link.active, .md-tabs .nav-item.open .nav-link {
        color: #fff;
        background-color: rgba(0,0,0,0.2);
        border-radius: .25rem;
        -webkit-transition: all 1s;
        transition: all 1s;
    }
    .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
        /*color: #495057;*/
        /*background-color: #fff;*/
        border-color: #1b706f;
    }
    .nav-tabs .nav-item {
        margin-bottom: -1px;
        margin: 0 auto;
    }
    #Checked{
        display: none;
    }

</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script
        src="https://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>


@include('layouts.newtopmenu')

@foreach($contestant as $data)
<div class="solid-inner-banner">
    <div class="bg-shape-holder">
        <span class="big-round-one wow fadeInLeft animated" data-wow-duration="3s"></span>
        <span class="big-round-two wow fadeInLeft animated" data-wow-duration="3s"></span>
        <span class="big-round-three wow fadeInLeft animated" data-wow-duration="3s"></span>
        <span class="shape-one"></span>
        <span class="shape-two"></span>
        <img src="front/images/shape/shape-32.svg" alt="" class="shape-three">
        <span class="shape-four"></span>
    </div>

    <h2 class="page-title" id="page-titles">Ugiye gutora {{$data->names}} ifite amajwi :</h2>
    <div class="col-lg-6 mx-auto" >
        <div class="contact-form" style="padding-bottom: 15px" >
{{--            <form class="form" id="contact-form" action="{{url('VoteNowNumber')}}" method="post">--}}
{{--                {{ csrf_field() }}--}}
{{--                <div class="messages"></div>--}}

{{--                <div class="controls">--}}
{{--                    <div class="form-group">--}}
{{--                        <input id="form_email" type="text" name="song_artist" value="{{$data->song_artist}}" readonly hidden>--}}
{{--                        <input id="form_email" type="text" name="id" value="{{$data->id}}" hidden>--}}
{{--                    </div>--}}

{{--                    <div class="form-group">--}}
{{--                        <input id="form_sub" type="text" name="song_name" value="{{$data->song_name}}" readonly hidden>--}}
{{--                    </div>--}}

{{--                    <div class="form-group">--}}
{{--                        <input id="form_sub" type="text" name="phonenumber" placeholder="numero yawe ya mobile 078xxxxxxx" required>--}}
{{--                    </div>--}}

{{--                    <button class="theme-button-two">Tora</button>--}}
{{--                </div> <!-- /.controls -->--}}

{{--            </form>--}}

            <ul class="nav nav-tabs md-tabs" id="myTabMD" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="limited-tab-md" data-toggle="tab" href="#limited-md" role="tab" aria-controls="limited-md"
                       aria-selected="true">Limited Votes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="unlimited-tab-md" data-toggle="tab" href="#unlimited-md" role="tab" aria-controls="unlimited-md"
                       aria-selected="false">Unlimited Votes</a>
                </li>
            </ul>
            <div class="tab-content card pt-5" id="myTabContentMD">
                <div class="tab-pane fade show active" id="limited-md" role="tabpanel" aria-labelledby="limited-tab-md">
                    <form class="form" action="#" id="contact-form">
                        <div id="messages" style="display: none"></div>
                        <div class="controls">
                            <div class="form-group">
                                <input id="song_artist_1" type="text" name="song_artist" value="{{$data->names}}" readonly hidden>
                                <input id="id_1" type="text" name="id" value="{{$data->id}}" hidden>
                            </div>

                            <div class="form-group">
                                <input id="song_name_1" type="text" name="song_name" value="{{$data->names}}" readonly hidden>
                            </div>

                            <div class="form-group">
                                <input id="phonenumber_1" type="text" name="phonenumber" placeholder="numero yawe ya mobile 078xxxxxxx" required>
                            </div>
                            <button type='button' class="theme-button-two" id="VoteNowUser_1">Tora</button>
                        </div> <!-- /.controls -->

                    </form>
                    <div class="modal fade" id="successmessage_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="CloseVote">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div id="Voting_response_1" style="display: none">
                                        <div id="successmodal_1">
                                        </div>
                                        <img src="front/images/loading-gif-transparent-4.gif" style="max-width: 100px;display: none;margin: 0 auto;" id="loading_1">
                                        <div id="messagestatus_1" style="background: #135c5a;color: #fff;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
{{--                <div class="tab-pane fade" id="unlimited-md" role="tabpanel" aria-labelledby="unlimited-tab-md">--}}
{{--                    <form class="form" action="#" id="contact-form">--}}
{{--                        <div id="messages" style="display: none"></div>--}}
{{--                        <div class="controls">--}}
{{--                            <div class="form-group">--}}
{{--                                <input id="song_artist_" type="text" name="song_artist" value="{{$data->song_artist}}" readonly hidden>--}}
{{--                                <input id="id_" type="text" name="id" value="{{$data->id}}" hidden>--}}
{{--                            </div>--}}

{{--                            <div class="form-group">--}}
{{--                                <input id="song_name_" type="text" name="song_name" value="{{$data->song_name}}" readonly hidden>--}}
{{--                            </div>--}}

{{--                            <div class="form-group">--}}
{{--                                <input id="phonenumber_" type="text" name="phonenumber" placeholder="numero yawe ya mobile 078xxxxxxx" required>--}}
{{--                            </div>--}}

{{--                            <div class="form-group">--}}
{{--                                <select name="amount" id="amount_">--}}
{{--                                    <option value="">Hitamo amafaranga</option>--}}
{{--                                    <option value="250">250 RWF ( 5 Votes)</option>--}}
{{--                                    <option value="500">500 RWF (10 Votes)</option>--}}
{{--                                    <option value="1000">1000 RWF (20 Votes)</option>--}}
{{--                                </select>--}}
{{--                            </div>--}}

{{--                            <button type='button' class="theme-button-two" id="VoteNowUser">Tora</button>--}}
{{--                        </div> <!-- /.controls -->--}}

{{--                    </form>--}}
{{--                    <div class="modal fade" id="successmessage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
{{--                        <div class="modal-dialog" role="document">--}}
{{--                            <div class="modal-content">--}}
{{--                                <div class="modal-header">--}}
{{--                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="CloseVote">--}}
{{--                                        <span aria-hidden="true">&times;</span>--}}
{{--                                    </button>--}}
{{--                                </div>--}}
{{--                                <div class="modal-body">--}}
{{--                                    <div id="Voting_response" style="display: none">--}}
{{--                                        <div id="successmodal">--}}
{{--                                        </div>--}}
{{--                                        <img src="front/images/loading-gif-transparent-4.gif" style="max-width: 100px;display: none;margin: 0 auto;" id="loading">--}}
{{--                                        <div id="messagestatus" style="background: #135c5a;color: #fff;"></div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
            <div class="contact-us-section" style="padding-bottom: 20px;padding-top: 20px; ">
                <div class="col-lg-12">
                    <div class="contact-info">
                        <p style="color: #fff">Share my song on social media</p>
                        <ul class="voteshare">

                            <li><a href="https://www.facebook.com/sharer.php?u=<?php echo url()->full(); ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true" style="position: relative;top: 10px;"></i></a></li>
                            <li><a href="https://twitter.com/share?url=<?php echo url()->full(); ?>&text={{$data->names}}" target="_blank"><i class="fa fa-twitter" aria-hidden="true" style="position: relative;top: 10px;"></i></a></li>
                            <li><a href="https://wa.me/?text=<?php echo url()->full(); ?>" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true" style="position: relative;top: 10px;"></i></a></li>
                            {{--<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>--}}
                        </ul>
                    </div> <!-- /.contact-info -->
                </div>
                    </div> <!-- /.row -->
                </div> <!-- /.container -->
            </div> <!-- /.contact-us-section -->
        </div> <!-- /.contact-form -->
    </div> <!-- /.col- -->
</div> <!-- /.solid-inner-banner -->



<!--
    =============================================
        Contact Us
    ==============================================
    -->
<div class="contact-us-section" style="padding-bottom: 20px;padding-top: 20px; ">
    <div class="container">
        <div class="row">
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif


            <div class="col-lg-6">
                <div class="contact-info">
                    {{--<h2 class="title">Don’t Hesitate to contact with us for any kind of information Ntutinde kutwandikira</h2>--}}
                    <p style="color: #15605e !important; font-weight: bold;">Ugiye Gutora indirimbo, ijwi rimwe ni 50RWF. Wemerewe gushyigikira indirimbo inshuro zose wifuza</p>
                    <p>Muramutse mugize ikibazo mwaduhamagara kuri iyi numero bagufashe</p>
                    <a href="tel:+250780008883" class="call">+250 780 008 883</a>
                    <ul>
                        <li><a href="https://www.facebook.com/MNISelection/"><i class="fa fa-facebook" aria-hidden="true" style="position: relative;top: 10px;"></i></a></li>
                        <li><a href="https://twitter.com/MNISelection"><i class="fa fa-twitter" aria-hidden="true" style="position: relative;top: 10px;"></i></a></li>
                        <li><a href="https://www.instagram.com/MNISelection/"><i class="fa fa-instagram" aria-hidden="true" style="position: relative;top: 10px;"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UC2kHMCoGVRYmaTeG5APFBPA?view_as=subscriber"><i class="fa fa-youtube" aria-hidden="true" style="position: relative;top: 10px;"></i></a></li>
                    </ul>
                </div> <!-- /.contact-info -->
            </div>
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div> <!-- /.contact-us-section -->
<script src="front/vendor/jquery.2.2.3.min.js"></script>
<script>
    $(document).on('click', '#VoteNowUser_1', function() {
        $('#VoteNowUser_1').html('Voting Now..');
        var song_artist = document.getElementById("song_artist_1").value;
        var id = document.getElementById("id_1").value;
        var song_name = document.getElementById("song_name_1").value;
        var phonenumber = document.getElementById("phonenumber_1").value;
        // var amount = document.getElementById("amount_1").value;
        // alert(phonenumber);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: "../api/VoteNowNumber",
            data: {
                'song_artist': song_artist,
                'id': id,
                'song_name': song_name,
                'phonenumber': phonenumber,
                // 'amount': amount,
            },
            dataType: 'json',
            success: function(response) {
                JSON.stringify(response);
                // console.log(response);
                jQuery('#messagestatus_1').show();
                document.getElementById("Voting_response_1").style.display = "block";
                $('#successmodal_1').html('<p style="text-align:center;">' + response.message + '</p>');
                $('#successmessage_1').modal('show');

                document.getElementById("page-titles").style.display = "none";
                $(function(){
                    setInterval(oneSecondFunction, 4000);
                });
                function oneSecondFunction() {
                    $.ajax
                    ({
                        type: "POST",
                        url: "../api/CallBackMomo",
                        data: {
                            transactionid:response.transactionid,
                        },
                        cache: false,
                        success: function (data) {
                            $('#messagestatus_1').html('<p style="text-align:center;">' + data.message + '</p>');
                            if(data.status == "SUCCESSFUL"){
                                document.getElementById("loading_1").style.display = "none";
                                document.getElementById("successmodal_1").style.display = "none";
                                setTimeout(function() {
                                    window.location = 'Thankyou';
                                }, 2000);

                            }else{
                                // jQuery('#messagestatus').show();
                                document.getElementById("messagestatus_1").style.display = "block";
                                document.getElementById("loading_1").style.display = "block";
                            }

                        },
                        error: function(xhr, status, error) {
                            console.log(xhr.responseText);
                        }
                    });
                }
            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    });

    $(document).on('click', '#VoteNowUser', function() {
        $('#VoteNowUser').html('Voting Now..');
        var song_artist = document.getElementById("song_artist_").value;
        var id = document.getElementById("id_").value;
        var song_name = document.getElementById("song_name_").value;
        var phonenumber = document.getElementById("phonenumber_").value;
        var amount = document.getElementById("amount_").value;
        // alert(phonenumber);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: "../api/VoteNowNumberUnlimited",
            data: {
                'song_artist': song_artist,
                'id': id,
                'song_name': song_name,
                'phonenumber': phonenumber,
                'amount': amount,
            },
            dataType: 'json',
            success: function(response) {
                JSON.stringify(response);
                // console.log(response);
                jQuery('#messagestatus').show();
                document.getElementById("Voting_response").style.display = "block";
                $('#successmodal').html('<p style="text-align:center;">' + response.message + '</p>');
                $('#successmessage').modal('show');

                document.getElementById("page-titles").style.display = "none";
                $(function(){
                    setInterval(oneSecondFunction, 4000);
                });
                function oneSecondFunction() {
                    $.ajax
                    ({
                        type: "POST",
                        url: "../api/CallBackMomo",
                        data: {
                            transactionid:response.transactionid,
                        },
                        cache: false,
                        success: function (data) {
                            $('#messagestatus').html('<p style="text-align:center;">' + data.message + '</p>');
                            if(data.status == "SUCCESSFUL"){
                                document.getElementById("loading").style.display = "none";
                                document.getElementById("successmodal").style.display = "none";
                                setTimeout(function() {
                                    window.location = 'Thankyou';
                                }, 2000);

                            }else{
                                // jQuery('#messagestatus').show();
                                document.getElementById("messagestatus").style.display = "block";
                                document.getElementById("loading").style.display = "block";
                            }

                        },
                        error: function(xhr, status, error) {
                            console.log(xhr.responseText);
                        }
                    });
                }
            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    });
    $(document).ready(function () {

        /*Transaction Loading*/


    });
</script>
@endforeach

@include('layouts.newfooter')
@endsection
