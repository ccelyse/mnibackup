@extends('layoutsv2.master')

@section('title', 'TERMS & CONDITIONS FOR MUSIC STAKEHOLDERS')

@section('content')
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
    .terms p{
        line-height: 2;
        color: #000;
        font-family: "Roboto Slab", serif;
    }
</style>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
        <section class="well" style="padding-bottom: 100px;">
            <div class="container">
                <div class="row">
                    <div class="terms">
                        <h1>TERMS & CONDITIONS FOR MUSIC STAKEHOLDERS</h1>
                        <h4 style="padding: 15px 0">Updated 01/07/2020</h4>
                        <p>
                            Welcome to MNI 's page on terms of use for music stakeholders. Before going any further, we would like you to understand that these terms of use are important to us because they are important to you.
                        </p>

                        <h4 style="padding: 15px 0">The purpose of the terms of use</h4>
                        <p>
                            - We appreciate and like what you do<br>
                            - We understand the value of the fans of music<br>
                            - We know the importance of promoting your work<br>
                            - We are honored to be part of your promotion<br>

                        </p>

                        <h4 style="padding: 15px 0">These terms of use for music stakeholders are important for the following reasons:</h4>
                        <p>
                            1. They clarify your rights when you use the services of MNI<br>
                            2. They clarify the rights that you give us when you use MNI services.<br>
                            3. They explain the terms and conditions of use of our services.<br>

                        </p>
                        <p>
                            We ask that you read these Terms of Use and Privacy Policy carefully, and we hope that you will continue to participate in the Rwandan music industry.
                        </p>


                        <h4 style="padding: 15px 0">Introduction</h4>
                        <p>
                            Thank you for your interest in MNI Ltd, in particular the services of music stakeholders, on our website (www.mni.rw) and on other platforms that we will use now or in the future (referred to as "MNI services"), provided to you by MNI (Muzika Nyarwanda Ipande) Ltd.
                        </p>
                        <p>
                            MNI Ltd is a private company that has been granted the right to operate by the responsible government department (RDB) classified under Company Code: 108135642. It has a document of partnership with the Rwandan Music Federation (RMF) on behalf of all Rwandan musicians dated 22/07/2019 and also one from the Rwandan Academy of Language and Culture (RALC) registered 280 / RALC/2019.
                        </p>
                        <p>
                            MNI for music stakeholders is a platform that connects music stakeholders to music fans, business people, private and public institutions and other stakeholders in a variety of ways. Music stakeholders willingly create an account on the website and this means they enter in compliance with MNI's terms of use and privacy policy available here.

                            In the event of a misunderstanding of the terms of use of MNI and the terms of use for music stakeholders, what is stated here will be considered.

                        </p>
                        <p>
                            To enter into this agreement means that you participate in one way or another in the music industry in Rwanda and that you are at least (+16) and if you are under that age you should have someone who represents your interest or your parents.
                        </p>
                        <h4 style="padding: 15px 0">Rights that we give to music stakeholders</h4>
                        <p>
                            MNI gives you the right to create a free account on the website and to use our services according to your category, to change or delete the information you have provided, to promote your services and brands on MNI platforms and to earn revenue if you have a product that can generate revenue on our platforms.
                        </p>
                        <h4 style="padding: 15px 0">Programs of third parties</h4>
                        <p>
                            In line with the performance of MNI services, MNI uses third party programs to provide good services, but we encourage you to read their terms of use and privacy policies to ensure that you agree with them. In the event that you do not agree with the terms of use and privacy policies of third party programs MNI will not be responsible for this.
                        </p>
                        <p>
                            MNI is ready to welcome people's ideas and perspectives in a polite manner, but it is not allowed to use abusive language.
                        </p>
                        <h4 style="padding: 15px 0">
                            Who is considered to be a music stakeholder by MNI Ltd.
                        </h4>
                        <p>
                            It's someone who carries out music-related activities that either promote the Rwandan music industry or do music.
                        </p>
                        <h4 style="padding: 15px 0">Information that we ask you and how it is used</h4>
                        <p>
                            1. Personal information<br>
                            2. Mode of payment to musicians<br>
                            3. Social media that you use<br>
                            4. Your photo<br>
                            5. Summary of Bio<br>

                        </p>
                        <p>
                            The information we request from you is used to confirm your account on our website and is used for payment purposes and to inform you, where necessary, of some information. Your artistic names, social media and song (on the musicians' side) are information that can be viewed on the website, but information related to your real name, payment information, email address, phone number is kept confidential by MNI in order to avoid a fake account that can be created on behalf of any music stakeholder. However, any account holder can publish this information on their own will in their bio or any other possible location on MNI platforms.
                        </p>
                        <p>
                            The account is approved after MNI confirms that the person signing up is the real person.
                        </p>
                        <p><strong>NB: As far as musicians are concerned, once their accounts have been approved, they give MNI the right to use and play all their songs on the MNI platform.</strong></p>
                        <h4 style="padding: 15px 0">Requirements for musicians to display a song on MNI platforms</h4>
                        <p>
                            Musicians whose account has been approved by MNI Ltd can display their song they want on the music chart so that they can be voted on the MNI Selection music chart. They are required to provide the name of the song, the YouTube link, the cover picture, and they must have the right to make a revenue from that song. The musician is allowed to display only one song on the music chart for a period of one quarter (3 months).
                        </p>
                        <h4 style="padding: 15px 0">Benefits of displaying a song in the MNI Selection Music Chart</h4>
                        <p>
                            A song on the MNI Selection Music Chart earns revenue when it is voted by music fans on the MNI website. One vote for a song on the music chart is made at 100 RWF and the musician receives 60% of the 50 RWF after withholding tax and the aggregators charge fees equal to 25 RWF per vote received by the musician. The top 3 songs are awarded by MNI in the MNI Selection Award and are awarded by partners and sponsors.
                        </p>
                        <h4 style="padding: 15px 0">Method and time of payment for the musician</h4>
                        <p>
                            The owner of the song on the MNI Selection Music Chart receives their royalties at the beginning of the following month for the song shown on the MNI Selection no later than 2 weeks after the beginning of the month on the payment details provided by the musician.
                        </p>
                        <h4 style="padding: 15px 0">The rights to the song shown on the MNI platforms in respect of the law</h4>
                        <p>
                            Musicians who display a song on the MNI platforms agree that they possess total rights of getting revenue from that song. In the event that it is reported that the musician does not have the right to display the song on MNI platforms, MNI Ltd shall not be liable for any consequences that may arise as a result of the illegal use/display of any song, but it shall be the responsibility of the person displaying the song.
                        </p>
                        <h4 style="padding: 15px 0">Duration of the song in the MNI Selection music chart</h4>
                        <p>
                            The song displayed in the MNI Selection will be removed at the end of the quarter (Example: Quarter starting in January ends in March). When the quarter ends, all the songs that were shown in the previous quarter will be placed in the archive on the website, but will not be able to be voted. It is the right of the musician to remove the song from the MNI Selection when the quarter is still on or when it's finished, and he can also display the song at any time in the quarter so that music fans can vote again.
                        </p>
                        <h4 style="padding: 15px 0">Specific terms for other music stakeholders who are not musicians</h4>
                        <p>
                            MNI is honored to promote anyone who participates in the music industry in one way or another, and we therefore request that you create an account on our website to promote your work, achievements and who you are on our website. MNI is also ready to partner with you in case you come up with your idea.
                        </p>
                        <h4 style="padding: 15px 0">Conclusions</h4>
                        <p>
                            These are the terms and conditions that we follow, when there is a change to them we inform you, and everyone using our services is encouraged to visit our platforms to check if there is any change.
                        </p>
                        <p>If you have any questions or ideas regarding these terms and conditions, please contact us by email: info@mni.rw or by telephone: +250780008883.</p>

                    </div>

                </div>
            </div>
        </section>
    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
</div>
@endsection
