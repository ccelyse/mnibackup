@extends('layoutsv2.master')

@section('title', 'MN-Muzika Nyarwanda Ipande| Urutonde rw\'Ukwezi|MNI Selection')

@section('content')
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
</style>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
        <section class="well" style="padding-bottom: 100px;">
            <div class="container center">
                <div class="row">
                    @foreach($contestant as $data)
                    <h1>Murakoze gutora <strong>{{$data->names}} agize amajwi: {{$data->votesNumber_}} </strong> <p style="font-size: 100px;margin-top: 40px;">&#x1F64F;</p></h1>
                    <h4><a href="{{$link}}" style="color:#000;    padding: 55px;" class="btn">Kanda hano wongere umutore</a></h4>
                    <h4><a href="{{ route('V2.Contestants',['id'=> $data->id])}}" style="color:#000;">Reba uko ahagaze ku rutonde</a></h4>
                    @endforeach
                </div>
            </div>
        </section>
    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
</div>
@endsection
