@extends('layoutsv2.master')

@section('title', 'StakeHolders')

@section('content')
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
</style>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
        <div class="camera_container">
            <div class="row">
                <div class="col-lg-6">
                    <div id="camera" class="camera_wrap">
                        @foreach($list_slider as $sliders)
                            <div data-src="HomeSlider/{{$sliders->slider_photo}}">
                                <div class="camera_caption fadeIn">
                                    <h3>“{{$sliders->slider_title}}”</h3>
                                    <p>{{$sliders->slider_description}}</p>
                                    <div class="wr">
                                        <div class="link_wr">
                                            <a href="{{'StakeHoldersLogin'}}" class="btn btn_mod1">Sign In</a>
                                        </div>
                                        <a href="{{'StakeHoldersRegister'}}" class="btn">SIgn Up</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
        <section class="well" style="padding-bottom: 15px;">
            <div class="container center">
{{--                <h2>Music StakeHolders</h2>--}}
                <div class="row">
                    <div class="owl-stake">
                        <div class="item">
                            @foreach($artist as $list)
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="custom_wr"><img src="ArtistPhoto/{{$list->picturename}}" alt="">
                                        <div class="wr">
                                            {{--                                        <h3><span>Diana Krall</span></h3>--}}
                                            <h3>“{{$list->artistic_names}}”</h3>
                                            <h6>{{$list->names}}</h6>
{{--                                            <p>{{$list->slider_moto}}</p>--}}
                                            <hr><a href="{{ route('StakeHoldersProfile',['id'=> $list->id])}}" class="btn">read more</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
</div>
@endsection
