@extends('layoutsv2.master')

@section('title', $contestant_title)

@section('content')
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
    @media (max-width: 479px){
        #mobile_first_Add {
            top: 302px !important;
            position: relative;
            padding: 130px 0px !important;
            margin-top: 0px !important;
        }
        .camera_caption{
            display: none !important;
        }
    }
    #Off{
        display: none;
    }
    @media (max-width: 767px){
        #promoted_song {
             margin-top: 0px !important;
            padding: 0px !important;
        }
        .camera_caption{
            display: none !important;
        }
    }

</style>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
        <div class="camera_container">
            <div class="row">
                <div class="col-lg-6">
                    <div id="camera" class="camera_wrap">
                        @foreach($competition_slider as $sliders)
                        <div data-src="HomeSlider/{{$sliders->slider_photo}}">
                            <div class="camera_caption fadeIn">
                                <h3>“{{$sliders->slider_title}}”</h3>
                                <p>{{$sliders->slider_description}}</p>
{{--                                <div class="wr">--}}
{{--                                    <div class="link_wr"><a href="#mni_selection" class="btn btn_mod1">Vote</a>--}}
{{--                                        <a href="{{'Discover'}}" class="btn btn_mod2">Discover</a>--}}
{{--                                        <a href="{{'StakeHoldersLogin'}}" class="btn">Sign In</a>--}}
{{--                                    </div>--}}
{{--                                    <a href="{{'BroadcastV2'}}" class="btn">Listen Music</a>--}}
{{--                                    <a href="{{'StakeHoldersRegister'}}" class="btn">SIgn Up</a>--}}
{{--                                </div>--}}
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>

        @foreach($groups as $group)
        <section class="well3 well3__ins1" style="" id="promoted_song">
            <div class="container">
{{--                <h3 class="center" style="margin-top: 50px;">{{$contestant_title}}</h3>--}}
                <h3 class="center" style="margin-top: 50px;">{{$group->group_name}}</h3>
                    <ul data-player-id="player" class="music-list music-list_mod1 border">
                    <li data-src="audio/sound_1.mp3" class="stopped"></li>
                    <li data-src="audio/sound_1.mp3" class="mod1">
                        <table>
                            <thead>
                            <tr>
                                <th class="position">Pos</th>
                                {{--                                <th class="primary-color lw">LW</th>--}}
                                <th class="artist" style="text-align: left !important;">Artist</th>
                                <th class="label" style="text-align: right !important;">Duration</th>
                                <th class="secondary-color" style="text-align: right !important;">Votes</th>
                                <th class="secondary-color" style="text-align: right !important;">Action</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </li>
                        <?php
                        $json = json_decode($group,true);
                        foreach ($json['ContestantsData'] as $index => $getmusicsmodern) {
                            $numbers = $index +1;
                            $created_at =  $getmusicsmodern['created_at'];
                            $lastTimeLoggedOut = \Illuminate\Support\Carbon::parse($created_at)->diffForHumans();
                            $route = route('V2.ContestantVote',['id'=> $getmusicsmodern['id']]);
                           echo "
                           <li data-src='audio/sound_1.mp3'>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td class='secondary-color numbering'>
                                             {$numbers}
                                        </td>

                                        <td class='artist'>
                                            <a href='#' style='text-decoration: none;'>
                                                <img src='Contestant/{$getmusicsmodern['photo']}' style='width: 100px;' alt='' id=''>
                                            </a>
                                            <p class='secondary-color'><a href='#' style='text-decoration: none;'>{$getmusicsmodern['names']}</a></p>
                                        </td>

                                        <td class='secondary-color' style='color: #f18d04 !important;text-align: center;' id=''>
                                            {$lastTimeLoggedOut}
                                        </td>
                                        <td class='secondary-color' style='color: #000 !important;'>
                                            {$getmusicsmodern['votesNumber_']}
                                        </td>
                                        <td>
                                            <a href='{$route}' id='{$competition_Status}' class='dlw-btn'>Vote Now</a>
                                            <div style='margin-right: 10px;'>
                                                <span>
                                                    <a href='{$getmusicsmodern['youtube']}' target='_blank'>
                                                        <i class='fa fa-youtube' aria-hidden='true' style='font-size: 30px;color:#1c706d;' id='promoted_song_icon'></i>
                                                    </a>
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </li>
                           ";
                        }
                        ?>


                </ul>

            </div>
        </section>
        @endforeach
    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
</div>
@endsection
