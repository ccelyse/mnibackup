@extends('layoutsv2.master')

@section('title', 'MN-Muzika Nyarwanda Ipande| Urutonde rw\'Ukwezi|MNI Selection')

@section('content')
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
</style>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
        <section class="well" style="padding-bottom: 100px;">
            <div class="container center">
                <div class="row">
                    @foreach($getmusicmodern as $data)
                        <?php
                        $votesbe = $data->votesNumber;
                        $newvotes = $votesbe - 1;
                        ?>
                        <p style="font-size: 35px; line-height: initial;color: #1c706e;">Murakoze gutora <strong>{{$data->song_name}}</strong>  ya <strong>{{$data->song_artist}}</strong>  ifite code ya  <strong>{{$data->playlist_code}}</strong> ifite amajwi : <strong><?php echo $newvotes;?></strong>

                    </p>

                    <h4><a href="{{url('Charts')}}" style="color:#000;">Go back to chart</a></h4>
                    @endforeach
                </div>
            </div>
        </section>
    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
</div>
@endsection
