@extends('layoutsv2.master')

@section('title', 'MN-Muzika Nyarwanda Ipande| Urutonde rw\'Ukwezi|MNI Selection')

@section('content')
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
</style>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
        <section class="well" style="padding-bottom: 100px;">
            <div class="container center">
                <div class="row">
                    <h3>Thank you for submitting your info, we are going to reach you either by a call or email within 24 hours for verification and start enjoying generating income on your songs and reaching your fans.</h3>
                </div>
            </div>
        </section>
    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
</div>
@endsection
