@extends('layoutsv2.master')

@section('title', 'Competition')

@section('content')
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
</style>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
        <div class="camera_container">
            <div class="row">
                <div class="col-lg-6">
                    <div id="camera" class="camera_wrap">
                        @foreach($list_slider as $sliders)
                            <div data-src="HomeSlider/{{$sliders->slider_photo}}">
                                <div class="camera_caption fadeIn">
                                    <h3>“{{$sliders->slider_title}}”</h3>
                                    <p>{{$sliders->slider_description}}</p>
{{--                                    <div class="wr">--}}
{{--                                        <div class="link_wr">--}}
{{--                                            <a href="{{'StakeHoldersLogin'}}" class="btn btn_mod1">Sign In</a>--}}
{{--                                        </div>--}}
{{--                                        <a href="{{'StakeHoldersRegister'}}" class="btn">SIgn Up</a>--}}
{{--                                    </div>--}}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
{{--        <section class="well3 well3__ins1" style="">--}}
{{--            <div class="container">--}}
{{--                <div class="col-md-12">--}}
{{--                    <h3 class="center">Available Competitions</h3>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}
        <section class="well" style="padding-bottom: 15px;">
            <div class="container center">
                <h3>Available Competitions</h3>
                <div class="row">
                    <div class="owl-stake">
                        <div class="item">
                            @foreach($competition as $list)
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="custom_wr"><img src="Competition/{{$list->competition_image}}" alt="" style="width: 300px;height: 300px;">
                                        <div class="wr">
                                            <h3>“{{$list->competition_title}}”</h3>
                                            <hr><a href="{{ route('V2.Contestants',['id'=> $list->id])}}" class="btn">VOTE NOW</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
</div>
@endsection
