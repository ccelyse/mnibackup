@extends('layoutsv2.master')

@section('title', 'About Us')

@section('content')
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
    .terms p{
        line-height: 2;
        color: #000;
        font-family: "Roboto Slab", serif;
    }
</style>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
        <section class="well" style="padding-bottom: 100px;">
            <div class="container">
                <div class="row">
                    <div class="terms">
                        <h1>WHO WE ARE</h1>
                        <p>
                            We are Entertainment industry fanatics
                        </p>

                        <h4 style="padding: 15px 0">COMPANY </h4>
                        <p>
                            MNI Ltd was founded on 23/07/2018 in Rwanda as a private limited company with the aim of participating in the development of the creative, artistic and entertainment industries in Rwanda.
                        </p>

                        <h4 style="padding: 15px 0">VISION </h4>
                        <p>
                            To always stand out in the promotion of the creative, artistic and entertainment industries in Rwanda and engage people to enjoy their local artistic work.
                        </p>
                        <h4 style="padding: 15px 0">MISSION</h4>
                        <p>
                            Create opportunities for Rwandan artists to provide them with adequate income (royalties), promote their artistic works and brands and enhance the quality of their artistic works.
                        </p>
                        <p>
                            Using our website, www.mni.rw, requires you to have access to the internet, which means that when you use cellular networks, you will need airtime for data,
                            and when you use wifi, you will not need airtime.
                        </p>
                        <div id="ourbrands">
                            <h4 style="padding: 15px 0">OUR BRANDS </h4>
                            <strong>
                                <p>
                                    1. MNI Selection
                                </p>
                            </strong>
                            <p>
                                MNI Selection is a quarterly national music chart on our website (www.mni.rw) which acts as a  source of income for Rwandan musicians. MNISelection rate songs based on how music fans vote for their songs at a cost of only 100 RWF/ $5, charged by mobile money or credit/debit card and 60  percent of the price is paid to the owner of the song after the collection fee and withholding tax.
                            </p>
                            <strong>
                                <p>2. MNI Fund</p>
                            </strong>

                            <p>
                                MNI Fund is a crowdfunding platform on our website that allows music stakeholders to raise money for their projects once they face the challenge of funding their project.
                            </p>
                            <strong>
                                <p>
                                    3. MNI Broadcast
                                </p>
                            </strong>

                            <p>
                                MNI Broadcast is a daily playlist program that entertains visitors to the MNI website. Every day has a particular playlist, and people comment on how they feel about the playlist and answer some of the general skills questions asked to win different incentives.
                            </p>
                            <strong>
                                <p>
                                    4. MNI Tarama
                                </p>
                            </strong>

                            <p>
                                MNI Tarama is a quarterly event that includes a variety of entertaining activities, such as live performances, dance and the awarding of MNI Selection winners.
                            </p>
                        </div>


                        <strong><p>Partners</p></strong>
                        <img src="front/images/MNI_PARTNERS-min.png" style="width:200px">

                    </div>

                </div>
            </div>
        </section>
    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
</div>
@endsection
