@extends('layoutsv2.master')

@section('title', 'Discover')

@section('content')
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
    @media (max-width: 479px){
        #mobile_first_Add {
            padding-top: 290px !important;
            position: relative;
            /* padding: 75px 0px; */
            margin-top: 0px !important;
            top: 300px !important;
        }
    }
</style>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
        <div class="camera_container">
            <div class="row">
                <div class="col-lg-6">
                    <div id="camera" class="camera_wrap">
                        @foreach($list_slider as $sliders)
                        <div data-src="HomeSlider/{{$sliders->slider_photo}}">
                            <div class="camera_caption fadeIn">
                                <h3>“{{$sliders->slider_title}}”</h3>
                                <p>{{$sliders->slider_description}}</p>
                                <div class="wr">
                                    <div class="link_wr"><a href="#mni_selection" class="btn btn_mod1">Vote</a><a href="#" class="btn btn_mod2">Discover</a></div><a href="#" class="btn">Listen Music</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
        <section class="well" style="padding-bottom: 15px;padding-top: 0px !important;" id="mobile_first_Add">
            <div class="container center">
                <h2>Music StakeHolders</h2>
                <div class="row">
                    <div class="owl-stake">
                        <div class="item">
                            @foreach($list_stake as $list)
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="custom_wr"><img src="HomeSlider/{{$list->slider_photo}}" alt="">
                                    <div class="wr">
{{--                                        <h3><span>Diana Krall</span></h3>--}}
                                        <h3>“{{$list->slider_title}}”</h3>
                                        <h6>{{$list->slider_moto}}</h6>
                                        <p>{{$list->slider_moto}}</p>
                                        <hr>
                                        <a href="{{ route('StakeHolders',['id'=> $list->id])}}" class="btn">read more</a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="well3 bg-primary" style="padding-top: 100px; !important;">
            <div class="container center">
                <div class="row">
                    <div data-wow-delay="0.1s" class="col-xs-4 col-sm-4 wr wow fadeInUp">
                        {{--                        <img src="https://livedemo00.template-help.com/wt_55566/images/page-1_img07.png" alt="">--}}
                        <h2>{{$music_Stake}}</h2>
                        <h3>
                            <span>Music StakeHolders</span>
                        </h3>
                        <hr>
                    </div>

                    <div data-wow-delay="0.3s" class="col-xs-4 col-sm-4 wr wow fadeInUp">
                        {{--                        <img src="https://livedemo00.template-help.com/wt_55566/images/page-1_img08.png" alt="">--}}
                        <h2>{{$songs_}}</h2>
                        <h3><span>Song Displayed</span></h3>
                        <hr>
                    </div>
                    <div data-wow-delay="0.3s" class="col-xs-4 col-sm-4 wr wow fadeInUp">
                        {{--                        <i class="fa fa-money-bill-alt"></i>--}}
                        <h2>{{$funds}} Rwf</h2>
                        <h3><span>Raised Fund</span></h3>
                        <hr>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
</div>
@endsection
