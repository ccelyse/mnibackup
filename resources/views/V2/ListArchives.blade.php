@extends('layoutsv2.master')

@section('title', 'Previous-Chart')

@section('content')
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
    .position_song{
        color: #1c706e;
        position: relative;
        font-size: 50px;
        font-family: "Roboto Slab", serif;
        left: 5px;
        top: 10%;
    }
    .songnumber{
        background: #fff;
        margin-bottom: 5px;
        font-family: "Roboto Slab", serif;
        text-align: center;
        /*border: 1px solid #1c706e;*/
    }
    .justify-content-md-center {
        -ms-flex-pack: center!important;
        justify-content: center!important;
    }
    /*.row {*/
    /*     display: -ms-flexbox;*/
    /*     display: flex;*/
    /*     -ms-flex-wrap: wrap;*/
    /*     !* flex-wrap: wrap; *!*/
    /*     !* margin-right: -15px; *!*/
    /*     !* margin-left: -15px; *!*/
    /* }*/
    @media (max-width: 479px){
        #mobile_first_Add {
            padding-top: 290px !important;
            position: relative;
            /* padding: 75px 0px; */
            margin-top: 0px !important;
            top: 0px !important;
        }
    }

</style>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
        <div class="camera_container">
            <div class="row">
                <div class="col-lg-6">
                    <div id="camera" class="camera_wrap">
                        @foreach($list_slider as $sliders)
                        <div data-src="HomeSlider/{{$sliders->slider_photo}}">
                            <div class="camera_caption fadeIn">
                                <h3>“{{$sliders->slider_title}}”</h3>
                                <p>{{$sliders->slider_description}}</p>
                                <div class="wr">
                                    <div class="link_wr"><a href="#mni_selection" class="btn btn_mod1">Vote</a>
                                        <a href="{{'Discover'}}" class="btn btn_mod2">Discover</a>
                                    </div><a href="{{'BroadcastV2'}}" class="btn">Listen Music</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
        <section class="well3 well3__ins1" id="mobile_first_Add">
            <div class="container">
                <div class="col-md-12">
                    <div class="tab-content" id="ToraIndirimbo" style="padding-top: 30px;">
                        <!-- ^^^^^^^^^^^^^^^^^^^^^ Monthly ^^^^^^^^^^^^^^^^^^^^^^^^^^^ -->

                        <div id="Modern" class="tab-pane fade show active">
                            <div class="row justify-content-md-center">
                                <div class="col-lg-6 col-lg-offset-3" style="padding-top: 15px">

                                    <div class="text-wrapper">
                                        <div id="resultsM">

                                            @foreach($all as $index => $getmusicsmodern)

                                                <div class="row" style="border: 1px solid #1c706f;margin-bottom: 5px;">
                                                    <a style="display: flex;" href="{{ route('V2.Archives',['id'=> $getmusicsmodern->id])}}">
                                                        <div class="col-md-2 songnumber">
                                                            <p class="position_song">{{ $index+1 }}</p>
                                                        </div>
                                                        <div class="col-md-10 songinfo" style="">
                                                            <div class="director-speech clearfix">
                                                                <img src="SongImages/{{$getmusicsmodern->archive_name}}" alt="" class="d-img">
                                                                <div class="bio-block">
                                                                    <h6 class="name" >{{$getmusicsmodern->archive_name}}</h6>
                                                                    <h6 class="name">
                                                    <span style="font-size: 12px; !important;">
                                                        <?php
                                                        $created_at =  $getmusicsmodern->created_at;
                                                        $lastTimeLoggedOut = \Illuminate\Support\Carbon::parse($created_at)->diffForHumans();
                                                        echo $lastTimeLoggedOut;
                                                        ?>
                                                    </span>
                                                                    </h6>

                                                                </div> <!-- /.bio-block -->
                                                            </div> <!-- /.director-speech -->
                                                        </div>
                                                    </a>
                                                </div>

                                            @endforeach
                                        </div>
                                        {{--<div class="text-wrapper md-center">--}}
                                        {{--<a href="" class="title-box" style="text-align: center;">See More</a>--}}
                                        {{--</div> <!-- /.text-wrapper -->--}}
                                        <div id="displayModern">
                                        </div>
                                        {{--{{ $getmusicmodern->links() }}--}}
                                    </div> <!-- /.text-wrapper -->
                                </div>

                            </div>
                        </div> <!-- /#modern -->


                    </div>
                </div>
            </div>
        </section>

    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
</div>
@endsection
