@extends('layoutsv2.master')

@section('title', 'MNI-Muzika Nyarwanda Ipande| Urutonde rw\'Ukwezi|MNI Selection')

@section('content')
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
    @media (max-width: 479px){
        #mobile_first_Add {
            padding-top: 290px !important;
            position: relative;
            /* padding: 75px 0px; */
            margin-top: 0px !important;
            top: 0px !important;
        }
    }
</style>

<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
        <div class="camera_container">
            <div class="row">
                <div class="col-lg-6">
                    <div id="camera" class="camera_wrap">
                        @foreach($list_slider as $sliders)
                            <div data-src="HomeSlider/{{$sliders->slider_photo}}">
                                <div class="camera_caption fadeIn">
                                    <h3>“{{$sliders->slider_title}}”</h3>
                                    <p>{{$sliders->slider_description}}</p>
                                    <div class="link_wr"><a href="#mni_selection" class="btn btn_mod1">Vote</a>
                                        <a href="{{'Discover'}}" class="btn btn_mod2">Discover</a>
                                    </div><a href="{{'BroadcastV2'}}" class="btn">Listen Music</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
        <section class="well3 well3__ins1" style="margin-top: 60px;" id="mobile_first_Add">
            <div class="container">
                <h3 class="center">{{$archive_name}}</h3>
                <ul data-player-id="player" class="music-list music-list_mod1 border">
                    <li data-src="audio/sound_1.mp3" class="stopped"></li>
                    <li data-src="audio/sound_1.mp3" class="mod1">
                        <table>
                            <thead>
                            <tr>
                                <th class="position">Pos</th>
                                <th class="artist">Artist</th>
                                <th class="title">Title</th>
                                <th class="label">Duration</th>
                                <th class="secondary-color">Votes</th>
{{--                                <th class="secondary-color">Action</th>--}}
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </li>
                    @foreach($getmusicmodern as $index => $getmusicsmodern)
                        <li data-src="audio/sound_1.mp3">
                            <table>
                                <tbody>
                                <tr>
                                    <td class="secondary-color numbering">
                                        {{ $index+1 }} <span id="promoted_song_picture">/</span>
                                    </td>
                                    {{--                                    <td class="lw">--}}
                                    {{--                                        <p class="fa-long-arrow-up secondary-color">3</p>--}}
                                    {{--                                    </td>--}}
                                    <td class="artist">
                                        <a href="{{ route('StakeHoldersProfileV2',['id'=> $getmusicsmodern->user_id])}}" style="text-decoration: none;">
                                            <img src="SongImages/{{$getmusicsmodern->song_cover_picture}}" style="width: 100px;" alt="" id="promoted_song_picture">
                                        </a>
                                        <a href="{{ route('StakeHoldersProfileV2',['id'=> $getmusicsmodern->user_id])}}" style="text-decoration: none;">
                                            <p class="secondary-color">{{$getmusicsmodern->song_artist}}</p>
                                        </a>
                                    </td>
                                    <td class="title">
                                        <span class="name secondary-color" style="color: #f18d04 !important;"><a href="{{$getmusicsmodern->song_youtube_link}}">{{$getmusicsmodern->song_name}}</a></span>
                                    </td>
                                    <td class="secondary-color" style="color: #f18d04 !important;">
                                        <?php
                                        $created_at =  $getmusicsmodern->created_at;
                                        $lastTimeLoggedOut = \Illuminate\Support\Carbon::parse($created_at)->diffForHumans();
                                        echo $lastTimeLoggedOut;
                                        ?>
                                    </td>
                                    <td class="secondary-color" style="color: #000 !important;">
                                        <?php
                                        //                                                        $song_id = $getmusicsmodern->id;
                                        //                                                        $votes = \App\Votes::where('voter_status','SUCCESSFUL')->select(DB::raw('count(id) as votes'))
                                        //                                                            ->where('song_id',$song_id)
                                        //                                                            ->where('vote','1')
                                        //                                                            ->value('votes');
                                        //                                                        $newvotes = $votes - 1;
                                        $votesbe = $getmusicsmodern->votesNumber;
                                        $newvotes = $votesbe - 1;
                                        if($newvotes < 0){
                                            echo "0";
                                        }else{
                                            echo "$newvotes";
                                        }
                                        ?>
                                    </td>
                                    <td>
{{--                                        <a href="{{ route('VoteNowV2',['id'=> $getmusicsmodern->id])}}" id="{{$check_status}}" class="dlw-btn">Vote Now</a>--}}
                                        <div style="margin-right: 10px;">
                                            {{--                                            <span><a href="{{$getmusicsmodern->song_youtube_link}}" target="_blank">--}}
                                            {{--                                                    <i class="fa fa-youtube" aria-hidden="true" style="font-size: 30px;color:#1c706d;" id="promoted_song_icon"></i>--}}
                                            {{--                                                </a>--}}
                                            {{--                                            </span>--}}
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </li>
                    @endforeach

                </ul><a href="#" class="btn2">view all</a>
            </div>
        </section>
    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
    <script>

        $(document).ready(function(){
            var limit = 10;
            var start = 0;
            var action = 'inactive';
            function load_country_data(limit, start)
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:"../api/ChartsData",
                    method:"POST",
                    data:{limit:limit, start:start},
                    cache:false,
                    dataType: 'json',
                    success:function(data)
                    {
                        JSON.stringify(data);
                        console.log(data);

                        $('#load_data').append(data);

                        if(data == '')
                        {
                            $('#load_data_message').html("<button type='button' class='btn btn-info'>No Data Found</button>");
                            action = 'active';
                        }
                        else
                        {
                            $('#load_data_message').html("<button type='button' class='btn btn-warning'>Please Wait....</button>");
                            action = "inactive";
                        }
                    }
                });
            }

            if(action == 'inactive')
            {
                action = 'active';
                load_country_data(limit, start);
            }
            $(window).scroll(function(){
                if($(window).scrollTop() + $(window).height() > $("#load_data").height() && action == 'inactive')
                {
                    action = 'active';
                    start = start + limit;
                    setTimeout(function(){
                        load_country_data(limit, start);
                    }, 1000);
                }
            });

        });
    </script>
</div>
@endsection
