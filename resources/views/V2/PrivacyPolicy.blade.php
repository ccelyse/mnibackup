@extends('layoutsv2.master')

@section('title', 'Privacy Policy')

@section('content')
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
    .terms p{
        line-height: 2;
        color: #000;
        font-family: "Roboto Slab", serif;
    }
</style>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
        <section class="well" style="padding-bottom: 100px;">
            <div class="container">
                <div class="row">
                    <div class="terms">
                        <h1>Privacy Policy</h1>
                        <h4 style="padding: 15px 0">Effective date: September 01, 2019</h4>
                        <p>
                            MNI Ltd with its project MNI Selection ("us", "we", or "our") operates the https://www.mni.rw website and the MNI USSD (*611#) mobile application (hereinafter referred to as the "Service").

                            This page informs you of our policies regarding the collection, use, and disclosure of personal data when you use our Service and the choices you have associated with that data.
                        </p>
                        <p>
                            We use your data to provide and improve the Service. By using the Service, you agree to the collection and use of information in accordance with this policy. Unless otherwise defined in this Privacy Policy, the terms used in this Privacy Policy have the same meanings as in our Terms and Conditions.
                        </p>

                        <h4 style="padding: 15px 0">Information Collection And Use</h4>
                        <p>
                            We collect several different types of information for various purposes to provide and improve our Service to you.
                        </p>

                        <h4 style="padding: 15px 0">Types of Data Collected</h4>
                        <h5 style="padding: 15px 0">Personal Data</h5>
                        <p>
                            While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to vote in MNI Selection, contact you or identify you ("Personal Data"). Personally identifiable information may include, but is not limited to:
                        </p>
                        <p>
                            Email address<br>
                            First name and last name<br>
                            Phone number<br>
                            Cookies and Usage Data<br>
                        </p>

                        <h4 style="padding: 15px 0">Usage Data</h4>
                        <p>
                            We may also collect information that your browser sends whenever you visit our Service or when you access the Service by or through a mobile device ("Usage Data").
                        </p>
                        <p>
                            This Usage Data may include information such as your computer's Internet Protocol address (e.g. IP address), browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages, unique device identifiers and other diagnostic data.
                        </p>
                        <p>
                            When you access the Service with a mobile device, this Usage Data may include information such as the type of mobile device you use, your mobile device unique ID, the IP address of your mobile device, your mobile operating system, the type of mobile Internet browser you use, unique device identifiers and other diagnostic data.
                        </p>
                        <h4 style="padding: 15px 0">Tracking & Cookies Data</h4>
                        <p>
                            We use cookies and similar tracking technologies to track the activity on our Service and hold certain information.

                            Cookies are files with small amount of data which may include an anonymous unique identifier. Cookies are sent to your browser from a website and stored on your device. Tracking technologies also used are beacons, tags, and scripts to collect and track information and to improve and analyze our Service.
                        </p>
                        <p>
                            You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Service. You can learn more how to manage cookies in the Browser Cookies Guide.
                        </p>

                        <h4 style="padding: 15px 0">Examples of Cookies we use:</h4>
                        <p>
                            Session Cookies. We use Session Cookies to operate our Service.<br>
                            Preference Cookies. We use Preference Cookies to remember your preferences and various settings.<br>
                            Security Cookies. We use Security Cookies for security purposes.<br>
                        </p>

                        <h4 style="padding: 15px 0">Use of Data</h4>
                        <p>
                            MNI Selection uses the collected data for various purposes:
                        </p>
                        <p>
                            To provide and maintain the Service<br>
                            To notify you about changes to our Service<br>
                            To allow you to participate in interactive features of our Service when you choose to do so<br>
                            To provide customer care and support<br>
                            To provide analysis or valuable information so that we can improve the Service<br>
                            To monitor the usage of the Service<br>
                            To detect, prevent and address technical issues<br>
                        </p>
                        <h4 style="padding: 15px 0">Transfer Of Data</h4>
                        <p>
                            Your information, including Personal Data, may be transferred to — and maintained on — computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.
                        </p>
                        <p>
                            If you are located outside Rwanda and choose to provide information to us, please note that we transfer the data, including Personal Data, to Rwanda and process it there.
                        </p>
                       <p>
                           Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.
                       </p>
                        <p>
                            MNI Selection will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this Privacy Policy and no transfer of your Personal Data will take place to an organization or a country unless there are adequate controls in place including the security of your data and other personal information.
                        </p>

                        <h4 style="padding: 15px 0">Disclosure Of Data</h4>
                        <h5 style="padding: 15px 0">Legal Requirements</h5>
                        <p>
                            MNI Selection may disclose your Personal Data in the good faith belief that such action is necessary to:</p>

                        <p>
                            To comply with a legal obligation <br>
                            To protect and defend the rights or property of MNI Selection<br>
                            To prevent or investigate possible wrongdoing in connection with the Service<br>
                            To protect the personal safety of users of the Service or the public<br>
                            To protect against legal liability<br>
                        </p>

                        <h4 style="padding: 15px 0">Security Of Data</h4>
                        <p>
                            The security of your data is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Data, we cannot guarantee its absolute security.
                        </p>

                        <h4 style="padding: 15px 0">Service Providers</h4>
                        <p>
                            We may employ third party companies and individuals to facilitate our Service ("Service Providers"), to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used.<p>

                        <p>
                            These third parties have access to your Personal Data only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose
                        </p>

                        <h4 style="padding: 15px 0">Links To Other Sites</h4>
                        <p>
                            Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site you visit.
                        </p>
                        <p>
                            We have no control over and assume no responsibility for the content, privacy policies or practices of any third party sites or services.
                        </p>


                        <h4 style="padding: 15px 0">Children's Privacy</h4>
                        <p>
                            Our Service does not address anyone under the age of 18 ("Children").
                        <p>
                            We do not knowingly collect personally identifiable information from anyone under the age of 18. If you are a parent or guardian and you are aware that your Children has provided us with Personal Data, please contact us. If we become aware that we have collected Personal Data from children without verification of parental consent, we take steps to remove that information from our servers
                        </p>

                        <h4 style="padding: 15px 0">Changes To This Privacy Policy</h4>
                        <p>
                            We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page.
                        </p>
                        <p>
                            We will let you know via email and/or a prominent notice on our Service, prior to the change becoming effective and update the "effective date" at the top of this Privacy Policy.
                        </p>
                        <p>
                            You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.
                        </p>

                        <h4 style="padding: 15px 0">Contact Us</h4>
                        <p>
                            If you have any questions about this Privacy Policy, please contact us:
                        <p>
                            By email: info@mni.rw <br>
                            By phone number: +250780008883 <br>
                            By mail: Kigali-Rwanda <br>
                        </p>

                    </div>

                </div>
            </div>
        </section>
    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
</div>
@endsection
