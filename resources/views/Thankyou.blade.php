@extends('layouts.newmaster')

@section('title', 'MNI PLAYLIST')

@section('content')
    <style>
        .spb-asset-content p{
            color:#fff !important;
        }
        .title-wrap h3{
            color: #fff !important;
        }
        .chart-page .chart-detail-header__chart-name {
            padding-top: 50px !important;
        }
        .modal-backdrop{
            position: relative !important;
        }
        /*.modal-backdrop.show {*/
        /*opacity: .5;*/
        /*}*/

        .modal-dialog {
            max-width: 800px !important;
            margin: 30px auto;
        }
        iframe{
            width: 100% !important;
        }

        .single-blog-post a,.single-blog-post p{
            color: #fff;
        }
        .bottom-title{
            font-size: 24px;
            line-height: 34px;
            color: #fff;
            padding: 45px 0 35px;
        }
        .title-box{
            display: inline-block;
            line-height: 35px;
            font-size: 13px;
            text-transform: uppercase;
            color: #fff;
            padding: 0 30px;
            border: 1px solid rgba(255,255,255,0.2);
            border-radius: 3px;
        }
        .alert-success {
            color: #155724;
            background-color: #d4edda;
            border-color: #c3e6cb;
            width: 100%;
            text-align: center;
            margin: 15px;
        }
        .page-title{
            color: #fff !important;
        }
        .urutonde_list{
            background: #166361;
            padding: 10px;
            color: #fff;
            margin: 0 auto;
            display: table;
            margin-top: 20px;
        }
    </style>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>

    @include('layouts.newtopmenu')

    <div class="solid-inner-banner">
        <div class="bg-shape-holder">
            <span class="big-round-one wow fadeInLeft animated" data-wow-duration="3s"></span>
            <span class="big-round-two wow fadeInLeft animated" data-wow-duration="3s"></span>
            <span class="big-round-three wow fadeInLeft animated" data-wow-duration="3s"></span>
            <span class="shape-one"></span>
            <span class="shape-two"></span>
            <img src="front/images/shape/shape-32.svg" alt="" class="shape-three">
            <span class="shape-four"></span>
        </div>
        <h2 class="page-title">Murakoze Gutora</h2>
    </div> <!-- /.solid-inner-banner -->

        <div class="contact-us-section" style="padding-bottom: 20px;padding-top: 20px; ">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12">
                        <div class="contact-info">
                            {{--<h2 class="title">Don’t Hesitate to contact with us for any kind of information Ntutinde kutwandikira</h2>--}}

                            <p style="text-align: center"> Kongera gutora iyi ndirimbo <a href="{{$link}}">kanda gusubira inyuma wongere utore</a> </p>
                            <p><a href="{{url('Urutonde')}}" class="urutonde_list">Reba urutonde</a></p>
                        </div> <!-- /.contact-info -->
                    </div>
                    </div> <!-- /.agn-our-pricing -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /.contact-us-section -->
    @include('layouts.newfooter')
@endsection
