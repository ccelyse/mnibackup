@extends('backend.layout.master')

@section('title', 'MNI')

@section('content')
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <script src="https://cdn.tiny.cloud/1/ox3adxrdeakt9hg7prtc5qbqx39tob02p0hd4gdffhbdesnm/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
        });
    </script>
    <style>
        .tox .tox-notification--warn, .tox .tox-notification--warning {
            background-color: #fffaea;
            border-color: #ffe89d;
            color: #222f3e;
            display: none;
        }
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }
        .btn-primary:hover{
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }

        .btn-secondary{
            color:#fff !important;
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }

        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        iframe{
            width: 100%;
        }
        .tox-tinymce{
            width: 100%;
        }
    </style>
    {{--<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">--}}



    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif
            <div class="content-body">
                <div class="content-body">

                    <section id="form-control-repeater">
                        <div class="row">
                            <div class="col-12">
                                <button type="button" class="btn btn-icon btn-primary btn-min-width mr-1 mb-1"
                                        data-toggle="modal"
                                        data-target="#addarchives">Create fundraising
                                </button>
                                <!-- Modal -->
                                <div class="modal fade text-left" id="addarchives" tabindex="-1"
                                     role="dialog" aria-labelledby="myModalLabel1"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal form-simple" method="POST" action="{{ url('AddFund') }}" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="projectinput1"> Title</label>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="fund_title" required/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="projectinput1"> Amount</label>
                                                                <div class="input-group">
                                                                    <input type="number" class="form-control" name="fund_amount" required/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Photo</label>
                                                                <div class="input-group">
                                                                    <input type="file" class="form-control" name="fund_image" required/>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="projectinput1"> Description is * required * </label>
                                                                <div class="input-group">
                                                                    <textarea class="form-control" id="mytextarea" name="fund_description"></textarea>
{{--                                                                    <textarea class="form-control" name="slider_description"></textarea>--}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" style="margin-top: 20px">
                                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Add fund</button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-12">
                                <div class="card">

                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered dataex-html5-export table-responsive" >
                                                <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Amount</th>
                                                    <th>Raised Amount</th>
                                                    <th>Photo</th>
                                                    <th>Description</th>
                                                    <th>Created BY</th>
                                                    <th>Date Created</th>
                                                    <th>Status</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($list as $data)
                                                    <tr>
                                                        <td>{{$data->fund_title}}</td>
                                                        <td>{{$data->fund_amount}} Rwf</td>
                                                        <td>{{$data->CheckRaisedAmount}} Rwf</td>
                                                        <td><img src="Funds/{{$data->fund_image}}" style="width:100px"></td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#archives{{$data->id}}">View description
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="archives{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <?php
                                                                            $text = $data->fund_description;
                                                                            echo $text;
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>{{$data->name}}</td>
                                                        <td>{{$data->created_at}}</td>
                                                        <td>{{$data->fund_status}}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#edit{{$data->id}}">Edit
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="edit{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('UpdateFund') }}" enctype="multipart/form-data">
                                                                                {{ csrf_field() }}
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1"> Title</label>
                                                                                            <div class="input-group">
                                                                                                <input type="text" class="form-control" name="fund_title" value="{{$data->fund_title}}"/>
                                                                                                <input type="text" class="form-control" name="id" value="{{$data->id}}" hidden/>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1"> Amount</label>
                                                                                            <div class="input-group">
                                                                                                <input type="number" class="form-control" name="fund_amount" value="{{$data->fund_amount}}"/>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Photo</label>
                                                                                            <div class="input-group">
                                                                                                <input type="file" class="form-control" name="fund_image"/>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1"> Description</label>
                                                                                            <div class="input-group">
                                                                                                <textarea class="form-control" id="mytextarea" name="fund_description">{{$data->fund_description}}</textarea>
                                                                                                {{--                                                                    <textarea class="form-control" name="slider_description"></textarea>--}}
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <img src="Funds/{{$data->fund_image}}" style="width:100%">
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group" style="margin-top: 20px">
                                                                                            <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Add fund</button>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                            </form>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><a href="{{ route('backend.DeleteFund',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary" onclick="return confirm('Are you sure you would like to delete this this fund?');">Delete</a></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/jszip.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/pdfmake.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/vfs_fonts.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.html5.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.print.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.colVis.min.js"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js"></script>
@endsection
