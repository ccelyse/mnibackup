<style>
    .main-menu.menu-light .navigation>li.open>a {
        color: #545766;
        background: #f5f5f5;
        border-right: 4px solid #6b442b !important;
    }
</style>
<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">

        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <?php
            $user_id = \Auth::user()->id;
            $Userrole = \App\User::where('id',$user_id)->value('role');

            $CreateAccount = url('CreateAccount');
            $AccountList = url('AccountList');
            $ArtistDashboard = url('ArtistDashboard');
            $Dashboard = url('Dashboard');
            $ArtistsAccounts = url('ArtistsAccounts');
            $ArtistAddSong = url('ArtistAddSong');
            $PlaylistCategory = url('PlaylistCategory');
            $Playlist = url('Playlist');
            $ListNominee = url('ListNominee');
            $ArtistsAccountsInfo = url('ArtistsAccountsInfo');
            $ArtistsAccountsPending = url('ArtistsAccountsPending');
            $ArtistsAccountsRejected = url('ArtistsAccountsRejected');
            $ArtistRegistrationUpdate = url('ArtistRegistrationUpdate');
            $PlaylistCompleted = url('PlaylistCompleted');
            $PlaylistRejected = url('PlaylistRejected');
            $AllSongs = url('AllSongs');
            $AllArchived = url('AllArchived');
            $MobileMoney = url('MobileMoney');
            $Homeslider = url('AdminHomeSlider');
            $BroadCast = url('BroadCast');
            $MusicStakeHolders = url('MusicStakeHolders');
            $Fundings = url('Fundings');
            $FundMobileMoney = url('FundMobileMoney');
            $BroadCastTopic = url('BroadCastTopic');
            $FundsSlider = url('FundsSlider');
            $FundingUser = url('FundingsUser');
            $DashboardUser = url('DashboardUser');
            $competition_dashboard = url('CompetitionDashboard');
            $AddCompetition = url('AddCompetition');
            $CompetitionSlider = url('CompetitionSlider');
            $Contestant = url('Contestants');
            $CompetitionMobileMoney = url('CompetitionMobileMoney');

            switch ($Userrole) {
                case "Admin":
                    echo "<li class=' nav-item'><a href='$Dashboard'><i class='fas fa-volume-up'></i><span class='menu-title' data-i18n='nav.dash.main'>Dashboard</span></a> ";
                    echo "<li class=' nav-item'><a href='#'><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.templates.main'>Account</span></a> <ul class='menu-content'> <li class=' nav-item'><a href='$CreateAccount'><i class='fas fa-user-alt'></i><span class='menu-title' data-i18n='nav.dash.main'>Create Account</span></a> </li><li class=' nav-item'><a href='$AccountList'><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.dash.main'>Account List</span></a> </li></ul> </li>";
                    echo "<li class=' nav-item'><a href='#'><i class='fas fa-users'></i><span class='menu-title' data-i18n='nav.templates.main'>Artist Account</span></a> <ul class='menu-content'> <li class=' nav-item'><a href='$ArtistsAccounts'><i class='fas fa-user-alt'></i><span class='menu-title' data-i18n='nav.dash.main'>Approved Account</span></a> </li><li class=' nav-item'><a href='$ArtistsAccountsPending'><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.dash.main'>Pending Account</span></a> </li><li class=' nav-item'><a href='$ArtistsAccountsRejected'><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.dash.main'>Rejected Account</span></a> </li></ul> </li>";
//                    echo "<li class=' nav-item'><a href='$ArtistsAccounts'><i class='fas fa-users'></i><span class='menu-title' data-i18n='nav.dash.main'>Artist Accounts</span></a>";
//                    echo "<li class=' nav-item'><a href='$PlaylistCategory'><i class='fas fa-music'></i><span class='menu-title' data-i18n='nav.dash.main'>Selection Category</span></a>";
                    echo "<li class=' nav-item'><a href='$AllSongs'><i class='fas fa-music'></i><span class='menu-title' data-i18n='nav.dash.main'>All Songs</span></a> ";
                    echo "<li class=' nav-item'><a href='$AllArchived'><i class='fas fa-file-archive'></i><span class='menu-title' data-i18n='nav.dash.main'>Archived  Songs</span></a> ";
                    echo "<li class=' nav-item'><a href='$MobileMoney'><i class='fas fa-money-bill'></i><span class='menu-title' data-i18n='nav.dash.main'>Mobile Money</span></a> ";
                    echo "<li class=' nav-item'><a href='$CompetitionMobileMoney'><i class='fas fa-money-bill'></i><span class='menu-title' data-i18n='nav.dash.main'>Competition Momo</span></a> ";
                    echo "<li class=' nav-item'><a href=''><i class='fas fa-drum'></i><span class='menu-title' data-i18n='nav.templates.main'>Selection</span></a> <ul class='menu-content'> <li class=' nav-item'><a href='$Playlist'><i class='fas fa-user-alt'></i><span class='menu-title' data-i18n='nav.dash.main'>Pending Payment</span></a> </li><li class=' nav-item'><a href='$PlaylistRejected'><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.dash.main'>Rejected Payment</span></a> </li><li class=' nav-item'><a href='$PlaylistCompleted'><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.dash.main'>Completed Payment</span></a> </li></ul> </li>";
                    echo "<li class=' nav-item'><a href='$Homeslider'><i class='fas fa-money-bill'></i><span class='menu-title' data-i18n='nav.dash.main'>HomeSlider</span></a> ";
                    echo "<li class=' nav-item'><a href='$BroadCast'><i class='fas fa-money-bill'></i><span class='menu-title' data-i18n='nav.dash.main'>BroadCast Slider</span></a> ";
                    echo "<li class=' nav-item'><a href='$BroadCastTopic'><i class='fas fa-money-bill'></i><span class='menu-title' data-i18n='nav.dash.main'>BroadCast Topic</span></a> ";
                    echo "<li class=' nav-item'><a href='$MusicStakeHolders'><i class='fas fa-money-bill'></i><span class='menu-title' data-i18n='nav.dash.main'>Music StakeHolders</span></a> ";
                    echo "<li class=' nav-item'><a href='$AddCompetition'><i class='fas fa-chart-line'></i><span class='menu-title' data-i18n='nav.dash.main'>Add/View Competitions</span></a>";
                    echo "<li class=' nav-item'><a href='$Fundings'><i class='fas fa-money-check-alt'></i><span class='menu-title' data-i18n='nav.dash.main'>Fundings</span></a> ";
                    echo "<li class=' nav-item'><a href='$FundsSlider'><i class='fas fa-money-check-alt'></i><span class='menu-title' data-i18n='nav.dash.main'>Funds Slider</span></a> ";
                    echo "<li class=' nav-item'><a href='$FundMobileMoney'><i class='fas fa-money-check-alt'></i><span class='menu-title' data-i18n='nav.dash.main'>Fund MobileMoney</span></a> ";

                    break;

                case "Artist":
                    echo "<li class=' nav-item'><a href='$ArtistDashboard'><i class='fas fa-volume-up'></i><span class='menu-title' data-i18n='nav.dash.main'>Dashboard</span></a> ";
                    echo "<li class=' nav-item'><a href='$ArtistAddSong'><i class='fas fa-music'></i><span class='menu-title' data-i18n='nav.dash.main'>Add/View Songs</span></a>";
                    echo "<li class=' nav-item'><a href='$ArtistRegistrationUpdate'><i class='fas fa-users'></i><span class='menu-title' data-i18n='nav.dash.main'>Account Settings</span></a> ";
                    echo "<li class=' nav-item'><a href='$FundingUser'><i class='fas fa-users'></i><span class='menu-title' data-i18n='nav.dash.main'>Create Fund</span></a> ";

                    break;

                case "Editor":
                    echo "<li class=' nav-item'><a href='$DashboardUser'><i class='fas fa-volume-up'></i><span class='menu-title' data-i18n='nav.dash.main'>Dashboard</span></a> ";
                    echo "<li class=' nav-item'><a href='$ArtistsAccountsPending'><i class='fas fa-music'></i><span class='menu-title' data-i18n='nav.dash.main'>Pending Account</span></a>";

                    break;

                case "Competition":
                    echo "<li class=' nav-item'><a href='$competition_dashboard'><i class='fas fa-volume-up'></i><span class='menu-title' data-i18n='nav.dash.main'>Dashboard</span></a> ";
                    echo "<li class=' nav-item'><a href='$AddCompetition'><i class='fas fa-chart-line'></i><span class='menu-title' data-i18n='nav.dash.main'>Add/View Competitions</span></a>";
                    echo "<li class=' nav-item'><a href='$CompetitionSlider'><i class='fas fa-sliders-h'></i><span class='menu-title' data-i18n='nav.dash.main'>Competition Slider</span></a>";
//                    echo "<li class=' nav-item'><a href='$Contestant'><i class='fas fa-sliders-h'></i><span class='menu-title' data-i18n='nav.dash.main'>Contestant</span></a>";
//                    echo "<li class=' nav-item'><a href='$ArtistRegistrationUpdate'><i class='fas fa-users'></i><span class='menu-title' data-i18n='nav.dash.main'>Account Settings</span></a> ";

                    break;

                default:
                    return redirect()->back();
                    break;
            }
            ?>

        </ul>
    </div>
</div>


