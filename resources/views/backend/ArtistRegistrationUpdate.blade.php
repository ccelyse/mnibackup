@extends('backend.layout.master')

@section('title', 'MNI SELECTION')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/plugins/forms/wizard.css">
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary:hover{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }

        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #b4753c  !important;
            border-color:#b4753c  !important;
        }
        .btn-primary:hover{
            background-color: #b4753c !important;
            border-color:#b4753c !important;
        }
        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        iframe{
            width: 100%;
        }

    </style>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('.multi-field-wrapper').each(function() {
                var $wrapper = $('.multi-fields', this);
                $(".add-field", $(this)).click(function(e) {
                    $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                });
                $('.multi-field .remove-field', $wrapper).click(function() {
                    if ($('.multi-field', $wrapper).length > 1)
                        $(this).parent('.multi-field').remove();
                });
            });
        });
    </script>
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')


    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <div class="content-body">
                    <!-- Basic Summernote start -->
                    <div class="card-header" style="margin-bottom: 30px">
                        <h4 class="card-title">Account Settings</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <h4 class="card-title" id="basic-layout-form">Account Info</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="number-tab-steps wizard-circle" method="POST" action="{{ url('UpdateArtistInfo') }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <!-- Step 1 -->
                                            {{--<h6>Beneficiary Information</h6>--}}
                                            <h6 style="font-weight: bold;">Personal Information</h6>
                                            @foreach($accountdetails as $data)

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Names</label>
                                                            <input type="text" id="projectinput1" class="form-control" placeholder="Elysee CONFIANCE"
                                                                   name="names" value="{{$data->names}}" required>
                                                            <input type="text" id="projectinput1" class="form-control"
                                                                   name="id" value="{{$data->id}}" hidden>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Artistic Names</label>
                                                            <input type="text" id="projectinput1" class="form-control" placeholder="BIMABOY"
                                                                   name="artistic_names" value="{{$data->artistic_names}}" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Mobile number</label>
                                                            <input type="number" id="projectinput1" class="form-control" placeholder="0782384772"
                                                                   name="artist_number" value="{{$data->artist_number}}" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" pattern="[+]{1}[0-9]{12}" maxlength="10" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Email</label>
                                                            <input type="email" id="projectinput1" class="form-control" placeholder="elysee@bimaboy.rw"
                                                                   name="artist_email" value="{{$data->artist_email}}" required>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">ID no/Valid passport no</label>
                                                            <input type="text" id="projectinput1" class="form-control"
                                                                   name="artist_ID" value="{{$data->artist_ID}}" required>

                                                        </div>
                                                    </div>
                                                </div>
                                            <!-- Step 2 -->
                                            <h6 style="font-weight: bold;">Mode of Payment</h6>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Mode of Payment</label>
                                                            <select class="select2 form-control" name="artist_modeofpayment" onchange="ModeOfPayment(this);" required>
                                                                {{--<option value="{{ old('artist_modeofpayment') }}">{{ old('artist_modeofpayment') }}</option>--}}
                                                                {{--<option value="Select mode of payment" selected>Select mode of payment</option>--}}
                                                                <option value="{{$data->artist_modeofpayment}}" selected>{{$data->artist_modeofpayment}}</option>
                                                                <option value="Mobile Money">Mobile Money</option>
                                                                <option value="Bank account">Bank account</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" id="MobileMoney">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Number to receive payment</label>
                                                            <input type="number" id="projectinput1" class="form-control" placeholder="0782384772"
                                                                   name="artist_mobilemoney_number" value="{{$data->artist_mobilemoney_number}}" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" pattern="[+]{1}[0-9]{12}" maxlength="10">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Name on the number that must receive payment</label>
                                                            <input type="text" id="projectinput1" class="form-control" placeholder="Elysee CONFIANCE"
                                                                   name="artist_mobilemoney_names" value="{{$data->artist_mobilemoney_names}}" >

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" id="Bank" style="display: none">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Account number(write correctly)</label>
                                                            <input type="text" id="projectinput1" class="form-control"
                                                                   name="artist_bankaccount_number" value="{{$data->artist_bankaccount_number}}" >

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Name on the account</label>
                                                            <input type="text" id="projectinput1" class="form-control"
                                                                   name="artist_bankaccount_names" value="{{$data->artist_bankaccount_names}}" >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Bank name</label>
                                                            <input type="text" id="projectinput1" class="form-control"
                                                                   name="artist_bank_name" value="{{$data->artist_bank_name}}" >
                                                        </div>
                                                    </div>
                                                </div>
                                            <!-- Step 3 -->
                                            <h6 style="font-weight: bold;">Social Media</h6>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Facebook profile link</label>
                                                            <input type="text" id="projectinput1" class="form-control"
                                                                   name="artist_facebook" value="{{$data->artist_facebook}}" required="false">

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Instagram profile link</label>
                                                            <input type="text" id="projectinput1" class="form-control"
                                                                   name="artist_instagram" value="{{$data->artist_instagram}}" required="false">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Twitter profile link</label>
                                                            <input type="text" id="projectinput1" class="form-control"
                                                                   name="artist_twitter" value="{{$data->artist_twitter}}"required="false">

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Youtube Channel</label>
                                                            <input type="text" id="projectinput1" class="form-control"
                                                                   name="artist_youtube" value="{{$data->artist_youtube}}"required="false">

                                                        </div>
                                                    </div> <!-- /.col- -->
                                                    <div class="col-md-12">
                                                        <label>Description</label>
                                                        <div class="form-group">
                                                            <textarea  class="form-control" name="artist_description">{{$data->artist_description}}</textarea>
                                                        </div> <!-- /.input-group -->
                                                    </div> <!-- /.col- -->
                                                </div>

                                                <h6 style="font-weight: bold;">Change Password</h6>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Password</label>
                                                            <input type="password" class="form-control"
                                                                   name="artist_password" id="artist_password" value="{{ old('artist_password') }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Confirm Password</label>
                                                            <input type="password" class="form-control"
                                                                   name="confirm_artist_password" id="confirm_artist_password" value="{{ old('confirm_artist_password') }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="error" style="color:red"></span><br />
                                                        </div>
                                                    </div>
                                                </div>
                                                {{--<div class="row">--}}
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label for="projectinput1">Password</label>--}}
                                                            {{--<input type="text" id="projectinput1" class="form-control"--}}
                                                                   {{--name="artist_password" id="artist_password">--}}

                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label for="projectinput1">Confirm Password</label>--}}
                                                            {{--<input type="text" id="projectinput1" class="form-control"--}}
                                                                   {{--name="confirm_artist_password" id="confirm_artist_password">--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}

                                                {{--<img src="ArtistPhoto/{{$data->picturename}}" style="width:50%">--}}
                                                <div class="form-actions">
                                                    <button type="submit" id="reset"  class="btn btn-login">
                                                        <i class="la la-check-square-o"></i> Update
                                                    </button>
                                                </div>

                                        @endforeach
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script type="application/javascript">
                        function ModeOfPayment(that) {
                            if (that.value == "Bank account") {
                                document.getElementById("Bank").style.display = "flex";
                                document.getElementById("MobileMoney").style.display = "none";
                            } else {
                                document.getElementById("MobileMoney").style.display = "flex";
                                document.getElementById("Bank").style.display = "none";
                            }
                        }
                        $(function(){
                            //on keypress
                            $('#confirm_artist_password').keyup(function(e){
                                //get values
                                var artist_password = $('#artist_password').val();
                                var confirm_artist_password = $(this).val();

                                //check the strings
                                if(artist_password == confirm_artist_password){
                                    //if both are same remove the error and allow to submit
                                    $('.error').text('Password are matching');
                                }else{
                                    //if not matching show error and not allow to submit
                                    $('.error').text('Password not matching');
                                }
                            });

                            //jquery form submit
                        });
                    </script>

                </div>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>


    <script src="backend/app-assets/vendors/js/extensions/jquery.steps.min.js"></script>
    <script src="backend/app-assets/js/scripts/forms/wizard-steps.js"></script>
    {{--<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/forms/wizard-steps.js"></script>
    <script src="backend/app-assets/vendors/js/extensions/jquery.steps.min.js"></script>
@endsection
