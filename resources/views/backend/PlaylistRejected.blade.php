@extends('backend.layout.master')

@section('title', 'MNI')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }
        .btn-primary:hover{
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }

        .btn-secondary{
            color:#fff !important;
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }

        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        iframe{
            width: 100%;
        }

    </style>
    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif
            <div class="content-body">
                <div class="content-body">
                    <div class="card-header" style="margin-bottom: 30px">
                        <h4 class="card-title">MNI payment pending</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <section id="form-control-repeater">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">

                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Month Name</th>
                                                    <th>Song Artist</th>
                                                    <th>Song Name</th>
                                                    <th>Song Youtube Video</th>
                                                    <th>Song Cover Picture</th>
                                                    <th>Song Votes</th>
                                                    <th>Song Amount</th>
                                                    <th>Song Code</th>
                                                    <th>Song Payment Status</th>
                                                    <th>Starting Date</th>
                                                    <th>Ending Date</th>

                                                    <th>Date Created</th>
                                                    {{--<th>Edit</th>--}}
                                                    <th>Approve Payment</th>
                                                    <th>Delete</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($listplaylist as $data)
                                                    <tr>
                                                        <td>{{$data->playlist_name}}</td>
                                                        <td>{{$data->song_artist}}</td>
                                                        <td>{{$data->song_name}}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#video{{$data->id}}">Youtube Video
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="video{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <?php
                                                                            $video = $data->song_youtube_link;
                                                                            echo $video;
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><img src="SongImages/{{$data->song_cover_picture}}" style="width:100%"></td>
                                                        <td> <?php
                                                            $votes = \App\Votes::select(DB::raw('count(id) as votes'))
                                                                ->where('voter_artist_song',$data->song_name)
                                                                ->where('voter_artist',$data->song_artist)
                                                                ->where('vote','1')
                                                                ->value('votes');

                                                            if ($votes ==0){
                                                                echo "0";
                                                            }else{
                                                                echo "$votes";
                                                            }
                                                            ?></td>
                                                        <td>
                                                            <?php
                                                            $amount  = $votes * 25;
                                                            echo $amount ;
                                                            ?>
                                                            FRW
                                                        </td>

                                                        <td>{{$data->playlist_code}}</td>
                                                        <td>{{$data->playlist_payment_status}}</td>
                                                        <td>{{$data->starting_date}}</td>
                                                        <td>{{$data->ending_date}}</td>
                                                        <td>{{$data->created_at}}</td>

                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#claimpayment{{$data->id}}">Approve Payment
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="claimpayment{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel1">Approve Payment</h4>
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('ClaimPaymentAdmin') }}" enctype="multipart/form-data">
                                                                                {{ csrf_field() }}</h4>
                                                                                <div class="form-body">
                                                                                    {{--<h4 class="form-section"><i class="fas fa-map-marker-alt"></i>Song Information</h4>--}}
                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Payment status</label>
                                                                                                <select id="projectinput1" class="form-control" name="payment_status">
                                                                                                    {{--<option value="Payment Being is Processed">Payment Being Processed</option>--}}
                                                                                                    <option value="Payment Rejected">Payment Rejected</option>
                                                                                                    <option value="Payment completed">Payment completed</option>
                                                                                                </select>
                                                                                                <input type="text" id="projectinput1" class="form-control" value="{{$data->id}}"
                                                                                                       name="id" hidden>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                                                                </div>


                                                                            </form>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><a href="{{ route('backend.DeletePlaylist',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary" onclick="return confirm('Are you sure you would like to delete this this song?');">Delete</a></td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>



                </div>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>

    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <script src="backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/jszip.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/pdfmake.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/vfs_fonts.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.html5.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.print.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.colVis.min.js"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js"></script>

@endsection