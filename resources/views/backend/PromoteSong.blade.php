@extends('backend.layout.master')

@section('title', 'MNI')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>

    <style>
        #payment_,#promotion_period_{
            display: none;
        }
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }
        .btn-primary:hover{
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }

        .btn-secondary{
            color:#fff !important;
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }

        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        iframe{
            width: 100%;
        }
        #contact-form .form-group input, #contact-form .form-group select {
            height: 60px;
            padding: 0 25px;
        }
        #contact-form .form-group input, #contact-form .form-group textarea, #contact-form .form-group select {
            border: 1px solid #ebebeb;
            width: 100%;
            max-width: 100%;
            color: #989ca2;
            background: #ffffff;
        }

        input, textarea {
            outline: none;
            box-shadow: none;
            transition: all 0.3s ease-in-out;
        }
    </style>
    {{--<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">--}}



    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif
            <div class="content-body">
                <div class="content-body">
                    <section id="form-control-repeater">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">

                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Song Artist</th>
                                                    <th>Song Name</th>
                                                    <th>Song Cover Picture</th>
                                                    <th>Song Code</th>
                                                    <th>Promotion Status</th>
                                                    <th>Promotion Period</th>
                                                    <th>Created At</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($getmusicmodern as $data)
                                                    <tr>
                                                        <td>{{$data->song_artist}}</td>
                                                        <td>{{$data->song_name}}</td>
                                                        <td><img src="SongImages/{{$data->song_cover_picture}}" style="width:100px"></td>
                                                        <td>{{$data->playlist_code}}</td>
                                                        <td>{{$data->promotion_status}}</td>
                                                        <td>{{$data->promotion_period}}</td>
                                                        <td>{{$data->created_at}}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#archives{{$data->id}}">Promote
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="archives{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
{{--                                                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('PromoteStatus') }}" enctype="multipart/form-data">--}}
{{--                                                                                {{ csrf_field() }}--}}
                                                                                <div class="row">
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Status</label>
                                                                                            <div class="input-group">
                                                                                                <select name="promotion_status" class="form-control" id="promotion_status">
                                                                                                    <option value="{{$data->promotion_status}}" selected>{{$data->promotion_status}}</option>
                                                                                                    <option value="Promoted">Promoted</option>
                                                                                                    <option value="Not Promoted">Not Promoted</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6" id="promotion_period_">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Period</label>
                                                                                            <div class="input-group">
                                                                                                <select name="promotion_period" class="form-control" id="promotion_period">
{{--                                                                                                    <option value="{{$data->promotion_status}}" selected>{{$data->promotion_status}}</option>--}}
                                                                                                    <option>Select Period</option>
                                                                                                    <option value="1">1 Week (10,000)</option>
                                                                                                    <option value="2">2 Weeks (20,000)</option>
                                                                                                    <option value="3">3 Weeks (30,000)</option>
                                                                                                    <option value="4">4 Weeks (40,000)</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6" id="payment_">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Phone number</label>
                                                                                            <div class="input-group">
                                                                                                <input type="number" name="phonenumber" id="phonenumber" class="form-control">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div id="Voting_response" style="display: none">
                                                                                            <div id="successmodal">
                                                                                            </div>
                                                                                            <img src="front/images/loading-gif-transparent-4.gif" style="max-width: 100px;display: block;margin: 0 auto;" id="loading">
                                                                                            <div id="messagestatus" style="background: #135c5a;color: #fff;">

                                                                                            </div>
                                                                                        </div>
                                                                                        <div id="messagestatus_not_promoted" style="background: #135c5a;color: #fff;">

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <input type="text" class="form-control" name="song_id" id="song_id" value="{{$data->id}}" hidden/>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group" style="margin-top: 20px">
{{--                                                                                            <button type="submit" class="btn btn-primary" id="ChangeStatus"> <i class="la la-check-square-o"></i> Promote</button>--}}
                                                                                            <button class="btn btn-primary" id="ChangeStatus"> <i class="la la-check-square-o"></i> Promote</button>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </div>
    </div>

    <script type="application/javascript">
        $(document).on('change', '#promotion_status', function() {
            var promotion_status =$('#promotion_status').val();
            if(promotion_status == "Promoted"){
                document.getElementById("payment_").style.display = "block";
                document.getElementById("promotion_period_").style.display = "block";
            }else{
                document.getElementById("payment_").style.display = "none";
                document.getElementById("promotion_period_").style.display = "none";
            }

        });
        $(document).on('click', '#ChangeStatus', function() {
            $('#ChangeStatus').html('Changing Promotion Status..');
            var promotion_status = document.getElementById("promotion_status").value;
            var promotion_period = document.getElementById("promotion_period").value;
            var phonenumber = document.getElementById("phonenumber").value;
            var song_id = document.getElementById("song_id").value;
            // alert(promotion_status);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "../api/PromoteStatus",
                data: {
                    'promotion_status': promotion_status,
                    'promotion_period': promotion_period,
                    'phonenumber': phonenumber,
                    'song_id': song_id,
                    // 'amount': amount,
                },
                dataType: 'json',
                success: function(response) {
                    JSON.stringify(response);
                    // console.log(response);
                    // jQuery('#messagestatus').show();
                    if(response.status_promoted == "promoted"){
                        document.getElementById("Voting_response").style.display = "block";
                        $('#successmodal').html('<p style="text-align:center;">' + response.message + '</p>');

                        $(function(){
                            setInterval(oneSecondFunction, 4000);
                        });
                        function oneSecondFunction() {
                            $.ajax
                            ({
                                type: "POST",
                                url: "../api/CallBackPromotedMomo",
                                data: {
                                    transactionid:response.transactionid,
                                },
                                cache: false,
                                success: function (data) {
                                    $('#messagestatus').html('<p style="text-align:center;">' + data.message + '</p>');
                                    if(data.status == "SUCCESSFUL"){
                                        document.getElementById("loading").style.display = "none";
                                        document.getElementById("successmodal").style.display = "none";
                                        setTimeout(function() { window.location.reload() }, 5000);
                                    }else{
                                        // jQuery('#messagestatus').show();
                                        document.getElementById("messagestatus").style.display = "block";
                                        document.getElementById("loading").style.display = "block";
                                    }

                                },
                                error: function(xhr, status, error) {
                                    console.log(xhr.responseText);
                                }
                            });
                        }
                    }else{
                        console.log(response.message);
                        // document.getElementById("Voting_response").style.display = "block";
                        $('#messagestatus_not_promoted').html('<p style="text-align:center;">' + response.message + '</p>');
                        document.getElementById("ChangeStatus").style.display = "none";
                        setTimeout(function() { window.location.reload() }, 5000);
                    }

                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                }
            });
        });

    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('.modernSearch input[type="text"]').on("keyup input", function() {
                /* Get input value on change */
                var inputValM = $(this).val();
//                alert(inputValM);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "SearchVotingM",
                    data: {
                        'modernSearch': inputValM,

                    },
                    success: function(response) {
                        JSON.stringify(response); //to string
                        $("#displayModern").html(response);
                        // if (response.response_status == 400) {
                        //     $('#Song_information').append('');
                        // } else {
                        //     var $i = 1;
                        //     $.each(response, function (key, value) {
                        //         console.log(value);
                        //         $("#Song_information").append("" +
                        //             // '<td>' + value.song_artist + '</td>' +
                        //             // '<td>' + value.song_name + '</td>' +
                        //             // '<td>' + value.song_cover_picture + '</td>' +
                        //             // '<td>' + value.votesNumber + '</td>' +
                        //             // '<td>' + value.playlist_code + '</td>' +
                        //             // '<td>' + value.created_at + '</td>' +
                        //             // // '<td><button type="button" class="btn btn-success btn-circle action_btn edit-modal" data-toggle="modal" data-id="' + this.id + '" data-target="#exampleModal"> <i class="fas fa-edit"></i> </button></td>' +
                        //             // // '<td><button type="button" class="btn btn-danger btn-circle action_btn delete" id="' + this.id + '"> <i class="fas fa-trash"></i> </button></td>' +
                        //             // '</tr>'
                        //         '<td></td>' +
                        //         '<td></td>' +
                        //         '<td></td>' +
                        //         '<td></td>' +
                        //         '<td></td>' +
                        //         '<td></td>' +
                        //         // '<td><button type="button" class="btn btn-success btn-circle action_btn edit-modal" data-toggle="modal" data-id="' + this.id + '" data-target="#exampleModal"> <i class="fas fa-edit"></i> </button></td>' +
                        //         // '<td><button type="button" class="btn btn-danger btn-circle action_btn delete" id="' + this.id + '"> <i class="fas fa-trash"></i> </button></td>' +
                        //         '</tr>'
                        //         );
                        //     });
                        //     /*
                        //     |--------------------------------------------
                        //     | Load Data Table
                        //     |--------------------------------------------
                        //     */
                        //     var table = $('#provincesTable').DataTable();
                        // }
                    },
                    error: function(xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            });
        });

    </script>

   <script type="application/javascript">

   </script>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/jszip.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/pdfmake.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/vfs_fonts.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.html5.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.print.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.colVis.min.js"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js"></script>
@endsection
