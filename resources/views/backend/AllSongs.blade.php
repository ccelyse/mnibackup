@extends('backend.layout.master')

@section('title', 'MNI')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>

    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }
        .btn-primary:hover{
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }

        .btn-secondary{
            color:#fff !important;
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }

        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        iframe{
            width: 100%;
        }
        #contact-form .form-group input, #contact-form .form-group select {
            height: 60px;
            padding: 0 25px;
        }
        #contact-form .form-group input, #contact-form .form-group textarea, #contact-form .form-group select {
            border: 1px solid #ebebeb;
            width: 100%;
            max-width: 100%;
            color: #989ca2;
            background: #ffffff;
        }

        input, textarea {
            outline: none;
            box-shadow: none;
            transition: all 0.3s ease-in-out;
        }
    </style>
    {{--<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">--}}



    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif
            <div class="content-body">
                <div class="content-body">
                    <section id="form-control-repeater">
                        <div class="row">
                            <div class="col-12">
                                <div class="form modernSearch" id="contact-form" style="margin-top: 10px">
                                    <div class="controls">
                                        <div class="form-group">
                                            <input id="modernSearch" type="text" name="modernSearch" placeholder="search">
                                        </div>
                                    </div> <!-- /.controls -->
                                </div>
                                <div id="displayModern">
                                </div>

{{--                                    <table class="table table-striped table-bordered dataex-html5-export" id="provincesTable">--}}
{{--                                    <thead>--}}
{{--                                    <tr>--}}
{{--                                        <th>Song Artist</th>--}}
{{--                                        <th>Song Name</th>--}}
{{--                                        <th>Song Cover Picture</th>--}}
{{--                                        <th>Total Votes</th>--}}
{{--                                        <th>Song Code</th>--}}
{{--                                        <th>Date Created</th>--}}
{{--                                    </tr>--}}
{{--                                    </thead>--}}
{{--                                    --}}
{{--                                    </table>--}}

                                <form class="form-horizontal form-simple" method="POST" action="{{ url('AllSongsFilter') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1">Start date</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="ft-calendar"></i></span>
                                                    </div>
                                                    <input type="date" class="form-control dp-month-year" name="startdate" value="" required/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1">End date</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="ft-calendar"></i></span>
                                                    </div>
                                                    <input type="date" class="form-control dp-month-year" name="enddate" value="" required/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group" style="margin-top: 20px">
                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Filter</button>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="col-12">
                                <div class="card">

                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Month Name</th>
                                                    <th>Mode of Payment</th>
                                                    <th>Payment Number</th>
                                                    <th>Total Votes</th>
                                                    <th>Monthly Votes</th>
                                                    <th>Song Artist</th>
                                                    <th>Song Name</th>
                                                    <th>Song Youtube Video</th>
                                                    <th>Song Cover Picture</th>
                                                    <th>Pending Votes</th>
                                                    <th>Successful Votes</th>
                                                    <th>Song Amount</th>
                                                    <th>Song Code</th>
                                                    <th>Song Payment Status</th>
                                                    <th>Starting Date</th>
                                                    <th>Ending Date</th>
                                                    <th>Date Created</th>
                                                    {{--<th>Edit</th>--}}
                                                    {{--<th>Approve Payment</th>--}}
                                                    <th>Archives</th>
                                                    <th>Delete</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($listplaylist as $data)
                                                    <tr>
                                                        <td>{{$data->playlist_name}}</td>
                                                        <td>
                                                            <?php
                                                            $pay = $data->payment;
                                                            $newpay = str_replace('"', "", $pay);
                                                            echo $newpay;
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $pay = $data->paymentNumber;
                                                            $newpay = str_replace('"', "", $pay);
                                                            echo $newpay;
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $votesbe = $data->votesNumber;
                                                            $newvotes = $votesbe - 1;
                                                            if($newvotes < 0){
                                                                echo "0";
                                                            }else{
                                                                echo "$newvotes";
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $votesbe = $data->votesNumberMon;
                                                            $newvotes = $votesbe - 1;
                                                            if($newvotes < 0){
                                                                echo "0";
                                                            }else{
                                                                echo "$newvotes";
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>{{$data->song_artist}}</td>
                                                        <td>{{$data->song_name}}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#video{{$data->id}}">Youtube Video
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="video{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <?php
                                                                            $video = $data->song_youtube_link;
                                                                            echo $video;
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><img src="SongImages/{{$data->song_cover_picture}}" style="width:100%"></td>
                                                        <td> <?php
//                                                            $votes = \App\Votes::select(DB::raw('count(id) as votes'))
//                                                                ->where('voter_artist_song',$data->song_name)
//                                                                ->where('voter_artist',$data->song_artist)
////                                                                ->where('vote','1')
//                                                                ->where('voter_status','PENDING')
////                                                                ->value('votes');
//                                                                ->sum('vote');
                                                            $votes = \App\Votes::where('voter_artist_song',$data->song_name)
                                                                ->where('voter_artist',$data->song_artist)
//                                                                ->where('vote','1')
                                                                ->where('voter_status','PENDING')
//                                                                ->value('votes');
                                                                ->sum('vote');

                                                            if ($votes ==0){
                                                                echo "0";
                                                            }else{
                                                                echo "$votes";
                                                            }
                                                            ?>
                                                        </td>
                                                        <td> <?php
//                                                            $votes = \App\Votes::select(DB::raw('count(id) as votes'))
//                                                                ->where('voter_artist_song',$data->song_name)
//                                                                ->where('voter_artist',$data->song_artist)
//                                                                ->where('vote','1')
//                                                                ->where('voter_status','SUCCESSFUL')
//                                                                ->value('votes');

                                                            $votes = \App\Votes::where('voter_artist_song',$data->song_name)
                                                                ->where('voter_artist',$data->song_artist)
//                                                                ->where('vote','1')
                                                                ->where('voter_status','SUCCESSFUL')
//                                                                ->value('votes')
                                                                ->sum('vote');

                                                            $newvotes = $votes - 1;

                                                            if ($newvotes ==0){
                                                                echo "0";
                                                            }else{
                                                                echo "$newvotes";
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $amount  = ($votes*25)-25;
                                                            echo $amount ;
                                                            ?>
                                                            FRW
                                                        </td>

                                                        <td>{{$data->playlist_code}}</td>
                                                        <td>{{$data->playlist_payment_status}}</td>
                                                        <td>{{$data->starting_date}}</td>
                                                        <td>{{$data->ending_date}}</td>
                                                        <td>{{$data->created_at}}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#archives{{$data->id}}">Archives
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="archives{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('AddArchivesSong') }}" enctype="multipart/form-data">
                                                                                {{ csrf_field() }}
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Archive Name</label>
                                                                                            <div class="input-group">
                                                                                                <select name="archive_id" class="form-control">
                                                                                                @foreach($archive as $datas)
                                                                                                    <option value="{{$datas->id}}">{{$datas->archive_name}}</option>
                                                                                                @endforeach
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <input type="text" class="form-control" name="song_id" value="{{$data->id}}" hidden/>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group" style="margin-top: 20px">
                                                                                            <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Archives</button>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                            </form>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><a href="{{ route('backend.DeletePlaylist',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary" onclick="return confirm('Are you sure you would like to delete this this song?');">Delete</a></td>
                                                    </tr>
                                                @endforeach
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){
            $('.modernSearch input[type="text"]').on("keyup input", function() {
                /* Get input value on change */
                var inputValM = $(this).val();
//                alert(inputValM);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "SearchVotingM",
                    data: {
                        'modernSearch': inputValM,

                    },
                    success: function(response) {
                        JSON.stringify(response); //to string
                        $("#displayModern").html(response);
                        // if (response.response_status == 400) {
                        //     $('#Song_information').append('');
                        // } else {
                        //     var $i = 1;
                        //     $.each(response, function (key, value) {
                        //         console.log(value);
                        //         $("#Song_information").append("" +
                        //             // '<td>' + value.song_artist + '</td>' +
                        //             // '<td>' + value.song_name + '</td>' +
                        //             // '<td>' + value.song_cover_picture + '</td>' +
                        //             // '<td>' + value.votesNumber + '</td>' +
                        //             // '<td>' + value.playlist_code + '</td>' +
                        //             // '<td>' + value.created_at + '</td>' +
                        //             // // '<td><button type="button" class="btn btn-success btn-circle action_btn edit-modal" data-toggle="modal" data-id="' + this.id + '" data-target="#exampleModal"> <i class="fas fa-edit"></i> </button></td>' +
                        //             // // '<td><button type="button" class="btn btn-danger btn-circle action_btn delete" id="' + this.id + '"> <i class="fas fa-trash"></i> </button></td>' +
                        //             // '</tr>'
                        //         '<td></td>' +
                        //         '<td></td>' +
                        //         '<td></td>' +
                        //         '<td></td>' +
                        //         '<td></td>' +
                        //         '<td></td>' +
                        //         // '<td><button type="button" class="btn btn-success btn-circle action_btn edit-modal" data-toggle="modal" data-id="' + this.id + '" data-target="#exampleModal"> <i class="fas fa-edit"></i> </button></td>' +
                        //         // '<td><button type="button" class="btn btn-danger btn-circle action_btn delete" id="' + this.id + '"> <i class="fas fa-trash"></i> </button></td>' +
                        //         '</tr>'
                        //         );
                        //     });
                        //     /*
                        //     |--------------------------------------------
                        //     | Load Data Table
                        //     |--------------------------------------------
                        //     */
                        //     var table = $('#provincesTable').DataTable();
                        // }
                    },
                    error: function(xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            });
        });

    </script>

   <script type="application/javascript">

   </script>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/jszip.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/pdfmake.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/vfs_fonts.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.html5.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.print.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.colVis.min.js"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js"></script>
@endsection
