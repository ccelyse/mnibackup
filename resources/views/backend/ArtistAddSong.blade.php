@extends('backend.layout.master')

@section('title', 'MNI')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <style>
        #payment_,#promotion_period_{
            display: none;
        }
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary:hover{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }

        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #b4753c  !important;
            border-color:#b4753c  !important;
        }
        .btn-primary:hover{
            background-color: #b4753c !important;
            border-color:#b4753c !important;
        }
        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        iframe{
            width: 100%;
        }

    </style>
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('.multi-field-wrapper').each(function() {
                var $wrapper = $('.multi-fields', this);
                $(".add-field", $(this)).click(function(e) {
                    $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                });
                $('.multi-field .remove-field', $wrapper).click(function() {
                    if ($('.multi-field', $wrapper).length > 1)
                        $(this).parent('.multi-field').remove();
                });
            });
        });
    </script>
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')


    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <div class="content-body">
                    <div class="card-header" style="margin-bottom: 30px">
                        <h4 class="card-title">Add Song</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Basic Summernote start -->
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif

                                    <h4 class="card-title" id="basic-layout-form">Selection Info</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddPlaylist') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="fas fa-music"></i>Song Info</h4>
                                                {{--<div class="row">--}}
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label for="date12">Starting Date</label>--}}
                                                            {{--<input type="date" class="form-control" name="starting_date" value="{{ old('starting_date') }}" id="date12">--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label for="date12">Ending Date</label>--}}
                                                            {{--<input type="date" class="form-control" name="ending_date" value="{{ old('ending_date') }}" id="date12">--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                <div class="row">
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label for="projectinput1">Month Name</label>--}}
                                                            {{--<input type="text" id="projectinput1" class="form-control" value="{{ old('playlist_name') }}"--}}
                                                                   {{--name="playlist_name" placeholder="January Playlist" required>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-12">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label for="projectinput1">Selection Category</label>--}}
                                                            {{--<select id="projectinput1" class="form-control" name="playlist_cate">--}}

                                                                {{--@foreach($listcate as $cate)--}}
                                                                {{--<option value="{{$cate->id}}">{{$cate->category}}</option>--}}
                                                                {{--@endforeach--}}
                                                            {{--</select>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                </div>

                                               <div class="multi-field-wrapper">
                                                    <div class="multi-fields">
                                                        <div class="row  multi-field">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Song Artist</label>
                                                                    <input type="text" id="projectinput1" class="form-control" value="<?php echo "$get_artistname";?>"
                                                                           name="song_artist" required readonly>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Song Name</label>
                                                                    <input type="text" id="projectinput1" class="form-control" value="{{ old('song_name') }}"
                                                                           name="song_name" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Song youtube link</label>
                                                                    <input type="text" id="projectinput1" class="form-control" value="{{ old('song_youtube_link') }}"
                                                                           name="song_youtube_link" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Song Cover Picture(<strong>Image should be Png, Jpg or Jpeg</strong>)</label>
                                                                    <input type="file" id="projectinput1" class="form-control"
                                                                           name="song_cover_picture">
                                                                </div>
                                                                @if ($errors->has('song_cover_picture'))
                                                                    <span class="help-block">
                                                                         <strong style="color: red;">{{ $errors->first('song_cover_picture') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                            </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section id="form-control-repeater">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">

                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered zero-configuration table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Month Name</th>
                                                    <th>Song Code</th>
                                                    <th>Song Payment Status</th>
                                                    <th>Starting Date</th>
                                                    <th>Ending Date</th>
                                                    <th>Song Artist</th>
                                                    <th>Song Name</th>
                                                    <th>Song Youtube Video</th>
                                                    <th>Song Cover Picture</th>
                                                    <th>Date Created</th>
                                                    <th>Edit</th>
                                                    <th>Claim Payment</th>
                                                    <th>Promotion Status</th>
                                                    <th>Promotion Period</th>
                                                    <th>Promote Song</th>
                                                    <th>Delete</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($listplaylist as $data)
                                                    <tr>
                                                        <td>{{$data->playlist_name}}</td>
                                                        <td>{{$data->playlist_code}}</td>
                                                        <td>{{$data->playlist_payment_status}}</td>
                                                        <td>{{$data->starting_date}}</td>
                                                        <td>{{$data->ending_date}}</td>
                                                        <td>{{$data->song_artist}}</td>
                                                        <td>{{$data->song_name}}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#video{{$data->id}}">Youtube Video
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="video{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <?php
                                                                                $video = $data->song_youtube_link;
                                                                                echo $video;
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><img src="SongImages/{{$data->song_cover_picture}}" style="width:100%"></td>
                                                        <td>{{$data->created_at}}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#editplaylist{{$data->id}}">Edit Song
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="editplaylist{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel1">Edit Song</h4>
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('EditPlaylist') }}" enctype="multipart/form-data">
                                                                                {{ csrf_field() }}</h4>
                                                                                <div class="form-body">
                                                                                    <h4 class="form-section"><i class="fas fa-map-marker-alt"></i>Song Information</h4>
                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Song Category</label>
                                                                                                <select id="projectinput1" class="form-control" name="playlist_cate">
                                                                                                    {{--<option value="{{$cate->id}}">{{$cate->category}}</option>--}}
                                                                                                    <option value="{{$data->playlist_cate}}">{{$data->category}}</option>
                                                                                                    @foreach($listcate as $cate)
                                                                                                        <option value="{{$cate->id}}">{{$cate->category}}</option>
                                                                                                    @endforeach
                                                                                                </select>
                                                                                                <input type="text" id="projectinput1" class="form-control" value="{{$data->id}}"
                                                                                                       name="id" hidden>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-6" hidden>
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Starting Date</label>
                                                                                                <input type="text" id="projectinput1" class="form-control" value="{{$data->starting_date}}"
                                                                                                       name="starting_date" >
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6" hidden>
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Ending Date</label>
                                                                                                <input type="text" id="projectinput1" class="form-control" value="{{$data->ending_date}}"
                                                                                                       name="ending_date" >
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6" hidden>
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Code</label>
                                                                                                <input type="text" id="projectinput1" class="form-control" value="{{$data->playlist_code}}"
                                                                                                       name="playlist_code" >
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6" hidden>
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Payment status</label>
                                                                                                <input type="text" id="projectinput1" class="form-control" value="{{$data->playlist_payment_status}}"
                                                                                                       name="playlist_payment_status">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6" hidden>
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Playlist Name</label>
                                                                                                <input type="text" id="projectinput1" class="form-control" value="{{$data->playlist_name}}"
                                                                                                       name="playlist_name" >
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Song Artist</label>
                                                                                                <input type="text" id="projectinput1" class="form-control" value="{{$data->song_artist}}"
                                                                                                       name="song_artist" required>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Song Name</label>
                                                                                                <input type="text" id="projectinput1" class="form-control" value="{{$data->song_name}}"
                                                                                                       name="song_name" required>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Song youtube link</label>
                                                                                                <input type="text" id="projectinput1" class="form-control" value="{{$data->song_youtube_link}}"
                                                                                                       name="song_youtube_link" required>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Song Cover Picture</label>
                                                                                                <input type="file" id="projectinput1" class="form-control"
                                                                                                       name="song_cover_picture">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12">
                                                                                            <?php
                                                                                            $video = $data->song_youtube_link;
                                                                                            echo $video;
                                                                                            ?>
                                                                                            <img src="SongImages/{{$data->song_cover_picture}}" style="width:100%">
                                                                                        </div>
                                                                                    </div>

                                                                                        <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                                                                </div>


                                                                            </form>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td><a href="{{ route('backend.ClaimPayment',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Claim Payment</a></td>
                                                        <td>{{$data->promotion_status}}</td>
                                                        <td>{{$data->promotion_period}}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#archives{{$data->id}}">Promote
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="archives{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            {{--                                                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('PromoteStatus') }}" enctype="multipart/form-data">--}}
                                                                            {{--                                                                                {{ csrf_field() }}--}}
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Status</label>
                                                                                        <div class="input-group">
                                                                                            <select name="promotion_status" class="form-control" id="promotion_status">
                                                                                                <option value="{{$data->promotion_status}}" selected>{{$data->promotion_status}}</option>
                                                                                                <option value="Promoted">Promoted</option>
                                                                                                <option value="Not Promoted">Not Promoted</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6" id="promotion_period_">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Period</label>
                                                                                        <div class="input-group">
                                                                                            <select name="promotion_period" class="form-control" id="promotion_period">
                                                                                                {{--                                                                                                    <option value="{{$data->promotion_status}}" selected>{{$data->promotion_status}}</option>--}}
                                                                                                <option>Select Period</option>
                                                                                                <option value="1">1 Week (10,000) Rwf</option>
                                                                                                <option value="2">2 Weeks (20,000) Rwf</option>
                                                                                                <option value="3">3 Weeks (30,000) Rwf</option>
                                                                                                <option value="4">4 Weeks (40,000) Rwf</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6" id="payment_">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Phone number</label>
                                                                                        <div class="input-group">
                                                                                            <input type="number" name="phonenumber" id="phonenumber" class="form-control">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <div id="Voting_response" style="display: none">
                                                                                        <div id="successmodal">
                                                                                        </div>
                                                                                        <img src="front/images/loading-gif-transparent-4.gif" style="max-width: 100px;display: block;margin: 0 auto;" id="loading">
                                                                                        <div id="messagestatus" style="background: #135c5a;color: #fff;">

                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="messagestatus_not_promoted" style="background: #135c5a;color: #fff;">

                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <input type="text" class="form-control" name="song_id" id="song_id" value="{{$data->id}}" hidden/>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group" style="margin-top: 20px">
                                                                                        {{--                                                                                            <button type="submit" class="btn btn-primary" id="ChangeStatus"> <i class="la la-check-square-o"></i> Promote</button>--}}
                                                                                        <button class="btn btn-primary" id="ChangeStatus"> <i class="la la-check-square-o"></i> Promote</button>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><a href="{{ route('backend.DeletePlaylist',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary" onclick="return confirm('Are you sure you would like to delete this this song?');">Delete</a></td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>


                </div>
            </div>
        </div>
    </div>
    <script type="application/javascript">
        $(document).on('change', '#promotion_status', function() {
            var promotion_status =$('#promotion_status').val();
            if(promotion_status == "Promoted"){
                document.getElementById("payment_").style.display = "block";
                document.getElementById("promotion_period_").style.display = "block";
            }else{
                document.getElementById("payment_").style.display = "none";
                document.getElementById("promotion_period_").style.display = "none";
            }

        });
        $(document).on('click', '#ChangeStatus', function() {
            $('#ChangeStatus').html('Changing Promotion Status..');
            var promotion_status = document.getElementById("promotion_status").value;
            var promotion_period = document.getElementById("promotion_period").value;
            var phonenumber = document.getElementById("phonenumber").value;
            var song_id = document.getElementById("song_id").value;
            // alert(promotion_status);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "../api/PromoteStatus",
                data: {
                    'promotion_status': promotion_status,
                    'promotion_period': promotion_period,
                    'phonenumber': phonenumber,
                    'song_id': song_id,
                    // 'amount': amount,
                },
                dataType: 'json',
                success: function(response) {
                    JSON.stringify(response);
                    // console.log(response);
                    // jQuery('#messagestatus').show();
                    if(response.status_promoted == "promoted"){
                        document.getElementById("Voting_response").style.display = "block";
                        $('#successmodal').html('<p style="text-align:center;">' + response.message + '</p>');

                        $(function(){
                            setInterval(oneSecondFunction, 4000);
                        });
                        function oneSecondFunction() {
                            $.ajax
                            ({
                                type: "POST",
                                url: "../api/CallBackPromotedMomo",
                                data: {
                                    transactionid:response.transactionid,
                                },
                                cache: false,
                                success: function (data) {
                                    $('#messagestatus').html('<p style="text-align:center;">' + data.message + '</p>');
                                    if(data.status == "SUCCESSFUL"){
                                        document.getElementById("loading").style.display = "none";
                                        document.getElementById("successmodal").style.display = "none";
                                        setTimeout(function() { window.location.reload() }, 2000);
                                    }else{
                                        // jQuery('#messagestatus').show();
                                        document.getElementById("messagestatus").style.display = "block";
                                        document.getElementById("loading").style.display = "block";
                                    }

                                },
                                error: function(xhr, status, error) {
                                    console.log(xhr.responseText);
                                }
                            });
                        }
                    }else{
                        console.log(response.message);
                        // document.getElementById("Voting_response").style.display = "block";
                        $('#messagestatus_not_promoted').html('<p style="text-align:center;">' + response.message + '</p>');
                        document.getElementById("ChangeStatus").style.display = "none";
                        setTimeout(function() { window.location.reload() }, 5000);
                    }

                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                }
            });
        });

    </script>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
@endsection
