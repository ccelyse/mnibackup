@extends('layouts.newmaster')

@section('title', 'MNI Selection')

@section('content')
    <style>
        .spb-asset-content p{
            color:#fff !important;
        }
        .title-wrap h3{
            color: #fff !important;
        }
        .chart-page .chart-detail-header__chart-name {
            padding-top: 50px !important;
        }
        .modal-backdrop{
            position: relative !important;
        }
        /*.modal-backdrop.show {*/
        /*opacity: .5;*/
        /*}*/

        .modal-dialog {
            max-width: 800px !important;
            margin: 30px auto;
        }
        iframe{
            width: 100% !important;
        }

        .single-blog-post a,.single-blog-post p{
            color: #fff;
        }
        .bottom-title{
            font-size: 24px;
            line-height: 34px;
            color: #fff;
            padding: 45px 0 35px;
        }
        .title-box{
            display: inline-block;
            line-height: 35px;
            font-size: 13px;
            text-transform: uppercase;
            color: #fff;
            padding: 0 30px;
            border: 1px solid rgba(255,255,255,0.2);
            border-radius: 3px;
        }
        .alert-success {
            color: #155724;
            background-color: #d4edda;
            border-color: #c3e6cb;
            width: 100%;
            text-align: center;
            margin: 15px;
        }
        .theme-title-three p {
            font-size: 17px !important;
            line-height: 35px !important;
            color: #000;
            padding-top: 10px !important;
        }
        .page-title{
            color: #fff !important;
        }
        .our-service .our-history .text-wrapper {
            padding: 80px 0 80px 0px !important;
        }
        .theme-list-item li {
            color: #fff;
        }
        .theme-list-item li i, .theme-list-item li span {
            color: #fff;
        }
        .our-service .our-history .text-wrapper p {
            color: #000 !important;
        }
        #theme-banner-five {
            /* background: linear-gradient( 145deg, rgb(132,0,252) 0%, rgb(17,222,244) 100%); */
            /* background: linear-gradient( 145deg, rgb(5, 54, 50) 0%, rgb(28, 112, 111) 100%); */
            position: relative;
            z-index: 5;
            background: url(./front/images/audience-band-celebration-2240771-compressed.jpg) !important;
            background-size: cover !important;
        }
    </style>
    @include('layouts.newArtisttopmenu')



    <!--
              =============================================
                  Signup Page
              ==============================================
              -->
    <div class="signUp-page signUp-minimal pt-50 pb-100" style="padding-top: 120px;">
        <div class="shape-wrapper">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div> <!-- /.shape-wrapper -->
        <div class="signin-form-wrapper">
            <div class="title-area text-center">
                <h3>RESET YOUR PASSWORD.</h3>
            </div> <!-- /.title-area -->

                 <div class="row">
                        <div class="col-md-12">
                            <h4>We have sent you a password recovery email.</h4>
                        </div> <!-- /.col- -->
                </div> <!-- /.row -->
        </div> <!-- /.sign-up-form-wrapper -->
    </div> <!-- /.signUp-page -->

    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script type="application/javascript">
        $(function(){
            //on keypress
            $('#confirm_artist_password').keyup(function(e){
                //get values
                var artist_password = $('#artist_password').val();
                var confirm_artist_password = $(this).val();

                //check the strings
                if(artist_password == confirm_artist_password){
                    //if both are same remove the error and allow to submit
                    $('.error').text('Password are matching');
                }else{
                    //if not matching show error and not allow to submit
                    $('.error').text('Password not matching');
                }
            });

            //jquery form submit
        });
    </script>
    @include('layouts.newArtistfooter')
@endsection
