<!doctype html>
<html class="" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <title>@yield('title')</title>
    <meta name="title" property="title" content="Charts" />
    <meta name="description" property="description" content="" />
    <meta name="twitter:site" content="@billboard">
    <meta property="og:site_name" content="Billboard" />
    <meta property="og:type" content="article" />
    <link rel="canonical" href="charts.html">
    <meta name="google-site-verification" content="ml9ZIe9TIeLmooQXFtKneyGc9FtbaQ1Gwl3CDThjN7E" />
    <link rel="shortcut icon" href="https://assets.billboard.com/assets/1557936143/images/favicon.ico?b3c95ee58bd271bc5b09" type="image/vnd.microsoft.icon" />
    <link rel="apple-touch-icon" href="https://assets.billboard.com/assets/1557936143/images/BB_favicon144.png?b3c95ee58bd271bc5b09">
    <script type="text/javascript">
        var PGM = window.PGM || {};
        PGM.config = PGM.config || {
            contentHost: "https://www.billboard.com",
        };
        (function(PGM) {
            var debug_queue = [];
            PGM.debug = function() {
                debug_queue.push(arguments);
            };
            PGM.debug.dump = function() {
                return debug_queue;
            }
            PGM.debug.initialCategories = {};
        })(PGM);

        PGM.events = (function() {
            var events_queue = [];
            return {
                trigger: function() {
                    events_queue.push(arguments);
                },
                get: function() {
                    return events_queue;
                }
            };
        })();

        PGM.createScript = (function() {
            var q = [],
                cs = function createScript() {
                    q.push(arguments)
                };
            cs.dump = function() {
                return q;
            };
            return cs;
        })();

        PGM.createScriptTag = (function() {
            var q = [],
                cs = function createScriptTag() {
                    q.push(arguments)
                };
            cs.dump = function() {
                return q;
            };
            return cs;
        })();

        PGM.resolveOptAnon = (function() {
            var q = [],
                roa = function resolveOptAnon() {
                    q.push(arguments)
                };
            roa.dump = function() {
                return q;
            };
            return roa;
        })();

        PGM.tests = [];
        PGM.ads = PGM.ads || {};
        PGM.ads.adUnit = '/';
        PGM.ads.slots = PGM.ads.slots || {};

        PGM.gtmTrackerName = "";
        PGM.blockScripts = false;

        PGM.mediaQueries = {
            small: "only screen and (max-width: 767px)",
            medium: "only screen and (min-width: 768px)",
            large: "only screen and (min-width: 1024px)"
        };
    </script>
    <script src="https://lib.pgmcdn.com/advertisement.js"></script>

    <script type="text/javascript">
        window.CLARITY_GPT_DELAY = 0.01 * 1000;

        clarityPageData = {
            "site_code": "billboard",
            "zone": "",
            "single_request_mode": true
        };

        (function() {
            window.CLARITY = window.CLARITY || [];
            var ns = document.createElement('script'),
                s = document.getElementsByTagName('script')[0];
            ns.async = true;
            ns.type = 'text/javascript';
            ns.src = '//lib.pgmcdn.com/clarity-1557351493223.min.js';
            s.parentNode.insertBefore(ns, s);
        })();
    </script>
    <script type="text/javascript">
        (function() {
            // queue this up in case moat comes back before clarity loads
            window.moatYieldReady = function() {
                CLARITY.push({
                    use: ['header', 'logger'],
                    run: function(header, logger) {
                        try {
                            header.partners.moat.setTargeting();
                        } catch (e) {
                            console.log(e);
                        }
                    }
                });
            };

            var ns = document.createElement('script'),
                s = document.getElementsByTagName('script')[0];
            ns.async = true;
            ns.type = 'text/javascript';
            ns.src = '//sejs.moatads.com/billboardprebidheader946633233358/yi.js';
            s.parentNode.insertBefore(ns, s);
        })();
    </script>
    <script>
        ! function(a9, a, p, s, t, A, g) {
            if (a[a9]) return;

            function q(c, r) {
                a[a9]._Q.push([c, r])
            }
            a[a9] = {
                init: function() {
                    q("i", arguments)
                },
                fetchBids: function() {
                    q("f", arguments)
                },
                setDisplayBids: function() {},
                _Q: []
            };
            A = p.createElement(s);
            A.async = !0;
            A.src = t;
            g = p.getElementsByTagName(s)[0];
            g.parentNode.insertBefore(A, g)
        }("apstag", window, document, "script", "//c.amazon-adsystem.com/aax2/apstag.js");

        apstag.init({
            pubID: 3125,
            adServer: 'googletag',
            videoAdServer: 'DFP',
            bidTimeout: 1000
        });

        // Tell Clarity not to load a9
        CLARITY.push({
            use: ['header'],
            run: function(header) {
                header.skipJsInject('a9');
            }
        });
    </script>
    <script>
        CLARITY.push({
            use: ['header'],
            run: function(header) {
                var slotData = [
                    ["t_1", "/6419/billboard/charts/index", [
                        [970, 250],
                        [728, 90],
                        [970, 90]
                    ], "t", {
                        "1024": [
                            [970, 250],
                            [728, 90],
                            [970, 90]
                        ],
                        "768": [
                            [728, 90]
                        ],
                        "0": [
                            [320, 50]
                        ]
                    }, 0],
                    ["a_1", "/6419/billboard/charts/index", [
                        [728, 90]
                    ], "a", {
                        "1024": [
                            [728, 90]
                        ],
                        "768": [
                            [728, 90]
                        ],
                        "0": [
                            [320, 50]
                        ]
                    }, 0],
                    ["adhesion_1", "/6419/billboard/charts", [
                        [320, 50]
                    ], "adhesion", {
                        "1024": [],
                        "768": [],
                        "0": [
                            [320, 50]
                        ]
                    }, 0]
                ];

                // Add header slots then run,
                // any slots added after initial run will be processed differently
                for (var i = 0; i < slotData.length; i++) {
                    header.addHeaderSlot.apply(header, slotData[i]);
                }

                header.run({
                    "a9": {
                        "account_id": 3125
                    },
                    "facebook": {
                        "placementIds": ["172342566722377_216052892351344", "172342566722377_313225972634035", "172342566722377_324669418156357", "172342566722377_324669928156306", "172342566722377_324670321489600", "172342566722377_324671711489461", "172342566722377_324673048155994", "172342566722377_324673331489299", "172342566722377_324680731488559", "172342566722377_324681081488524", "172342566722377_324681421488490", "172342566722377_324681798155119", "172342566722377_324683821488250", "172342566722377_324684094821556", "172342566722377_324684408154858", "172342566722377_324684894821476", "172342566722377_324685178154781", "172342566722377_324685464821419", "172342566722377_324685731488059", "172342566722377_324686064821359", "172342566722377_324686414821324"]
                    },
                    "moat": true
                });
            }
        });
    </script>

    <script type='text/javascript'>
        var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];

        var PGM = PGM || {};
        PGM.ads = PGM.ads || {};
        PGM.ads.data = {
            "ad_zone": "",
            "targeting": {
                "tags_global": ["fe-api", "charts", "[node:chart_primary_category]"],
                "page": "80813",
                "tags": [],
                "env": "prod",
                "url": "1fef4cc6"
            },
            "slots": {
                "b4": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "b4",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [88, 31],
                            [120, 60]
                        ],
                        "768": [
                            [88, 31],
                            [120, 60]
                        ],
                        "0": [
                            [88, 31],
                            [120, 60]
                        ]
                    },
                    "defaultSizes": [
                        [88, 31],
                        [120, 60]
                    ],
                    "targetingParameters": [],
                    "chartLine": null,
                    "lazyLoad": false
                },
                "bbwatch1": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "bbwatch1",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [88, 31],
                            [120, 60]
                        ],
                        "768": [
                            [88, 31],
                            [120, 60]
                        ],
                        "0": [
                            [88, 31],
                            [120, 60]
                        ]
                    },
                    "defaultSizes": [
                        [88, 31],
                        [120, 60]
                    ],
                    "targetingParameters": {
                        "pos": "bbwatch1"
                    },
                    "chartLine": null,
                    "lazyLoad": false
                },
                "bbwatch2": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "bbwatch2",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 50]
                        ],
                        "768": [
                            [300, 50]
                        ],
                        "0": [
                            [300, 50]
                        ]
                    },
                    "defaultSizes": [
                        [300, 50]
                    ],
                    "targetingParameters": {
                        "pos": "bbwatch2"
                    },
                    "chartLine": null,
                    "lazyLoad": false
                },
                "g1": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "g1",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250]
                        ],
                        "768": [
                            [300, 250]
                        ],
                        "0": [
                            [300, 250]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250]
                    ],
                    "targetingParameters": {
                        "pos": "g1"
                    },
                    "chartLine": null,
                    "lazyLoad": false
                },
                "t2": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "t2",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [644, 380],
                            [620, 380]
                        ],
                        "768": [
                            [728, 90],
                            [644, 380],
                            [620, 380]
                        ],
                        "0": [
                            [300, 50],
                            [320, 50]
                        ]
                    },
                    "defaultSizes": [
                        [644, 380],
                        [620, 380]
                    ],
                    "targetingParameters": {
                        "pos": "t2"
                    },
                    "chartLine": null,
                    "lazyLoad": false
                },
                "lis": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "lis",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [92, 28]
                        ],
                        "768": [
                            [92, 28]
                        ],
                        "0": [
                            [92, 28]
                        ]
                    },
                    "defaultSizes": [
                        [92, 28]
                    ],
                    "targetingParameters": {
                        "pos": "lis"
                    },
                    "chartLine": "0",
                    "lazyLoad": false
                },
                "longform_t2": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "longform_t2",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [728, 90],
                            [970, 90]
                        ],
                        "768": [
                            [728, 90]
                        ],
                        "0": [
                            [300, 50],
                            [320, 50],
                            [300, 250]
                        ]
                    },
                    "defaultSizes": [
                        [728, 90],
                        [970, 90]
                    ],
                    "targetingParameters": {
                        "pos": "t2"
                    },
                    "chartLine": null,
                    "lazyLoad": false
                },
                "m": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250]
                        ],
                        "768": [
                            [300, 250]
                        ],
                        "0": [
                            [300, 50],
                            [320, 50]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250]
                    ],
                    "targetingParameters": {
                        "pos": "m"
                    },
                    "chartLine": null,
                    "lazyLoad": false
                },
                "lightbox": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "lightbox",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250],
                            [300, 600]
                        ],
                        "768": [
                            [728, 90]
                        ],
                        "0": [
                            [320, 50],
                            [300, 50]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250],
                        [300, 600]
                    ],
                    "targetingParameters": {
                        "pos": "lightbox"
                    },
                    "chartLine": null,
                    "lazyLoad": false
                },
                "m_artists": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m_artists",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250]
                        ],
                        "768": [
                            [300, 250]
                        ],
                        "0": [
                            [300, 250],
                            [300, 50],
                            [320, 50]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250]
                    ],
                    "targetingParameters": {
                        "pos": "m"
                    },
                    "chartLine": null,
                    "lazyLoad": false
                },
                "t": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "t",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [990, 350],
                            [970, 250],
                            [728, 90],
                            [970, 90],
                            [970, 66],
                            [728, 91],
                            [980, 50],
                            [980, 250]
                        ],
                        "768": [
                            [728, 91],
                            [728, 90]
                        ],
                        "0": [
                            [300, 50],
                            [320, 50],
                            [320, 170],
                            [320, 51]
                        ]
                    },
                    "defaultSizes": [
                        [990, 350],
                        [970, 250],
                        [728, 90],
                        [970, 90],
                        [970, 66],
                        [728, 91],
                        [980, 50],
                        [980, 250]
                    ],
                    "targetingParameters": {
                        "pos": "t"
                    },
                    "chartLine": null,
                    "lazyLoad": false
                },
                "m2": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m2",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250],
                            [300, 252]
                        ],
                        "768": [
                            [300, 250],
                            [300, 252]
                        ],
                        "0": [
                            [300, 50],
                            [320, 50],
                            [300, 250]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250],
                        [300, 252]
                    ],
                    "targetingParameters": {
                        "pos": "m2",
                        "dartlegolas": "segments",
                        "amazon": "1"
                    },
                    "chartLine": "7",
                    "lazyLoad": false
                },
                "m1": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m1",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250],
                            [300, 600],
                            [300, 251]
                        ],
                        "768": [
                            [300, 250],
                            [300, 600],
                            [300, 251]
                        ],
                        "0": [
                            [300, 50],
                            [320, 50],
                            [300, 250],
                            [300, 251]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250],
                        [300, 600],
                        [300, 251]
                    ],
                    "targetingParameters": {
                        "pos": "m1",
                        "amazon": "1"
                    },
                    "chartLine": "0",
                    "lazyLoad": false
                },
                "top": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "top",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [990, 350],
                            [970, 250],
                            [970, 90],
                            [970, 66],
                            [728, 91]
                        ],
                        "768": [
                            [728, 91]
                        ],
                        "0": null
                    },
                    "defaultSizes": [
                        [990, 350],
                        [970, 250],
                        [970, 90],
                        [970, 66],
                        [728, 91]
                    ],
                    "targetingParameters": {
                        "pos": "t"
                    },
                    "chartLine": null,
                    "lazyLoad": false
                },
                "a": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "a",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [728, 90],
                            [728, 92],
                            [728, 66]
                        ],
                        "768": [
                            [728, 90],
                            [728, 92],
                            [728, 66]
                        ],
                        "0": [
                            [300, 50],
                            [320, 50]
                        ]
                    },
                    "defaultSizes": [
                        [728, 90],
                        [728, 92],
                        [728, 66]
                    ],
                    "targetingParameters": {
                        "pos": "a",
                        "dartlegolas": "segments",
                        "amazon": "1"
                    },
                    "chartLine": null,
                    "lazyLoad": false
                },
                "b1": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "b1",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [88, 31],
                            [120, 60]
                        ],
                        "768": [
                            [88, 31],
                            [120, 60]
                        ],
                        "0": [
                            [88, 31],
                            [120, 60]
                        ]
                    },
                    "defaultSizes": [
                        [88, 31],
                        [120, 60]
                    ],
                    "targetingParameters": {
                        "pos": "b1"
                    },
                    "chartLine": null,
                    "lazyLoad": false
                },
                "b3": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "b3",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [88, 31],
                            [120, 60]
                        ],
                        "768": [
                            [88, 31],
                            [120, 60]
                        ],
                        "0": [
                            [88, 31],
                            [120, 60]
                        ]
                    },
                    "defaultSizes": [
                        [88, 31],
                        [120, 60]
                    ],
                    "targetingParameters": [],
                    "chartLine": null,
                    "lazyLoad": false
                },
                "bb": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "bb",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [970, 90],
                            [970, 250],
                            [980, 250],
                            [728, 90]
                        ],
                        "768": [
                            [728, 90]
                        ],
                        "0": [
                            [320, 50],
                            [300, 50]
                        ]
                    },
                    "defaultSizes": [
                        [970, 90],
                        [970, 250],
                        [980, 250],
                        [728, 90]
                    ],
                    "targetingParameters": {
                        "pos": "bb"
                    },
                    "chartLine": null,
                    "lazyLoad": false
                },
                "m10": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m10",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250],
                            [300, 600]
                        ],
                        "768": [
                            [300, 250],
                            [300, 600]
                        ],
                        "0": [
                            [300, 250],
                            [320, 50],
                            [300, 50]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250],
                        [300, 600]
                    ],
                    "targetingParameters": {
                        "pos": "m10"
                    },
                    "chartLine": "77",
                    "lazyLoad": false
                },
                "m11": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m11",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250],
                            [300, 600]
                        ],
                        "768": [
                            [300, 250],
                            [300, 600]
                        ],
                        "0": [
                            [300, 250],
                            [320, 50],
                            [300, 50]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250],
                        [300, 600]
                    ],
                    "targetingParameters": {
                        "pos": "m11"
                    },
                    "chartLine": "89",
                    "lazyLoad": false
                },
                "m12": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m12",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250],
                            [300, 600]
                        ],
                        "768": [
                            [300, 250],
                            [300, 600]
                        ],
                        "0": [
                            [300, 250],
                            [320, 50],
                            [300, 50]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250],
                        [300, 600]
                    ],
                    "targetingParameters": {
                        "pos": "m12"
                    },
                    "chartLine": "94",
                    "lazyLoad": false
                },
                "m13": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m13",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250],
                            [300, 600]
                        ],
                        "768": [
                            [300, 250],
                            [300, 600]
                        ],
                        "0": [
                            [300, 250],
                            [320, 50],
                            [300, 50]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250],
                        [300, 600]
                    ],
                    "targetingParameters": {
                        "pos": "m13"
                    },
                    "chartLine": "109",
                    "lazyLoad": false
                },
                "m14": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m14",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250],
                            [300, 600],
                            [300, 1050]
                        ],
                        "768": [
                            [300, 250],
                            [300, 600],
                            [300, 1050]
                        ],
                        "0": [
                            [300, 250],
                            [320, 50],
                            [300, 50]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250],
                        [300, 600],
                        [300, 1050]
                    ],
                    "targetingParameters": {
                        "pos": "m14"
                    },
                    "chartLine": "119",
                    "lazyLoad": false
                },
                "m15": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m15",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250],
                            [300, 600]
                        ],
                        "768": [
                            [300, 250],
                            [300, 600]
                        ],
                        "0": [
                            [300, 250],
                            [320, 50],
                            [300, 50]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250],
                        [300, 600]
                    ],
                    "targetingParameters": {
                        "pos": "m15",
                        "dartlegolas": "segments"
                    },
                    "chartLine": "129",
                    "lazyLoad": false
                },
                "m16": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m16",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250],
                            [300, 600]
                        ],
                        "768": [
                            [300, 250],
                            [300, 600]
                        ],
                        "0": [
                            [300, 250],
                            [320, 50],
                            [300, 50]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250],
                        [300, 600]
                    ],
                    "targetingParameters": {
                        "pos": "m16"
                    },
                    "chartLine": "139",
                    "lazyLoad": false
                },
                "m17": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m17",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250],
                            [300, 600]
                        ],
                        "768": [
                            [300, 250],
                            [300, 600]
                        ],
                        "0": [
                            [300, 250],
                            [320, 50],
                            [300, 50]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250],
                        [300, 600]
                    ],
                    "targetingParameters": {
                        "pos": "m17"
                    },
                    "chartLine": "159",
                    "lazyLoad": false
                },
                "m18": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m18",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250],
                            [300, 600]
                        ],
                        "768": [
                            [300, 250],
                            [300, 600]
                        ],
                        "0": [
                            [300, 250],
                            [320, 50],
                            [300, 50]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250],
                        [300, 600]
                    ],
                    "targetingParameters": {
                        "pos": "m18"
                    },
                    "chartLine": "159",
                    "lazyLoad": false
                },
                "m1_iframe": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m1_iframe",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250],
                            [300, 251]
                        ],
                        "768": [
                            [300, 250],
                            [300, 251]
                        ],
                        "0": [
                            [300, 50],
                            [320, 50]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250],
                        [300, 251]
                    ],
                    "targetingParameters": {
                        "pos": "m1",
                        "amazon": "1"
                    },
                    "chartLine": null,
                    "lazyLoad": false
                },
                "m2_iframe": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m2_iframe",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250],
                            [300, 252]
                        ],
                        "768": [
                            [300, 250],
                            [300, 252]
                        ],
                        "0": null
                    },
                    "defaultSizes": [
                        [300, 250],
                        [300, 252]
                    ],
                    "targetingParameters": {
                        "pos": "m2"
                    },
                    "chartLine": null,
                    "lazyLoad": false
                },
                "m3": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m3",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250],
                            [300, 600]
                        ],
                        "768": [
                            [300, 250],
                            [300, 600]
                        ],
                        "0": [
                            [300, 50],
                            [320, 50],
                            [300, 250]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250],
                        [300, 600]
                    ],
                    "targetingParameters": {
                        "pos": "m3"
                    },
                    "chartLine": "19",
                    "lazyLoad": false
                },
                "m4": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m4",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250],
                            [300, 600]
                        ],
                        "768": [
                            [300, 250],
                            [300, 600]
                        ],
                        "0": [
                            [320, 50],
                            [300, 50],
                            [200, 250]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250],
                        [300, 600]
                    ],
                    "targetingParameters": {
                        "pos": "m4"
                    },
                    "chartLine": "27",
                    "lazyLoad": false
                },
                "m5": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m5",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250],
                            [300, 600]
                        ],
                        "768": [
                            [300, 250],
                            [300, 600]
                        ],
                        "0": [
                            [320, 50],
                            [300, 50]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250],
                        [300, 600]
                    ],
                    "targetingParameters": {
                        "pos": "m5"
                    },
                    "chartLine": "33",
                    "lazyLoad": false
                },
                "m6": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m6",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250],
                            [300, 600]
                        ],
                        "768": [
                            [300, 250],
                            [300, 600]
                        ],
                        "0": [
                            [320, 50],
                            [300, 50]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250],
                        [300, 600]
                    ],
                    "targetingParameters": {
                        "pos": "m6"
                    },
                    "chartLine": "39",
                    "lazyLoad": false
                },
                "m7": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m7",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250],
                            [300, 600]
                        ],
                        "768": [
                            [300, 250],
                            [300, 600]
                        ],
                        "0": [
                            [300, 250],
                            [320, 50],
                            [300, 50]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250],
                        [300, 600]
                    ],
                    "targetingParameters": {
                        "pos": "m7"
                    },
                    "chartLine": "47",
                    "lazyLoad": false
                },
                "m8": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m8",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250],
                            [300, 600],
                            [300, 1050]
                        ],
                        "768": [
                            [300, 250],
                            [300, 600]
                        ],
                        "0": [
                            [300, 250],
                            [320, 50],
                            [300, 50]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250],
                        [300, 600],
                        [300, 1050]
                    ],
                    "targetingParameters": {
                        "pos": "m8"
                    },
                    "chartLine": "60",
                    "lazyLoad": false
                },
                "m9": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "m9",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [
                            [300, 250],
                            [300, 600]
                        ],
                        "768": [
                            [300, 250],
                            [300, 600]
                        ],
                        "0": [
                            [300, 250],
                            [320, 50],
                            [300, 50]
                        ]
                    },
                    "defaultSizes": [
                        [300, 250],
                        [300, 600]
                    ],
                    "targetingParameters": {
                        "pos": "m9"
                    },
                    "chartLine": "66",
                    "lazyLoad": false
                },
                "o1": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "o1",
                    "outOfBody": 1,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": null,
                        "768": null,
                        "0": null
                    },
                    "defaultSizes": [],
                    "targetingParameters": {
                        "pos": "o1"
                    },
                    "chartLine": null,
                    "lazyLoad": false
                },
                "tout": {
                    "adUnit": "\/6419\/billboard\/charts\/index",
                    "slotName": "tout",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": null,
                        "768": null,
                        "0": null
                    },
                    "defaultSizes": [],
                    "targetingParameters": {
                        "pos": "tout"
                    },
                    "chartLine": null,
                    "lazyLoad": false
                },
                "adhesion": {
                    "adUnit": "\/6419\/billboard\/charts",
                    "slotName": "adhesion",
                    "outOfBody": 0,
                    "responsive": true,
                    "sizeMapping": {
                        "1024": [],
                        "768": [],
                        "0": [
                            [320, 50]
                        ]
                    },
                    "defaultSizes": [
                        [320, 50]
                    ],
                    "targetingParameters": {
                        "pos": "adhesion"
                    },
                    "chartLine": null,
                    "lazyLoad": false
                }
            }
        };
        PGM.ads.slots = PGM.ads.slots || {};
        PGM.brightcove = {
            "account_id": 1125911414,
            "default_player": "SJeOLIT3Hf",
            "amp_player": "Cy13jcWohS",
            "autoplayers": ["HynaCuMbg", "S1endnWG9x", "HyHqRqMw", "SJeOLIT3Hf"]
        };

        window.CLARITY = window.CLARITY || [];

        CLARITY.push({
            use: ["ads", "jquery"],
            run: function(Ads, $) {
                if (typeof Krux !== 'undefined') {
                    Ads.addKeyVal("all", "ksg", Krux.segments);
                    Ads.addKeyVal("all", "kuid", Krux.user);
                }

                var $embeddedVideos = $('.embedded-content[data-template="video"]');
                if ($embeddedVideos.length) {
                    // Wish we could do this in the module but it's too late by then
                    // for now all embedded videos are autoplay
                    Ads.addKeyVal("all", "video", "autoplay");
                } else {
                    // Else check if it's one of our autoplay videos
                    $('video').each(function() {
                        var $vid = $(this),
                            player = $vid.data('player'),
                            account = $vid.data('account');

                        if (PGM.brightcove.autoplayers.indexOf(player) >= 0) {
                            // If there are any auto-play vids stop
                            Ads.addKeyVal("all", "video", "autoplay");
                            return false;
                        } else if (account) {
                            Ads.addKeyVal("all", "video", "click-to-play");
                            // if there are click-to-plays there could still be an auto-play somewhere, continue
                        }
                    });
                }
            }
        });
    </script>
    <script type='text/javascript'>
        PGM.ads.adUnit = '/6419/billboard/charts/index';

        CLARITY.push({
            use: ["ads"],
            run: function(Ads) {}
        });
    </script>
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-1266747-9']);
        _gaq.push(['_setDomainName', 'billboard.com']);
        _gaq.push(['_setAllowLinker', true]);
        _gaq.push(['_trackPageview']);
    </script>

    <script type="text/javascript">
        var dataLayer = dataLayer || [];
        dataLayer.push({
            'fireGTM': 'on',
            'contentId': '80813'

            ,
            'contentType': 'chart page'

            ,
            'category': 'chart'

        });
        dataLayer.push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
        });
        PGM.createScript('//www.googletagmanager.com/gtm.js?id=GTM-MD3ZLS');
    </script>

    <script>
        var _comscore = _comscore || [];
        var comscoreVars = {
            c1: "2",
            c2: "7395269",
            c3: ""
        };
        _comscore.push(comscoreVars);
    </script>
    <script type="text/javascript" data-src="https://sb.scorecardresearch.com/beacon.js"></script>
    <noscript>
        <img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=7395269&amp;cv=2.0&amp;cj=1" />
    </noscript>

    <script type='text/javascript'>
        var _sf_startpt = (new Date()).getTime()
    </script>
    <link rel="stylesheet" type="text/css" href="https://assets.billboard.com/assets/1557936143/css/charts.css?b3c95ee58bd271bc5b09">
    <script type='text/javascript'>
        var BB = BB || {};
        BB.config = BB.config || {
            contentHost: "https://www.billboard.com",
            mediaHost: "https://www.billboard.com"
        };
    </script>
    <style>
        .site-footer .footer-brand .brand {
            background: url('frontend/assets/images/logo.png') no-repeat scroll 50% !important;
            float: none;
        }
        .site-header__brand-logo {
            max-height: 75px !important;
            left: 27px !important;
            top: 60% !important;
        }
        /*.brand {*/
            /*margin-top: 0;*/
            /*float: left;*/
            /*height: 70px !important;*/
            /*width: 165px !important;*/
        /*}*/
    </style>
</head>

<body class="chart-page chart-page- chart-page-home" data-trackcategory="Charts-Charts" data-content-type="chart">
@yield('content')
