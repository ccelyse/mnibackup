<footer id="site-footer" class="site-footer">
    <div class="container footer-content">
        <img class="" src="frontend/assets/images/logo.png" alt="Billboard" style="width: 150px;">
    </div>
    <div class="container">
        <p class="copyright__paragraph">&copy; 2020 MNI PLAYLIST. All Rights Reserved.</p>
        <p class="copyright__paragraph">
            <a class="copyright__link" href="#">Terms of Use</a>
            <a class="copyright__link" href="#">Privacy Policy</a>
            <a class="copyright__link" href="#">About Our Ads</a>
            <a class="copyright__link copyright__link--contrast" href="#">Advertising</a>
        </p>
    </div>
    <div class="container">
        <p class="station-identification">
            MNI PLAYLIST is a local Rwandan Global Media.
        </p>
    </div>
</footer>
<script>
    window.CLARITY = window.CLARITY || [];
</script>
<div class="ad_clarity" data-out-of-page="true" style="display: none;"></div>

<script type="text/javascript" src="https://assets.billboard.com/assets/1557936143/js/commons.bundle.js?b3c95ee58bd271bc5b09"></script>
<script type="text/javascript" src="https://assets.billboard.com/assets/1557936143/js/transition.bundle.js?b3c95ee58bd271bc5b09"></script>
<script type="text/javascript" src="https://assets.billboard.com/assets/1557936143/js/vendor.js?b3c95ee58bd271bc5b09"></script>
<script type="text/javascript" src="https://assets.billboard.com/assets/1557936143/js/home.bundle.js?b3c95ee58bd271bc5b09"></script>
<script type="text/javascript" src="https://assets.billboard.com/assets/1557936143/js/bb.js?b3c95ee58bd271bc5b09"></script>

<script type='text/javascript'>
    var _sf_async_config = {};
    /** CONFIGURATION START **/
    _sf_async_config.uid = 3388;
    _sf_async_config.domain = 'billboard.com';
    _sf_async_config.useCanonical = true;
    /** CONFIGURATION END **/
    PGM.createScript('//static.chartbeat.com/js/chartbeat.js');
</script>

<script class="kxct" data-id="JsVUOKRj" data-timing="async" data-version="1.9" type="text/javascript">
    window.Krux || ((Krux = function() {
        Krux.q.push(arguments)
    }).q = []);
    (function() {
        PGM.createScript((location.protocol === 'https:' ? 'https:' : 'http:') + '//cdn.krxd.net/controltag/JsVUOKRj.js');
    }());
</script>

<script class="kxint" type="text/javascript">
    window.Krux || ((Krux = function() {
        Krux.q.push(arguments);
    }).q = []);
    (function() {
        function retrieve(n) {
            var k = 'kx' + '' + n,
                ls = (function() {
                    try {
                        return window.localStorage;
                    } catch (e) {
                        return null;
                    }
                })();
            if (ls) {
                return ls[k] || '';
            } else if (navigator.cookieEnabled) {
                var m = document.cookie.match(k + '=([^;]*)');
                return (m && unescape(m[1])) || '';
            } else {
                return '';
            }
        }
        Krux.user = retrieve('user');
        var segs = retrieve('segs');
        Krux.segments = segs ? segs.split(',') : [];
    })();
</script>

<script data-src="//platform.instagram.com/en_US/embeds.js"></script>
<script data-src="//platform.twitter.com/widgets.js"></script>
<div id="fb-root"></div>
<script type="text/javascript">
    PGM.createScriptTag("//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=303838389949803");
</script>
<script type="text/javascript">
    PGM.createScript('//launch.newsinc.com/js/embed.js', function() {
        if (_informq && typeof _informq.push === 'function') {
            _informq.push(['embed']);
        }
    });
</script>

<script type="text/javascript">
    ! function(f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function() {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window,
        document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1554285401541797'); // Insert your pixel ID here.
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1554285401541797&amp;ev=PageView&amp;noscript=1" /></noscript>

<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 879509920;
    var google_conversion_label = "wh8cCLa922cQoIOxowM";
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" data-src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/879509920/?value=1.00&amp;currency_code=USD&amp;label=wh8cCLa922cQoIOxowM&amp;guid=ON&amp;script=0" />
    </div>
</noscript>

<script type="text/javascript">
    window.PGM.createScript('//platform.twitter.com/oct.js', function() {
        twttr.conversion.trackPid('nw8g2', {
            tw_sale_amount: 0,
            tw_order_quantity: 0
        });
    });
</script>
<noscript>
    <img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=nw8g2&amp;p_id=Twitter&amp;tw_sale_amount=0&amp;tw_order_quantity=0" />
    <img height="1" width="1" style="display:none;" alt="" src="https://t.co/i/adsct?txn_id=nw8g2&amp;p_id=Twitter&amp;tw_sale_amount=0&amp;tw_order_quantity=0" />
</noscript>

</body>

</html>