<div class="header-wrapper ">
    <header id="site-header" class="site-header " role="banner">
        <div class="site-header__background">
            <div class="site-header__contents">
                <div class="site-header__bar">
                    <button class="site-header__nav-toggle"><i class="fa fa-bars"></i></button>
                    <div class="site-header__brand">
                        <a class="site-header__brand-link" href="index.html">
                            <img class="site-header__brand-logo" src="frontend/assets/images/logo.png" alt="Billboard">
                            <span class="site-header__brand-name">MNI PLAYLIST</span>
                        </a>
                    </div>
                    <button class="site-header__search-toggle"><i class="fa fa-search"></i></button>
                </div>
                <nav class="site-header__nav">
                    <div class="site-header__nav-inner">
                        <div class="site-header__social-subscribe">
                            <a href=#" title="Facebook">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a href="#" title="Twitter">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </div>
                        <ul class="site-header__nav-items">
                            <li class="site-header__nav-item  ">
                                <a href="{{'/'}}" class=site-header__nav-link>Home</a>
                            </li>
                            <li class="site-header__nav-item  ">
                                <a href="" class=site-header__nav-link>Playlist</a>
                            </li>
                            <li class="site-header__nav-item  ">
                                <a href="" class=site-header__nav-link>About Us</a>
                            </li>
                            <li class="site-header__nav-item  ">
                                <a href="" class=site-header__nav-link>Contact Us</a>
                            </li>
                        </ul>
                        <ul class="site-header__nav-items site-header__nav-items--more">
                            <li class="site-header__nav-item collapsed ">
                                <span class=site-header__nav-link--more&#x20;site-header__nav-link class=site-header__nav-link>More</span><span class="carat"></span>
                                <ul class="site-header__subnav-items">
                                    <li class="site-header__subnav-item  ">
                                        <a href="{{'/'}}" class=site-header__subnav-link>Home</a>
                                    </li>
                                    <li class="site-header__subnav-item  ">
                                        <a href="" class=site-header__subnav-link>Playlist</a>
                                    </li>
                                    <li class="site-header__subnav-item  ">
                                        <a href="" class=site-header__subnav-link>About Us</a>
                                    </li>
                                    <li class="site-header__subnav-item  ">
                                        <a href="" class=site-header__subnav-link>Contact Us</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
                <form class="site-header__search-form site-header__search-form--main js-search-form" method="get" accept-charset="UTF-8" action="/search/">
                    <input class="site-header__search-input js-search-input" placeholder="Search..." name="keys" maxlength="255">
                    <button id="site-header__search-submit" class="site-header__search-submit js-search-submit"><i class="fa fa-times"></i></button>
                </form>
            </div>
        </div>
    </header>

</div>
<div class="site-header__placeholder"></div>

</div>