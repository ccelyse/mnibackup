<!--
=====================================================
    Footer
=====================================================
-->
<style>
    .theme-footer-one .bottom-footer {
        /* border-top: 1px solid #ededed; */
        /* margin-top: 90px; */
        padding: 35px 50px;
    }
</style>
<footer class="theme-footer-one top-border">
    <div class="shape-one" data-aos="zoom-in-right"></div>
    {{--<img src="images/shape/shape-67.svg" alt="" class="shape-two">--}}
    <div class="top-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-3 col-12 about-widget" data-aos="fade-up">
                    <a href="{{'/'}}" class="logo" style="display: block;"><img src="front/images/logo/logo.png" style="width: 200px;    display: block;margin: auto;" alt=""></a>
                </div> <!-- /.about-widget -->
                <div class="col-lg-3 col-lg-3 col-sm-6 col-12 footer-list" data-aos="fade-up">
                    <h5 class="title">Our Contact</h5>
                    <a href=#" class="email" style="color: #fff;font-size: 13px;"><i class="fa fa-location-arrow" ></i> Kigali ,Rwanda</a><br>
                    <a href="mailto:info@mni.rw" class="email" style="color: #fff;font-size: 13px;"><i class="fa fa-envelope-square" ></i> info@mni.rw</a><br>
                    <a href="tel:+250780008883" class="phone" style="color: #fff;font-size: 13px;"><i class="fa fa-phone" ></i> 0780008883</a><br>
                    <a href="tel:+250784702692" class="phone" style="color: #fff;font-size: 13px;"><i class="fa fa-phone" ></i> 0784702692</a><br>
                    <a href="tel:+250789813910" class="phone" style="color: #fff;font-size: 13px;"><i class="fa fa-phone" ></i> 0789813910</a>
                </div> <!-- /.footer-recent-post -->
                <div class="col-lg-3 col-lg-3 col-sm-6 col-12 footer-list" data-aos="fade-up">
                    <h5 class="title">Important Links</h5>
                    <ul>
                        <li><a href="{{'Faq'}}" style="color: #fff;">FAQ</a></li>
                        <li><a href="{{'PrivacyPolicy'}}" style="color: #fff;">Privacy policy</a></li>
                        <li><a href="{{'AbahanziTermsandConditions'}}" style="color: #fff;">Amabwiriza ku mikoreshereze ku bahanzi</a></li>

                    </ul>
                </div> <!-- /.footer-recent-post -->
                <div class="col-lg-3 col-lg-3 col-sm-6 col-12 footer-information" data-aos="fade-up">
                    <h5 class="title">Partners</h5>
                    <img src="front/images/logopartners.png" style="width: 200px;" alt="">
                </div>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.top-footer -->

    <div class="container">
        <div class="bottom-footer">
            <p>&copy; 2020 copyright all right reserved. MNI</p>
            <p>MNI Selection Powered by <a href="http://sd.rw/" target="_blank" style="color:#e2a34c;">Skyline Digital</a> </p>
            {{--<ul>--}}
            {{--<li><a href="shop.html#">Privace & Policy.</a></li>--}}
            {{--<li><a href="faq.html">Faq.</a></li>--}}
            {{--<li><a href="shop.html#">Terms.</a></li>--}}
            {{--</ul>--}}
        </div> <!-- /.bottom-footer -->
    </div>
</footer> <!-- /.theme-footer-one -->

<!-- Scroll Top Button -->
<button class="scroll-top tran3s">
    <i class="fa fa-angle-up" aria-hidden="true"></i>
</button>



<!-- Optional JavaScript _____________________________  -->

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!-- jQuery -->
<script src="front/vendor/jquery.2.2.3.min.js"></script>
<!-- Popper js -->
<script src="front/vendor/popper.js/popper.min.js"></script>
<!-- Bootstrap JS -->
<script src="front/vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- menu  -->
<script src="front/vendor/mega-menu/assets/js/custom.js"></script>
<!-- AOS js -->
<script src="front/vendor/aos-next/dist/aos.js"></script>
<!-- WOW js -->
<script src="front/vendor/WOW-master/dist/wow.min.js"></script>
<!-- owl.carousel -->
<script src="front/vendor/owl-carousel/owl.carousel.min.js"></script>
<!-- js count to -->
<script src="front/vendor/jquery.appear.js"></script>
<script src="front/vendor/jquery.countTo.js"></script>
<!-- Fancybox -->
<script src="front/vendor/fancybox/dist/jquery.fancybox.min.js"></script>
<!-- Tilt js -->
<script src="front/vendor/tilt.jquery.js"></script>
<!-- ajaxchimp -->
<script src="front/vendor/ajaxchimp/jquery.ajaxchimp.min.js"></script>

<!-- Language js -->
<script src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>


<!-- Theme js -->
<script src="front/js/lang.js"></script>
<script src="front/js/theme.js"></script>


</div> <!-- /.main-page-wrapper -->
</body>
</html>