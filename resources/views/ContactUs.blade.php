@extends('layouts.newmaster')

@section('title', 'MNI PLAYLIST')

@section('content')
    <style>
        .spb-asset-content p{
            color:#fff !important;
        }
        .title-wrap h3{
            color: #fff !important;
        }
        .chart-page .chart-detail-header__chart-name {
            padding-top: 50px !important;
        }
        .modal-backdrop{
            position: relative !important;
        }
        /*.modal-backdrop.show {*/
        /*opacity: .5;*/
        /*}*/

        .modal-dialog {
            max-width: 800px !important;
            margin: 30px auto;
        }
        iframe{
            width: 100% !important;
        }

        .single-blog-post a,.single-blog-post p{
            color: #fff;
        }
        .bottom-title{
            font-size: 24px;
            line-height: 34px;
            color: #fff;
            padding: 45px 0 35px;
        }
        .title-box{
            display: inline-block;
            line-height: 35px;
            font-size: 13px;
            text-transform: uppercase;
            color: #fff;
            padding: 0 30px;
            border: 1px solid rgba(255,255,255,0.2);
            border-radius: 3px;
        }
        .page-title{
            color: #fff !important;
        }
    </style>
    @include('layouts.newtopmenu')

    <div class="solid-inner-banner">
        <div class="bg-shape-holder">
            <span class="big-round-one wow fadeInLeft animated" data-wow-duration="3s"></span>
            <span class="big-round-two wow fadeInLeft animated" data-wow-duration="3s"></span>
            <span class="big-round-three wow fadeInLeft animated" data-wow-duration="3s"></span>
            <span class="shape-one"></span>
            <span class="shape-two"></span>
            <img src="front/images/shape/shape-32.svg" alt="" class="shape-three">
            <span class="shape-four"></span>
        </div>
        <h2 class="page-title">Twandikire</h2>
    </div> <!-- /.solid-inner-banner -->



    <!--
        =============================================
            Contact Us
        ==============================================
        -->
    <div class="contact-us-section" style="padding-bottom: 20px; padding-top: 20px">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="contact-form">
                        <form class="form" id="contact-form" action="inc/contact.php" data-toggle="validator">
                            <div class="messages"></div>
                            <div class="controls">

                                <div class="form-group">
                                    <input id="form_email" type="text" name="text" placeholder="Amazina yawe" required="required">
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group">
                                    <input id="form_email" type="email" name="email" placeholder="email" required="required">
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group">
                                    <input id="form_sub" type="text" name="sub" placeholder="Umutwe w'ubutumwa bwawe" required="required">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <textarea id="form_message" name="message" class="form_message" placeholder="Ubutumwa bwawe" required="required" data-error="Please,leave us a message."></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <button class="theme-button-two">Send Message</button>
                            </div> <!-- /.controls -->
                        </form>
                    </div> <!-- /.contact-form -->
                </div> <!-- /.col- -->

                <div class="col-lg-6">
                    <div class="contact-info">
                        {{--<h2 >Don’t Hesitate to contact with us for any kind of information </h2>--}}
                        <h2 class="title">Muduhamagare cg mutwandikire</h2>
                        <p><i class="fa fa-phone" aria-hidden="true"></i> +250 780 008 883 <br><br><i class="fa fa-envelope-square" aria-hidden="true"></i> mniteam1@gmail.com</p><br>
                        <ul>
                            <li><a href="https://www.facebook.com/MNISelection/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="https://twitter.com/MNISelection"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.instagram.com/MNISelection/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.youtube.com/channel/UC2kHMCoGVRYmaTeG5APFBPA?view_as=subscriber"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                            {{--<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>--}}
                        </ul>
                    </div> <!-- /.contact-info -->
                </div>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.contact-us-section -->




    @include('layouts.newfooter')
@endsection
