@extends('layouts.newmaster')

@section('title', 'MNI-Kwinjira')

@section('content')
    <style>
        .spb-asset-content p{
            color:#fff !important;
        }
        .title-wrap h3{
            color: #fff !important;
        }
        .chart-page .chart-detail-header__chart-name {
            padding-top: 50px !important;
        }
        .modal-backdrop{
            position: relative !important;
        }
        /*.modal-backdrop.show {*/
        /*opacity: .5;*/
        /*}*/

        .modal-dialog {
            max-width: 800px !important;
            margin: 30px auto;
        }
        iframe{
            width: 100% !important;
        }

        .single-blog-post a,.single-blog-post p{
            color: #fff;
        }
        .bottom-title{
            font-size: 24px;
            line-height: 34px;
            color: #fff;
            padding: 45px 0 35px;
        }
        .title-box{
            display: inline-block;
            line-height: 35px;
            font-size: 13px;
            text-transform: uppercase;
            color: #fff;
            padding: 0 30px;
            border: 1px solid rgba(255,255,255,0.2);
            border-radius: 3px;
        }
        .alert-success {
            color: #155724;
            background-color: #d4edda;
            border-color: #c3e6cb;
            width: 100%;
            text-align: center;
            margin: 15px;
        }
        .theme-title-three p {
            font-size: 17px !important;
            line-height: 35px !important;
            color: #000;
            padding-top: 10px !important;
        }
        .page-title{
            color: #fff !important;
        }
        .our-service .our-history .text-wrapper {
            padding: 80px 0 80px 0px !important;
        }
        .theme-list-item li {
            color: #fff;
        }
        .theme-list-item li i, .theme-list-item li span {
            color: #fff;
        }
        .our-service .our-history .text-wrapper p {
            color: #000 !important;
        }
        #theme-banner-five {
            /* background: linear-gradient( 145deg, rgb(132,0,252) 0%, rgb(17,222,244) 100%); */
            /* background: linear-gradient( 145deg, rgb(5, 54, 50) 0%, rgb(28, 112, 111) 100%); */
            position: relative;
            z-index: 5;
            background: url(./front/images/audience-band-celebration-2240771-compressed.jpg) !important;
            background-size: cover !important;
        }
    </style>
    @include('layouts.newArtisttopmenu')



    <!--
              =============================================
                  Signup Page
              ==============================================
              -->
    <div class="signUp-page signUp-minimal pt-50 pb-100" style="padding-top: 120px;">
        <div class="shape-wrapper">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div> <!-- /.shape-wrapper -->
        <div class="signin-form-wrapper">
            <div class="title-area text-center">
                <h3>Kwinjira.</h3>
            </div> <!-- /.title-area -->

            <form class="form-horizontal form-simple"  id="login-form" method="POST" action="{{ route('SignIn_') }}">
                {{ csrf_field() }}
                <div class="row">
                    @if (session('success'))
                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="col-12">
                        <div class="input-group">
                            <input type="email" value="{{ old('email') }}" name="email" required>
                            <label>Email</label>
                        </div> <!-- /.input-group -->
                    </div> <!-- /.col- -->
                    <div class="col-12">
                        <div class="input-group">
                            <input type="password" name="password" required>
                            <label>Password</label>
                        </div> <!-- /.input-group -->
                    </div> <!-- /.col- -->
                </div> <!-- /.row -->
                {{--<div class="agreement-checkbox d-flex justify-content-between align-items-center">--}}
                {{--<div>--}}
                {{--<input type="checkbox" id="remember">--}}
                {{--<label for="remember">Remember Me</label>--}}
                {{--</div>--}}
                {{--<a href="#">Forget Password?</a>--}}
                {{--</div>--}}
                <button class="line-button-one">Kwinjira</button>
            </form>
            <p class="signUp-text text-center"><a href="{{'ForgetPassword'}}">Forgot Password?</a></p>
            <p class="signUp-text text-center">Nta Konti ufite? <a href="{{'ArtistRegistration'}}">Fungura Konti</a> Ubungubu.</p>
        </div> <!-- /.sign-up-form-wrapper -->
    </div> <!-- /.signUp-page -->


    @include('layouts.newArtistfooter')
@endsection
