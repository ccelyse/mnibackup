@extends('layouts.newmaster')

@section('title', 'MNI PLAYLIST')

@section('content')
<style>
    .spb-asset-content p{
        color:#fff !important;
    }
    .title-wrap h3{
        color: #fff !important;
    }
    .chart-page .chart-detail-header__chart-name {
        padding-top: 50px !important;
    }
    .modal-backdrop{
        position: relative !important;
    }
    /*.modal-backdrop.show {*/
        /*opacity: .5;*/
    /*}*/

    .modal-dialog {
        max-width: 800px !important;
        margin: 30px auto;
    }
    iframe{
        width: 100% !important;
    }

    .single-blog-post a,.single-blog-post p{
        color: #fff;
    }
    .bottom-title{
        font-size: 24px;
        line-height: 34px;
        color: #fff;
        padding: 45px 0 35px;
    }
    .title-box{
        display: inline-block;
        line-height: 35px;
        font-size: 13px;
        text-transform: uppercase;
        color: #fff;
        padding: 0 30px;
        border: 1px solid rgba(255,255,255,0.2);
        border-radius: 3px;
    }
    .alert-success {
        color: #155724;
        background-color: #d4edda;
        border-color: #c3e6cb;
        width: 100%;
        text-align: center;
        margin: 15px;
    }
    .theme-title-three p {
        font-size: 17px !important;
        line-height: 35px !important;
        color: #798598;
        padding-top: 10px !important;
    }
.page-title{
    color: #fff !important;
}
</style>
@include('layouts.newtopmenu')

<div class="solid-inner-banner">
    <div class="bg-shape-holder">
        <span class="big-round-one wow fadeInLeft animated" data-wow-duration="3s"></span>
        <span class="big-round-two wow fadeInLeft animated" data-wow-duration="3s"></span>
        <span class="big-round-three wow fadeInLeft animated" data-wow-duration="3s"></span>
        <span class="shape-one"></span>
        <span class="shape-two"></span>
        <img src="front/images/shape/shape-32.svg" alt="" class="shape-three">
        <span class="shape-four"></span>
    </div>
    <h2 class="page-title">Imishinga ya MNI</h2>
</div> <!-- /.solid-inner-banner -->


<!--
    =============================================
        About Us Standard
    ==============================================
    -->
<div class="about-us-standard">
    <div class="container">
        <div class="theme-title-three text-center mb-170">

            <div class="col-md-12">
                <ul class="theme-list-item pb-70">
                    <li><i class="fa fa-check-circle" aria-hidden="true"></i> MNI Selection</li>
                    <li><i class="fa fa-check-circle" aria-hidden="true"></i> MNI Funding</li>
                    <li><i class="fa fa-check-circle" aria-hidden="true"></i> MNI Tarama</li>
                </ul>
            </div>

            <p>MNI Selection ni umushinga wa mbere wa MNI ushyiraho uburyo bushya abahanzi nyarwanda bakwinjiza amafaranga. MNI yashizeho uburyo abafana ba muzika nyarwanda bakwihitiramo indirimbo bakunze mu majonjora azajya aba buri kwezi, maze eshanu zambere zikajya mu irushanwa zigatorwa hakoreshejwe uburyo bwa USSD. amafaranga cyiyo servise ikaba amafaranga 50 kuri buri tora ry’umufana akazajya ava kuri mobile money haba ari umurongo wa MTN, AIRTEL cyangwa TIGO.</p>
            <p>Igice 1: Amajonjora azajya abera kuri nimero ya whatsapp ya MNI</p>
            <p>Igice 2: Amarushanwa y’indirimbo eshanu zahize izindi</p>
            <p>Igice 3: Guhemba</p>

        </div> <!-- /.theme-title-three -->
    </div> <!-- /.container -->
</div> <!-- /.about-us-standard -->





@include('layouts.newfooter')
@endsection
