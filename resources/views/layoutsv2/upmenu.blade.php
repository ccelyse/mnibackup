<header>
    <div class="container">
        <div class="brand">
            <div class="brand_img" style="display: inline-flex;">
                <img src="front/images/logo/logo.png" style="width: 100px;" alt="" id="brand_img_logo">
            </div>
            <h1 class="brand_slogan" style="display: inline-flex;"><a href="{{'/'}}" style="color: #fff !important;">MNI</a></h1>
{{--            <p class="brand_slogan"></p>--}}
            <h1 class="brand_name">Muzika Nyarwanda Ipande</h1>

            <div class="mobile_logo_" style="display: none">
                <img src="frontend/assets/images/mobile.png">
            </div>

        </div>
    </div>
    <div id="stuck_container" class="stuck_container">
        <div class="container">
            <nav class="nav" style="display: inline-block !important;">
                <ul data-type="navbar" class="sf-menu">
                    <li><a href="{{'HomeV2'}}">Home</a>
                    </li>
{{--                    <li><a href="index-2.html">Musicians</a></li>--}}
{{--                    <li><a href="index-3.html">Charts</a>--}}
                    <li>
                        <a href="#">Chart</a>
                        <ul>
                            <li><a href="{{'Charts'}}">Monthly Chart</a></li>
                            <li><a href="{{'ListArchives'}}">Previous Chart</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{'Discover'}}">Discover</a>
                        <ul>
                            <?php
                                $list = \App\MusicStakeHolders::all();
                                foreach ($list as $lists){
                                    $route = route('StakeHolders',['id'=> $lists->id]);
                                    echo "<li><a href='$route'>$lists->slider_title</a></li>";
                                }
                            ?>
                        </ul>
                    </li>
                    <li><a href="{{'Broadcast'}}">Broadcast</a>
                    <li><a href="{{'Fundraising'}}">Fund</a>
                    <li><a href="{{'Competitions'}}">Competitions</a>
                </ul>
            </nav>
        </div>
    </div>
</header>