@extends('layouts.newmaster')

@section('title', 'MNI-Abo turibo')

@section('content')
    <style>
        .spb-asset-content p{
            color:#fff !important;
        }
        .title-wrap h3{
            color: #fff !important;
        }
        .chart-page .chart-detail-header__chart-name {
            padding-top: 50px !important;
        }
        .modal-backdrop{
            position: relative !important;
        }
        /*.modal-backdrop.show {*/
        /*opacity: .5;*/
        /*}*/

        .modal-dialog {
            max-width: 800px !important;
            margin: 30px auto;
        }
        iframe{
            width: 100% !important;
        }

        .single-blog-post a,.single-blog-post p{
            color: #fff;
        }
        .bottom-title{
            font-size: 24px;
            line-height: 34px;
            color: #fff;
            padding: 45px 0 35px;
        }
        .title-box{
            display: inline-block;
            line-height: 35px;
            font-size: 13px;
            text-transform: uppercase;
            color: #fff;
            padding: 0 30px;
            border: 1px solid rgba(255,255,255,0.2);
            border-radius: 3px;
        }
        .alert-success {
            color: #155724;
            background-color: #d4edda;
            border-color: #c3e6cb;
            width: 100%;
            text-align: center;
            margin: 15px;
        }
        .theme-title-three p {
            font-size: 17px !important;
            line-height: 35px !important;
            color: #000;
            padding-top: 10px !important;
        }
        .page-title{
            color: #fff !important;
        }
        .our-service .our-history .text-wrapper {
            padding: 80px 0 80px 0px !important;
        }
        .theme-list-item li {
            color: #fff;
        }
        .theme-list-item li i, .theme-list-item li span {
            color: #fff;
        }
        .our-service .our-history .text-wrapper p {
            color: #000 !important;
        }
        #theme-banner-five {
            /* background: linear-gradient( 145deg, rgb(132,0,252) 0%, rgb(17,222,244) 100%); */
            /* background: linear-gradient( 145deg, rgb(5, 54, 50) 0%, rgb(28, 112, 111) 100%); */
            position: relative;
            z-index: 5;
            background: url(./front/images/aboutus.jpg) !important;
            background-size: cover !important;
            background-position: bottom !important;
        }
        .team-minimal .single-team-member .img-box {
            width: 200px !important;
            height: 200px !important;
             border-radius:0px !important;
            overflow: hidden;
            margin: 0 auto;
            position: relative;
        }
    </style>
    @include('layouts.newtopmenu')

    <div id="theme-banner-five">

        {{--<div class="mobile-screen-one wow fadeInRight animated" data-wow-duration="1.5s"><img src="front/images/Picture1.png" alt=""></div>--}}
        <div class="mobile-screen-two wow fadeInRight animated" data-wow-duration="1.5s" data-wow-delay="0.8s">
            {{--<img src="front/images/home/screen8.png" alt="">--}}
            {{--<img src="front/images/home/screen9.png" alt="" class="search-box wow zoomIn animated" data-wow-duration="1.5s" data-wow-delay="1.8s">--}}
        </div>
        <div class="bg-shape-holder">
            <span class="big-round-one wow fadeInLeft animated" data-wow-duration="3s"></span>
            <span class="big-round-two wow fadeInLeft animated" data-wow-duration="3s"></span>
            <span class="big-round-three wow fadeInLeft animated" data-wow-duration="3s"></span>
            <span class="shape-one"></span>
            <span class="shape-two"></span>
            <img src="front/images/shape/shape-32.svg" alt="" class="shape-three">
            <span class="shape-four"></span>
        </div>
        <div class="main-wrapper">
            <h1 class="main-title wow fadeInUp animated" data-wow-delay="0.4s">Turi MNI</h1>
            {{--<p class="sub-title wow fadeInUp animated" data-wow-delay="0.9s">Shyigikira indirimbo ukunda.</p>--}}
        </div> <!-- /.main-wrapper -->
        {{--<p class="muzikahashtag">#MuzikaNyarwandaIpande</p>--}}
    </div> <!-- /#theme-banner-five -->


    <!--
        =============================================
            Our Service Creative
        ==============================================
        -->
    <div class="our-service service-creative">
        <div class="our-history">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-wrapper">
                            <h2 class="service-title">Abo turibo</h2>
                            <p>Umuziki Nyarwanda umaze gutera intambwe ishimishije kubera ibihangano byiza bikorwa ndetse no kubera ushyigikirwa n’abakunzi bamuzika, itangazamakuru, abashoramari ndetse na Leta.</p>
                            <p>Kuri iyi ntambwe nziza imaze guterwa haracyari urugendo rwo gukomeza kwiyubaka kugirango umuziki nyarwanda ukomeze utere imbere. Bimwe mubyo dukeneye gufatanya gushakira ibisubizo harimo gushyiraho uburyo bushoboka bwinjiriza abahanzi amafaranga kandi bumenyekanisha ibihangano ndetse n’ibikorwa byabo, uburyo bubahuza n’abakunzi bamuzika babo haba mubitaramo, ndetse n’uburyo bwo guhemba no gushimira abitwara neza.</p>
                            <p>Nk’urubyiruko twishyize hamwe kugirango dutange umusanzu wacu mu iterambere ry’umuziki nyarwanda dukora imishinga ya MNI Selection ndetse na MNI Tarama aho umuhanzi yakwinjiza amafaranga ku indirimbo ze niyo yaba ari imwe yasohoye kandi bukaba ari uburyo yiyumvamo.</p>
                            {{--<p>Nk’urubyiruko twishyize hamwe kugirango dutange umusanzu wacu mu iterambere ry’umuziki nyarwanda dukora kompanyi yitwa Muzika Nyarwanda Ipande (MNI Ltd.) MNI Ltd ku ikubitiro izatangirana imishinga ya MNI Selection ndetse na MNI Tarama aho umuhanzi yakwinjiza amafaranga ku gihangano cye niyo yaba ari  kimwe yasohoye kandi bukaba ari uburyo yiyumvamo.</p>--}}
                            <p><strong>MNI Selection</strong> in urutonde ngaruka kwezi rugaragza indirimbo zikunzwe mu igihugu. Urutonde rukorwa hashingiwe ku amajwi ibihangano byabonye binyuze mu matora akorwa n’abakunzi ba muzika kuri www.mni.rw cyangwa ukanze *611#. Gutora ni amafaranga 50 RWF atangwa n’umukunzi w’igihangano. Uko igihangano gitorwa kiba kininjiriza nyir’igihangano. Banyir’Ibihangano byahize ibindi mugihe cy’amezi atatu ashize bahabwa ibihembo bitandukanye ndetse bakabasha gutarama mu igitaramo cyacu cya <strong>MNI Tarama.</strong></p>
                        </div>
                    </div>
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /.our-history -->


        {{--<div class="service-block" style="background: #7d4afe;">--}}
        {{--<div class="container">--}}
        {{--<div class="demo-container-1100">--}}
        {{--<div class="row">--}}
        {{--<div class="col-lg-6 order-lg-last" data-aos="fade-left">--}}
        {{--<div class="service-info">--}}
        {{--<div class="num">01</div>--}}
        {{--<h2 class="service-title"><a href="service-creative.html#">Abo turibo </a></h2>--}}
        {{--<p style="color: #fff;"><strong>MNI Ltd</strong> ni sosiyete y’igenga ifite intego yo kugira uruhare mu guteza imbere uruganda rwa Muzika Nyarwanda. Dukunda umuziki, cyane cyane uwa hano I wacu mu Rwanda akaba ari nayo mpamvu twifuza kugira uruhare rufatika mu iterambere ryawo. Turifuza gukorana n’inzego zose zifite aho zihurira n’umuziki nyarwanda harimo abahanzi, abafana ba muzika nyarwanda, urwego rwabikorera na leta muri uru rugendo rwo kugeza umuziki wacu kuyindi ntambwe nziza. </p>--}}
        {{--<a href="#" class="read-more"><i class="flaticon-next-1"></i></a>--}}
        {{--</div> <!-- /.service-info -->--}}
        {{--</div> <!-- /.col- -->--}}
        {{--<div class="col-lg-6 order-lg-first" data-aos="fade-right"><div class="img-box"><img src="front/images/icon/icon54.svg" alt=""></div></div>--}}
        {{--</div> <!-- /.row -->--}}
        {{--</div> <!-- /.demo-container-1100 -->--}}
        {{--</div> <!-- /.container -->--}}
        {{--</div> <!-- /.service-block -->--}}

        <div class="service-block" style="background: #676dff;">
            <div class="container">
                <div class="demo-container-1100">
                    <div class="row">
                        <div class="col-lg-6" data-aos="fade-right">
                            <div class="service-info">
                                {{--<div class="num">02</div>--}}
                                <h2 class="service-title"><a href="#"> Icyerekezo</a></h2>
                                <p style="color: #fff;">Kuba kompanyi ya mbere y’imyidagaduro mu Rwanda igira uruhare mu iterambere rya muzika Nyarwanda kandi ituma abakunzi bamuzika biyumvamo umuziki wabo kurushaho.</p>

                                <h2 class="service-title"><a href="#"> Ikigamijwe</a></h2>
                                <p style="color: #fff;">Gushyiraho uburyo bushya abahanzi ba muzika babona amafararanga, bumenyekanisha ibihangano byabo ndetse buteza imbere ibikorwa bya muzika bakora.</p>
                                <a href="#" class="read-more"><i class="flaticon-next-1"></i></a>
                            </div> <!-- /.service-info -->
                        </div> <!-- /.col- -->
                        <div class="col-lg-6" data-aos="fade-left"><div class="img-box"><img src="front/images/icon/icon55.svg" alt=""></div></div>
                    </div> <!-- /.row -->
                </div> <!-- /.demo-container-1100 -->
            </div> <!-- /.container -->
        </div> <!-- /.service-block -->

        <div class="service-block" style="background: #549cff;">
            <div class="container">
                <div class="demo-container-1100">
                    <div class="row">
                        <div class="col-lg-6 order-lg-last" data-aos="fade-left">
                            <div class="service-info">
                                {{--<div class="num">03</div>--}}
                                <h2 class="service-title"><a href="#">Intego</a></h2>
                                <ul class="theme-list-item pb-70">
                                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i>Kwimakaza umuco nyarwanda </li>
                                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i> Guteza imbere uruganda rw’umuziki nyarwanda</li>
                                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i> Guha agaciro amahitamo ya bakunzi ba muzika</li>
                                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i> Guteza imbere ubukerarugendo</li>
                                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i> Gutanga akazi ku baturage</li>
                                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i> Gushyiraho gahunda ziduhuza n’urubyiruko ndetse n’Abanyarwanda muri rusange</li>
                                </ul>
                                <a href="#" class="read-more"><i class="flaticon-next-1"></i></a>
                            </div> <!-- /.service-info -->
                        </div> <!-- /.col- -->
                        <div class="col-lg-6 order-lg-first" data-aos="fade-right"><div class="img-box"><img src="front/images/icon/icon56.svg" alt=""></div></div>
                    </div> <!-- /.row -->
                </div> <!-- /.demo-container-1100 -->
            </div> <!-- /.container -->
        </div> <!-- /.service-block -->



{{--        <div class="element-section">--}}
{{--            <div class="team-minimal our-team" style="    margin-top: 50px;">--}}
{{--                <div class="container">--}}
{{--                    <h2 class="service-title"style="text-align: center; padding-bottom: 30px" ><a href="#" style="color: #266664">Partners</a></h2>--}}
{{--                    <div class="row">--}}

{{--                        <div class="col-lg-4">--}}
{{--                            <div class="single-team-member">--}}
{{--                                <div class="wrapper">--}}
{{--                                    <div class="img-box">--}}
{{--                                        <img src="front/images/logo-with-a-name (1).jpg" alt="">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div> <!-- /.single-team-member -->--}}
{{--                        </div> <!-- /.col- -->--}}
{{--                        <div class="col-lg-4">--}}
{{--                            <div class="single-team-member">--}}
{{--                                <div class="wrapper">--}}
{{--                                    <div class="img-box">--}}
{{--                                        <img src="front/images/logo_Masha.png" alt="">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div> <!-- /.single-team-member -->--}}
{{--                        </div> <!-- /.col- -->--}}
{{--                        <div class="col-lg-4">--}}
{{--                            <div class="single-team-member">--}}
{{--                                <div class="wrapper">--}}
{{--                                    <div class="img-box">--}}
{{--                                        <img src="front/images/logo_royo_enter.jpg" alt="">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div> <!-- /.single-team-member -->--}}
{{--                        </div> <!-- /.col- -->--}}
{{--                    </div> <!-- /.row -->--}}
{{--                </div> <!-- /.container -->--}}
{{--            </div> <!-- /.team-minimal -->--}}
{{--        </div>--}}
    </div> <!-- /.our-service -->




    @include('layouts.newfooter')
@endsection
