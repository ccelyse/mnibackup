@extends('layouts.newmaster')

@section('title', 'MNI Selection - Gutora')

@foreach($getmusicmodern as $datas)
<meta property="og:url"                content="<?php echo url()->full(); ?>" />
<meta property="og:type"               content="article" />
<meta property="og:title"              content="Tora {{$datas->song_name}} ya {{$datas->song_artist}}" />
<meta property="og:description"        content="Biroroshye! Yishigikire kuri www.mni.rw cyangwa ukande *611# ifite code ya {{$datas->playlist_code}}" />
<meta property="og:image"              content="http://mni.rw/public/SongImages/{{$datas->song_cover_picture}}" />

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="{{$datas->song_name}} ya {{$datas->song_artist}}" />
<meta name="twitter:creator" content="@MNISelection" />
<meta name="twitter:image" content="http://mni.rw/public/SongImages/{{$datas->song_cover_picture}}">

@endforeach



@section('content')
<style>
    .spb-asset-content p{
        color:#fff !important;
    }
    .title-wrap h3{
        color: #fff !important;
    }
    .chart-page .chart-detail-header__chart-name {
        padding-top: 50px !important;
    }
    .modal-backdrop{
        position: relative !important;
    }
    /*.modal-backdrop.show {*/
        /*opacity: .5;*/
    /*}*/

    .modal-dialog {
        max-width: 800px !important;
        margin: 30px auto;
    }
    iframe{
        width: 100% !important;
    }
    #contact-form button {
        /*margin-top: 35px;*/
        width: 100%;
    }
    .social-icon li a i {
         bottom: 0px;
        position: relative;
    }
    #contact-form .form-group input, #contact-form .form-group textarea, #contact-form .form-group select {
        border: 1px solid #fff !important;
        color: #fff !important;
    }
    .single-blog-post a,.single-blog-post p{
        color: #fff;
    }
    .bottom-title{
        font-size: 24px;
        line-height: 34px;
        color: #fff;
        padding: 45px 0 35px;
    }
    .title-box{
        display: inline-block;
        line-height: 35px;
        font-size: 13px;
        text-transform: uppercase;
        color: #fff;
        padding: 0 30px;
        border: 1px solid rgba(255,255,255,0.2);
        border-radius: 3px;
    }
    .alert-success {
        color: #155724;
        background-color: #d4edda;
        border-color: #c3e6cb;
        width: 100%;
        text-align: center;
        margin: 15px;
    }
    .solid-inner-banner .page-title {
        font-size: 35px !important;
        padding-bottom:0px !important;
    }
    .voteshare li a {
        width: 40px;
        line-height: 36px;
        border: 2px solid #fff !important;
        border-radius: 50%;
        text-align: center;
        font-size: 18px;
        color: #fff !important;
        margin-right: 5px;
    }
    .contact-us-section .contact-info ul li a {
        height: 40px;
    }
.page-title{
    color: #fff !important;
}
    .modal-dialog {
        max-width: 50% !important;
        margin: 30px auto;
    }
    .successmessage_{
        display: none !important;
    }
    .loading_{
        display: none !important;
    }
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script
        src="https://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>


@include('layouts.newtopmenu')

@foreach($getmusicmodern as $data)
<div class="solid-inner-banner">
    <div class="bg-shape-holder">
        <span class="big-round-one wow fadeInLeft animated" data-wow-duration="3s"></span>
        <span class="big-round-two wow fadeInLeft animated" data-wow-duration="3s"></span>
        <span class="big-round-three wow fadeInLeft animated" data-wow-duration="3s"></span>
        <span class="shape-one"></span>
        <span class="shape-two"></span>
        <img src="front/images/shape/shape-32.svg" alt="" class="shape-three">
        <span class="shape-four"></span>
    </div>
    <?php
    $votesbe = $data->votesNumber;
    $newvotes = $votesbe - 1;
    ?>
    <h2 class="page-title" id="page-titles">Ugiye gutora {{$data->song_name}}  ya {{$data->song_artist}} ifite code ya {{$data->playlist_code}} ifite amajwi y'ukwezi : <?php echo $newvotes;?></h2>
    <div class="col-lg-3 mx-auto" >
        <div class="contact-form" style="padding-bottom: 15px">

            <form class="form" action="#" id="contact-form">
                <div id="messages" style="display: none"></div>
                <div class="controls">
                    <div class="form-group">
                        <input id="song_artist" type="text" name="song_artist" value="{{$data->song_artist}}" readonly hidden>
                        <input id="id" type="text" name="id" value="{{$data->id}}" hidden>
                    </div>

                    <div class="form-group">
                        <input id="song_name" type="text" name="song_name" value="{{$data->song_name}}" readonly hidden>
                    </div>

                    <div class="form-group">
                        <input id="phonenumber" type="text" name="phonenumber" placeholder="numero yawe ya mobile 078xxxxxxx" required>
                    </div>
                    <button type='button' class="theme-button-two" id="VoteNowUser">Tora</button>
                </div> <!-- /.controls -->

            </form>
            <div class="modal fade" id="successmessage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="CloseVote">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div id="Voting_response" style="display: none">
                                <div id="successmodal">
                                </div>
                                <img src="front/images/loading-gif-transparent-4.gif" style="max-width: 100px;display: none;margin: 0 auto;" id="loading">
                                <div id="messagestatus" style="background: #135c5a;color: #fff;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="contact-us-section" style="padding-bottom: 20px;padding-top: 20px; ">
                <div class="col-lg-12">
                    <div class="contact-info">
                        <p style="color: #fff">Share my song on social media</p>
                        <ul class="voteshare">

                            <li><a href="https://www.facebook.com/sharer.php?u=<?php echo url()->full(); ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true" style="position: relative;top: 10px;"></i></a></li>
                            <li><a href="https://twitter.com/share?url=<?php echo url()->full(); ?>&text={{$data->song_name}} ya {{$data->song_artist}}" target="_blank"><i class="fa fa-twitter" aria-hidden="true" style="position: relative;top: 10px;"></i></a></li>
                            <li><a href="https://wa.me/?text=<?php echo url()->full(); ?>" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true" style="position: relative;top: 10px;"></i></a></li>
                            {{--<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>--}}
                        </ul>
                    </div> <!-- /.contact-info -->
                </div>
                    </div> <!-- /.row -->
                </div> <!-- /.container -->
            </div> <!-- /.contact-us-section -->
        </div> <!-- /.contact-form -->
    </div> <!-- /.col- -->
</div> <!-- /.solid-inner-banner -->



<!--
    =============================================
        Contact Us
    ==============================================
    -->
<div class="contact-us-section" style="padding-bottom: 20px;padding-top: 20px; ">
    <div class="container">
        <div class="row">
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif


            <div class="col-lg-6">
                <div class="contact-info">
                    {{--<h2 class="title">Don’t Hesitate to contact with us for any kind of information Ntutinde kutwandikira</h2>--}}
                    <p style="color: #15605e !important; font-weight: bold;">Ugiye Gutora indirimbo, amafaranga ni 50RWF.</p>
                    <p>Muramutse mugize ikibazo mwaduhamagara kuri iyi numero bagufashe</p>
                    <a href="tel:+250780008883" class="call">+250 780 008 883</a>
                    <ul>
                        <li><a href="https://www.facebook.com/MNISelection/"><i class="fa fa-facebook" aria-hidden="true" style="position: relative;top: 10px;"></i></a></li>
                        <li><a href="https://twitter.com/MNISelection"><i class="fa fa-twitter" aria-hidden="true" style="position: relative;top: 10px;"></i></a></li>
                        <li><a href="https://www.instagram.com/MNISelection/"><i class="fa fa-instagram" aria-hidden="true" style="position: relative;top: 10px;"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UC2kHMCoGVRYmaTeG5APFBPA?view_as=subscriber"><i class="fa fa-youtube" aria-hidden="true" style="position: relative;top: 10px;"></i></a></li>
                    </ul>
                </div> <!-- /.contact-info -->
            </div>
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div> <!-- /.contact-us-section -->


{{--<script src="https://code.jquery.com/jquery-3.4.1.min.js" type="application/javascript"></script>--}}
<script src="front/vendor/jquery.2.2.3.min.js"></script>
<script>
    $(document).on('click', '#VoteNowUser', function() {
        $('#VoteNowUser').html('Voting Now..');
        var song_artist = document.getElementById("song_artist").value;
        var id = document.getElementById("id").value;
        var song_name = document.getElementById("song_name").value;
        var phonenumber = document.getElementById("phonenumber").value;
        // alert(phonenumber);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: "../api/VoteNowNumber",
            data: {
                'song_artist': song_artist,
                'id': id,
                'song_name': song_name,
                'phonenumber': phonenumber,
            },
            dataType: 'json',
            success: function(response) {
                JSON.stringify(response);
                // console.log(response);
                jQuery('#messagestatus').show();
                document.getElementById("Voting_response").style.display = "block";
                $('#successmodal').html('<p style="text-align:center;">' + response.message + '</p>');
                $('#successmessage').modal('show');

                document.getElementById("page-titles").style.display = "none";
                $(function(){
                    setInterval(oneSecondFunction, 4000);
                });
                function oneSecondFunction() {
                    $.ajax
                    ({
                        type: "POST",
                        url: "../api/CallBackMomo",
                        data: {
                            transactionid:response.transactionid,
                        },
                        cache: false,
                        success: function (data) {
                            $('#messagestatus').html('<p style="text-align:center;">' + data.message + '</p>');
                            if(data.status == "SUCCESSFUL"){
                                document.getElementById("loading").style.display = "none";
                                document.getElementById("successmodal").style.display = "none";
                                setTimeout(function() {
                                    window.location = 'Thankyou';
                                }, 2000);

                            }else{
                                // jQuery('#messagestatus').show();
                                document.getElementById("messagestatus").style.display = "block";
                                document.getElementById("loading").style.display = "block";
                            }

                        },
                        error: function(xhr, status, error) {
                            console.log(xhr.responseText);
                        }
                    });
                }
            },
            error: function(xhr, status, error) {
                   console.log(xhr.responseText);
            }
        });
    });
    $(document).ready(function () {

        /*Transaction Loading*/


    });
</script>
@endforeach

@include('layouts.newfooter')
@endsection
