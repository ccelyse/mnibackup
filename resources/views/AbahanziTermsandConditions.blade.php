@extends('layouts.newmaster')

@section('title', 'Amabwiriza ku mikoreshereze ku bahanzi')

@section('content')
<style>
    .spb-asset-content p{
        color:#fff !important;
    }
    .title-wrap h3{
        color: #fff !important;
    }
    .chart-page .chart-detail-header__chart-name {
        padding-top: 50px !important;
    }
    .modal-backdrop{
        position: relative !important;
    }
    /*.modal-backdrop.show {*/
        /*opacity: .5;*/
    /*}*/

    .modal-dialog {
        max-width: 800px !important;
        margin: 30px auto;
    }
    iframe{
        width: 100% !important;
    }

    .single-blog-post a,.single-blog-post p{
        color: #fff;
    }
    .bottom-title{
        font-size: 24px;
        line-height: 34px;
        color: #fff;
        padding: 45px 0 35px;
    }
    .title-box{
        display: inline-block;
        line-height: 35px;
        font-size: 13px;
        text-transform: uppercase;
        color: #fff;
        padding: 0 30px;
        border: 1px solid rgba(255,255,255,0.2);
        border-radius: 3px;
    }
    .alert-success {
        color: #155724;
        background-color: #d4edda;
        border-color: #c3e6cb;
        width: 100%;
        text-align: center;
        margin: 15px;
    }
    .theme-title-three p {
        font-size: 17px !important;
        line-height: 35px !important;
        color: #000;
        padding-top: 10px !important;
    }
    .theme-title-three .title {
        font-size: 30px !important;
        font-weight: bold;
    }
    .theme-list-item li {
        color: #000;
    }
.page-title{
    color: #fff !important;
}
</style>
@include('layouts.newArtisttopmenu')

<div class="solid-inner-banner">
    <div class="bg-shape-holder">
        <span class="big-round-one wow fadeInLeft animated" data-wow-duration="3s"></span>
        <span class="big-round-two wow fadeInLeft animated" data-wow-duration="3s"></span>
        <span class="big-round-three wow fadeInLeft animated" data-wow-duration="3s"></span>
        <span class="shape-one"></span>
        <span class="shape-two"></span>
        <img src="front/images/shape/shape-32.svg" alt="" class="shape-three">
        <span class="shape-four"></span>
    </div>
    <h2 class="page-title">Amabwiriza ku mikoreshereze ku bahanzi</h2>
</div> <!-- /.solid-inner-banner -->


<!--
    =============================================
        About Us Standard
    ==============================================
    -->
<div class="about-us-standard">
    <div class="container">
        <div class="theme-title-three mb-170">
            <h2 class="title">Byavuguruwe  01/01/2020</h2>
            <p>Murakaza neza kurubuga rw’amabwiriza ku mikoreshereze y’urubuga rwa MNI yagenewe abahanzi. Mbere yo gusabonura birambuye, Muzirikane ko aya mabwiriza ari ngenzi kuri twe kubera ko ari ingenzi kuri mwe abahanzi.</p>

            <h2 class="title">Impamvu yaya mabwiriza ku mikoreshereze:</h2>
            <ul class="theme-list-item pb-70">
                <li><i class="fa fa-chevron-right" aria-hidden="true"></i>Dutewe ishema kandi twubaha ibyo mukora. </li>
                <li><i class="fa fa-chevron-right" aria-hidden="true"></i>Tuzi agaciro k’abakunzi ba muzika mukora. </li>
                <li><i class="fa fa-chevron-right" aria-hidden="true"></i>Tuzi icyo bimaze kumenyekana kw’ibikorwa byanyu </li>
                <li><i class="fa fa-chevron-right" aria-hidden="true"></i>Dufite ishema mu kugira uruhare mu iterambere ryanyu</li>

            </ul>

            <h2 class="title">Aya mabwiriza ku mikoreshereze areba abahanzi ni ngombwa kubera izi mpamvu zikurikira:</h2>
            <ul class="theme-list-item pb-70">
                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i>Asobanura uburenganzira bwawe iyo ukoresheje servise ya MNI</li>
                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i>burenganzira uduha iyo ikoresheje servise za MNI yagenewe abahanzi.</li>
                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i>Asobanura amategeko n’amabwiriza</li>

            </ul>
            <p>Turabasaba gusoma mwitonze mukumva neza ikintu cyose kiri mu bikubiye muri aya mabwiriza ku mikoreshereze ndetse n’amabwiriza ku kutamena amabanga kandi turizera ko mukomeza kunogerwa na muzika Nyarwanda.</p>

            <h2 class="title">Intangiriro</h2>
            <p>Murakoze guhitamo MNI Ltd, byumwihariko serivise ya MNI yagenewe abahanzi (“MNI y’Abahanzi”), uruhande rumwe ni Urubuga rwa MNI (www.mni.rw) na gahunda ya USSD (*611#) nizindi progaramu tuzakoresha ubu cyangwa mugihe kizaza (“hamwe,ziswe serivise za MNI”) zitangwa na MNI (Muzika Nyarwanda Ipande) Ltd.</p>
            <p>MNI Ltd ni kompanyi yigenga yahawe uburenganzira butangwa n’ Ikigo cy’ igihugu gishinzwe iterambere (RDB) bwo gukorera imirimo ibyara inyungu mu Rwanda, ikaba yarahawe icyemezo cya kompanyi (TIN) no:108135642. Ifite icyemezo gitangwa n’Urwego Ngenzuramikorere mu Rwanda (RURA) cyo gukoresha umurongo mugufi (short code ya *611# ) gifite no: RURA/SC/USSD/L4359, n’icyemezo cy’ubufatanye n’Urugaga rw’Abanyamuziki mu Rwanda (RMF- Rwanda Music Federation) cyatanzwe kuwa 22/07/2019 ndetse n’ icyemezo gitangwa n’Inteko Nyarwanda y’Ururimi n’Umuco (RALC) gifite no: 280/RALC/2019.</p>
            <p>MNI y’Abahanzi ni umuronko ufasha umuhanzi kwihuza n’abakunzi ba muzika be, kubyaza inyungu igihangano cyawe, kwamamara ku igihangano cye no kuririmba muri MNI Tarama</p>
            <p>Umuhanzi (“Nyir’igihangano”) kurundi ruhande yemera gufungura konti, gushyira indirimbo ku rubuga rwa MNI kugirango itorwe  cyangwa gukoresha indi serivisi ya MNI y’Abahanzi yaba kurubuga rwa MNI, USSD nuwundi muronko wa MNI cyangwa ugakoresha andi makuru cyangwa ibindi bintu ibyaribyo byose byashyizwe hanze na MNI, bivuze ko ugiranye amasezerano y’imikoranire na MNI Ltd. Gukoresha Servise ya MNI y’Abahanzi bivuze ko wemeranya n’amtegeko ku mikoreshereze aboneka hano n’amabwriza ku <a href="{{'PrivacyPolicy'}}">kutamena amabanga aboneka hano</a></p>
            <p>Mugihe habayeho kutumvikana ku mabwiriza ku mikoreshereze ya MNI n’amabwiriza ku mikoreshereze y’abahanzi, ibikubiye mu mabwiriza ku mikoreshereze y’abahanzi bizakurikizwa.</p>
            <p>Kwinjira muri aya masezerano bivuze ko uri umuhanzi Nyarwanda kandi wujuje imyaka y’ubukure (+18) waba uri munsi yiyo myaka ukaba ufite uguhagarariye (Umubyeyi, ukurera cyangwa ureberera inyungu zawe).</p>

            <h2 class="title">Impinduka  muri aya masezerano</h2>
            <p>Amatora  akorwa muburyo bubiri: Hari ugukoresha USSD(*611#)  kuri telephone yawe ugakurikiza amabwiriza cyangwa ukajya kugikoresho gikenera interineti(Mudasobwa,Telephone cg Tablet) ukajya kurubuga rwa mni.rw ubundi uhasanga urutonde rw’indirimbo,ugahitamo iyo wakunze ukayitora.  uko utoye wishyura 50Rwf ava kuri Mobile Money ahabwa nyirigihangano watoye.</p>
            <p>Ntayandi mafaranga yishyuzwa ukoresha system za MNI Ltd; hagize uwagira ikindi akwishyuza yaba umukozi cyangwa undi muntu uwari wese usabwe kubitugezaho cyangwa ukitabaza inzego z’umutekano kuko bifatwa nkubujura.</p>
            <p>Gukoresha urubuga rwa MNI Ltd arirwo mni.rw bikenera kuba uri kuri internet bivuze ko  mugihe ugiye kurukoresha bitwara amafaranga ya interineti mugihe ukoresha cellular network cyangwa ntibigire amafaranga bigutwara mugihe uri kuri wi-fi.</p>
            <p>Gukoresha USSD(*611#) bigusaba kuba ufite amafaranga atantu(5 Rwf) ubundi ugakomeza n’itora twavuze haruguru.</p>


            <h2 class="title">Uko amatora akorwa  n’amafaranga yishyuzwa  </h2>
            <p>Amatora  akorwa muburyo bubiri: Hari ugukoresha USSD (*611#)  kuri telephone yawe ugakurikiza amabwiriza cyangwa ukajya kugikoresho gikenera murandasi (Mudasobwa,Telephone cg Tablet) ukajya kurubuga rwa www.mni.rw ubundi uhasanga urutonde rw’indirimbo, ugahitamo iyo wakunze ukayitora.  Uko umukunzi atoye yishyura 50 Rwf ava kuri Mobile Money cyangwa ku ikarita ya banki.</p>
            <p>Gukoresha urubuga rwa MNI Ltd arirwo www.mni.rw bikenera kuba uri kuri murandasi bivuze ko mugihe ugiye kurukoresha ukoresheje cellular network (bundle) bitwara amafaranga ya murandasi cyangwa ntibigire amafaranga bigutwara mugihe uri kuri wi-fi.</p>
            <p>Gukoresha USSD (*611#) bigusaba kuba ufite amafaranga atantu (5 Rwf) ubundi ugakomeza n’itora twavuze haruguru.</p>


            <h2 class="title">Uburenganzira duha Umuhanzi</h2>
            <p>MNI iguha uburenganzira bwo gufungura konti kubuntu, kuyikoresha mugushyiraho igihangano, kugisiba cyangwa kucyamamaza no kureba uko winjiza amafaranga ariko munyungu zo kukibyaza inyungu ziri mumuronko wa MNI y’Abahanzi.</p>

            <h2 class="title">Izindi porogaramu zabandi bantu</h2>
            <p>Murwego rwo kunoza akazi no kugirango hubaharizwe imikorere inoze ya MNI. MNI ikoresha izindi porogaramu zabandi bantu gusa tukaba tubasaba gusoma amabwiriza agenga imikoreshereze ndetse n’amabwiriza ku kutamena  amabanga  yabo ukareba ko wemeranya nabo. Mugihe hari ibyo utemeranyije nabo MNI ntabwo izabibazwa.</p>

            <h2 class="title">Ninde ufatwa nk’umuhanzi wa muzika kuri MNI Ltd</h2>
            <p>Umuhanzi ni umunyarwanda wese ufite indirimbo (nibura imwe) yasohoye mu buryo bwemewe kandi akaba ayifiteho uburenganzira bwo kuyibyaza inyungu mu buryo busesuye.</p>

            <h2 class="title">Amakuru tugusaba n’imikoreshereze yayo</h2>
           <p>MNI Ltd ikenera aya makuru akurikira murwego rwo kuguha gahunda yagenewe abahanzi:</p>
            <ul class="theme-list-item pb-70">
                <li><i class="fa fa-chevron-right" aria-hidden="true"></i>myirondoro yawe bwite</li>
                <li><i class="fa fa-chevron-right" aria-hidden="true"></i>Uburyo bwo kwishyurwa</li>
                <li><i class="fa fa-chevron-right" aria-hidden="true"></i>Imbuga nkoranyambaga ukoresha</li>
                <li><i class="fa fa-chevron-right" aria-hidden="true"></i>Ifoto ikuranga</li>

            </ul>
            <p>Amakuru tugusaba akoreshwa mu gufungura no kwemeza  konti yawe kurubuga rwa  MNI, akadufasha mu kwishyura abahanzi ndetse no ku kumenyesha amakuru mu gihe bibaye ngombwa. Amazina ya Gihanzi, imbuga nkoranyambaga zawe watanze ndetse n’amakuru kugihangano watanze  niyo abasha kugaragazwa ku rubuga rusange mu gihe amakuru ajyanye n’amazina bwite yawe, amakuru kubwishyu, email, nimero ya telephone, ID/Pass no bibikwa na MNI mu rwego rwo kukumenya ndetse no gukora ubwishyu kandi rubanda ntago rubona ayo makuru.</p>

            <h2 class="title">Gufungura konti kurubuga rwa MNI</h2>
            <ul class="theme-list-item pb-70">
                <li><i class="fa fa-chevron-right" aria-hidden="true"></i>Umwirondoro bwite w’umuhanzi cyangwa uhagarariye nyir’igihangano</li>
                <li><i class="fa fa-chevron-right" aria-hidden="true"></i>Amakuru y’uburyo bwo kwishyurwa</li>
                <li><i class="fa fa-chevron-right" aria-hidden="true"></i>Kwemera amategeko n’amabwiriza ya MNI Ltd ajyanye n’imikoreshereze y’igihangano cy’umuhanzi</li>
                <li><i class="fa fa-chevron-right" aria-hidden="true"></i>Konti yemewe ni iyamaze gusabwa ikemezwa na MNI nyuma y’isuzuma ry’uko nyirayo ariwe nyakuri.</li>

            </ul>


            <h2 class="title">Ibisabwa gushyira igihangano kurubuga rwa MNI</h2>

            <p>Umuhanzi asabwa amazina y’igihangano yagihaye, umuronko waho igihangano gikurwa (“YouTube link”), Ifoto yamamaza/iranga igihangano (“cover picture”) kandi akaba afite uburenganzira bwo kubyaza inyungu icyo gihangano. Ahabwa umwanya wo kugaragaza igihangano cye k’urutonde aho abakunzi bigihangano cye bagitora.</p>
            <p>ashobora kongera gusubizaho n’ubundi mugihe kingana n’ukwezi gusa. Nukuvuga  ko igihangano cyemerewe gutangwa inshuro ebyeri mugihe cy’amezi abiri gusa.</p>

            <h2 class="title">Imikoreshereze y’ibihangano</h2>
            <p>Umuhanzi wamaze kwemezwa na MNI Ltd, ashobora kugaragaza igihangano cye yifuza kugirango gitorwe mugikorwa cya MNI Selection ariko akaba yemera ko agifiteho uburanganzira bwo kibyaza inyungu.</p>
            <p>Mugihe bigaragaye ko wakoresheje igihangano udafiteho uburenganzira, MNI Ltd ntibazwa ingaruka  izarizo zose zava mugukoresha igihangano muburyo bunyaranyije n’amategeko ahubwo bibazwa uwagikoresheje kugiti cye.</p>

            <h2 class="title">Inyungu zo gushyira igihangano kuri MNI Selection.</h2>
            <p>Igihangano gishyizwe ku rubuga rwa MNI kibyazwa inyungu binyuze mu itora rikorerwa kurubuga rwa MNI cyangwa mu buryo bwa USSD (*611#). Itora riri ku mafaranga mirongo itanu (50 Rwf) rihwanye n’ijwi rimwe, umuhanzi akakira 60% by’amafaranga yose nyuma y’imisoro ya RRA ndetse n’ amafaranga y’ishyurwa ibigo by’itumanaho. Nukuvugako nyir’igihangano ahabwa angana na (25 Rwf) kuri buri tora. Indirimbo zahiga izindi zigira imyanya y’imbere k’urutonde rw’indirimbo zikunzwe, zihabwa ibihembo ndetse banyir’ibihangano bagataramana n’abakunzi babo mu gitaramo cya MNI Tarama.</p>

            <h2 class="title">Ibihembo bitangwa na MNI </h2>
            <p>MNI itanga ibihembo buri gihembwe cy’umwaka kingana n’amezi atatu kundirimbo zahize izindi.</p>
            <h4>Ibihembo bitangwa mu buryo bukurikira;</h4>

            <ul class="theme-list-item">
                <li><i class="fa fa-chevron-right" aria-hidden="true"></i>Indirimbo ya mbere: Miliyoni ebyiri (2,000,000 RWF)</li>
                <li><i class="fa fa-chevron-right" aria-hidden="true"></i>Indirimbo ya kabiri: Ibihumbi magana rindwi (700,000 RWF)</li>
                <li><i class="fa fa-chevron-right" aria-hidden="true"></i>Indirimbo ya gatatu: Ibihumbi magana tatu (300,000 RWF)</li>

            </ul>
            <p style="color: #ff6363 !important;">N.B: Amafaranga yagaragajwe aba abariyemo umusoro wa Withholding bivuze ko uyahabwa uwo musoro wabanje gukurwamo ukishyurwa RRA.</p>


            <h2 class="title">Umutekano w’amafaranga, uburyo n’igihe cyo kwishyurwa.</h2>
            <p> Amafaranga yishyurwa n’abakunzi bamuzika yakirwa kuri konti ya banki MNI ihuriyeho na Rwanda Music Federation (RMF) nk’urwego rurengera inyungu z’abahanzi. Amafaranga akurwaho gusa iyo byagizwemo uruhare n’impande zombi akoherezwe kuri ba nyirayo.</p>
            <p>Nyuma y’ukwezi indirimbo imaze muri MNI Selection, nyir’igihangano yishyurwa bitarenze ibyumweru bibiri. Iyo hagize imbogamizi ituma icyo gihe kitubahirizwa, nyir’igihangano amenyshwa impamvu mu ibaruwa yanditse n’igihe azishyurwa.</p>


            {{--<h2 class="title">Igihe igihangano kimara kuri MNI Selection kibyazwa amafaranga</h2>--}}
            {{--<p>Igihangano cyemerewe kubyazwa inyungu igihe cy’ukwezi gishobora kongerwa mugihe cy’ukwezi kumwe mugihe nyir’igihangano abyifuje.</p>--}}

            <h2 class="title">Uburenganzira kugihangano mu mategeko</h2>
            <p>Umuhanzi ushyira igihangano kuri MNI, aba yemera ko abifitiye uburenganzira busesuye bwo kukibyaza inyungu. Mugihe bigaragaye ko yakoresheje igihangano muburyo butemewe akurikiranwa n’inzego zibifitiye ububasha kandi MNI Ltd ntiyirengera ingaruka zituruka kumakosa yakorwa n’uwiyitiriye cyangwa uwakoresheje igihangano mu buryo bunyuranyije n’amategeko.Umusozo.</p>

            <h2 class="title">Igihe igihangano kimara kuri MNI Selection</h2>
            <p>Igihangano cyagaragajwe kuri MNI Selection, kihava iyo igihembwe kirangiye (Urugero: Igihembwe gitangiye mu kwezi kwa mbere kirangira mu kwezi kwa gatutu. Bivuze ko indirimbo yashyizweho muri icyo gihe guhera mu kwezi kwa kane izashyirwa kurutonde rw’izagaragajwe mugihe gishize ( Archive) ariko itabasha gutorwa)  Gusa umuhanzi ubwe ashobora kugikuraho icyo gihe kitarangiye  cyangwa igihemwe cyaba cyarangiye akaba ashobora kongera kukigaragaza kikongera gutorwa.</p>

            <h2 class="title">Umusozo</h2>
            <p>Aya mabwiriza niyo tugenderaho, hagize impinduka zibaho zigomba kumenyeshwa buri ruhande kandi buri wese asabwe gusura urubuga rwa MNI Ltd kugira amenye ibyaba byarahindutse.</p>
            <p>Hari igitekerezo cyangwa ikibazo kijyanye naya mabwiriza cyangwa ikindi cyose cyadufasha kugera kuntego zacu watwandikira kuri email:info@mni.rw cyangwa ukaduhamagara kuri Tel:250 780 008 883</p>
        </div> <!-- /.theme-title-three -->
    </div> <!-- /.container -->
</div> <!-- /.about-us-standard -->



@include('layouts.newArtistfooter')
@endsection
