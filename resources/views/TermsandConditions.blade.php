@extends('layouts.newmaster')

@section('title', 'Amabwiriza ku mikoreshereze')

@section('content')
    <style>
        .spb-asset-content p{
            color:#fff !important;
        }
        .title-wrap h3{
            color: #fff !important;
        }
        .chart-page .chart-detail-header__chart-name {
            padding-top: 50px !important;
        }
        .modal-backdrop{
            position: relative !important;
        }
        /*.modal-backdrop.show {*/
        /*opacity: .5;*/
        /*}*/

        .modal-dialog {
            max-width: 800px !important;
            margin: 30px auto;
        }
        iframe{
            width: 100% !important;
        }

        .single-blog-post a,.single-blog-post p{
            color: #fff;
        }
        .bottom-title{
            font-size: 24px;
            line-height: 34px;
            color: #fff;
            padding: 45px 0 35px;
        }
        .title-box{
            display: inline-block;
            line-height: 35px;
            font-size: 13px;
            text-transform: uppercase;
            color: #fff;
            padding: 0 30px;
            border: 1px solid rgba(255,255,255,0.2);
            border-radius: 3px;
        }
        .alert-success {
            color: #155724;
            background-color: #d4edda;
            border-color: #c3e6cb;
            width: 100%;
            text-align: center;
            margin: 15px;
        }
        .theme-title-three p {
            font-size: 17px !important;
            line-height: 35px !important;
            color: #000;
            padding-top: 10px !important;
        }
        .theme-title-three .title {
            font-size: 30px !important;
            font-weight: bold;
        }
        .theme-list-item li {
            color: #000;
        }
        .page-title{
            color: #fff !important;
        }
    </style>
    @include('layouts.newtopmenu')

    <div class="solid-inner-banner">
        <div class="bg-shape-holder">
            <span class="big-round-one wow fadeInLeft animated" data-wow-duration="3s"></span>
            <span class="big-round-two wow fadeInLeft animated" data-wow-duration="3s"></span>
            <span class="big-round-three wow fadeInLeft animated" data-wow-duration="3s"></span>
            <span class="shape-one"></span>
            <span class="shape-two"></span>
            <img src="front/images/shape/shape-32.svg" alt="" class="shape-three">
            <span class="shape-four"></span>
        </div>
        <h2 class="page-title">AMATEGEKO N’AMABWIRIZA AGENGA MNI LTD</h2>
    </div> <!-- /.solid-inner-banner -->


    <!--
        =============================================
            About Us Standard
        ==============================================
        -->
    <div class="about-us-standard">
        <div class="container">
            <div class="theme-title-three mb-170">
                <h2 class="title">Bishyizwe mungiro guhera 01/09/2019</h2>
                <p>Ikaze kuri MNI (Muzika Nyarwanda Ipande) Ltd. MNI Ltd ni kompanyi yigenga yahawe uburenganzira butangwa n’ Ikigo cy’ igihugu gishinzwe iterambere (RDB) bwo gukorera imirimo ibyara inyungu mu Rwanda, ikaba yarahawe icyemezo cya kompanyi (TIN) no:108135642. Ifite icyemezo gitangwa n’Urwego Ngenzuramikorere mu Rwanda (RURA) cyo gukoresha umurongo mugufi (short code ya *611# ) gifite no: RURA/SC/USSD/L4359, n’icyemezo cy’ubufatanye n’Urugaga rw’Abanyamuziki mu Rwanda (RMF- Rwanda Music Federation) cyatanzwe kuwa 22/07/2019 ndetse n’ icyemezo gitangwa n’Inteko Nyarwanda y’Ururimi n’Umuco (RALC) gifite no: 280/RALC/2019.</p>
                <p>Utora (“Umukunzi wa muzika”) ni umuntu wese ukoresha uburyo bwa MNI zaba izikenera murandasi cyangwa izitayikenera akishyura amafaranga mirongo itanu (50 Frw) ashyigikira nyir’ igihangano. Gukoresha gahunda ya MNI Ltd yaba kurubuga rwa MNI, USSD n’uwundi murongo wa MNI cyangwa ukabona andi makuru cyangwa ibindi bintu ibyaribyo byose byashyizwe hanze na MNI, bivuze ko ugiranye amasezerano y’imikoranire na MNI. Gukoresha imikorere ya MNI bivuze ko unemeranya n’amabwiriza ku kutamena amabanga (Privacy policy) <a href="{{'PrivacyPolicy'}}">aboneka hano</a></p>

                <h2 class="title">Impinduka</h2>

                <p>Nubwo ari inshingano zacu kubamenyesha ibyahindutse muri aya mabwiriza, ni ngombwa ko usura urubuga rwacu nibura buri kwezi mu rwego rwo kumenya ibyahindutse mugihe imenyesha ryacu ritakugezeho.</p>

                <h2 class="title">Amakuru yakwa umukunzi wa muzika n’umutekano wayo</h2>

                <p>MNI Ltd yakira  nimero ya telephone  yakoreshejwe  mu gutora ariko igirwa ibanga kuko ku rubuga rwa MNI Ltd ntihagaragara kandi ntihagaragazwa indirimbo watoye ahubwo hagarazwa amajwi igihangano kigira, kandi amakuru  yakiriwe abikwa mu Rwanda</p>
                <p>Nimero za telephone zakiriwe zishobora gukoreshwa mu kukumenyesha ibikorwa bya MNI bitandukanye, gusuzuma uko amatora agenda n’ubundi bushashakashatsi bwose MNI yaba yifuza gukora kugirango inoze serivisi zayo.</p>
                <p>Kubakoresha amakarita y’banki mukwishyura ntabwo tubika amakuru yayo ahubwo twakira igikorwa cyemeza ko wishyuye. Amakuru yamakarita akomeza gucungwa n’ibigo bishinzwe kuyacunga.</p>
                <p>MNI ikora ibishoboka mukubika neza mu mutekano usesuye amakuru yawe mu ibanga kandi ntiyemerewe gusangirwa n’abandi.</p>

                <h2 class="title">Uko amatora akorwa  n’amafaranga yishyuzwa</h2>
                <p>Amatora akorwa muburyo bubiri: Hari ugukoresha USSD (*611#) kuri telephone yawe ugakurikiza amabwiriza cyangwa ukajya kugikoresho gikenera murandasi (Mudasobwa, Telephone cg Tablet) ukajya kurubuga rwa www.mni.rw ubundi uhasanga urutonde rw’indirimbo, ugahitamo iyo wakunze ukayitora. Uko utoye wishyura 50 Rwf ava kuri Mobile Money azajya ashyigikira indirimbo watoye.</p>
                <p>Ntayandi mafaranga yishyuzwa ukoresha uburyo bwa MNI Ltd; hagize uwagira ikindi akwishyuza yaba umukozi cyangwa undi muntu uwari wese usabwe kubitugezaho cyangwa ukitabaza inzego z’umutekano kuko bifatwa nkubujura.</p>
                <p>Gukoresha urubuga rwa MNI Ltd arirwo www.mni.rw bikenera kuba uri kuri murandasi bivuze ko mugihe ugiye kurukoresha ukoresheje cellular network (bundle) bitwara amafaranga ya murandasi cyangwa ntibigire amafaranga bigutwara mugihe uri kuri wi-fi.</p>
                <p>Gukoresha USSD (*611#) bigusaba kuba ufite amafaranga atantu (5 Rwf)  ya airtime ubundi ugakomeza n’itora twavuze haruguru.</p>

                <h2 class="title">Ubwisanzure bw’ukoresha uburyo bwa MNI Ltd</h2>
                <p>Umuntu wese yemerewe gukoresha serivisi zacu igihe yumva abishaka kandi akishyura yamafaranga yavuzwe haruguru igihe abishaka, mugihe yumva atiteguye kuyatanga afite uburenganzira bwo kubihagarika. Ntabwo MNI Ltd ifite system z’ifatabuguzi aho wishyura mugihe cyangwa mugihe utakoresheje imikorere yishyuzwa, ahubwo wishyura mugihe wumva ukeneye gukora imikorere yavuzwe haruguru. Ntamwirondoro wumuntu runaka (Umukunzi wa muzika) cyangwa isebanya cyangwa isesereza iryo ari ryose ryemewe.</p>

                <h2 class="title">Uburyo ibihangano bizakoreshwa</h2>
                <p>Ibihangano byatanzwe kurubuga rwa MNI muri uko kwezi, bizajya bikoreshwa muri gahunda y’ amatora. Bishobora kandi kwamamazwa haba  mubitangazamakuru cyangwa ubundi buryo bushoboka bizajya binyuzwamo mu rwego rwo gushishikariza abantu kubishyigikira mu irushanwa, kubimenyakanisha (promotion) muburyo butandukanye, mumajwi cyangwa ubwa mashusho. Kandi bikanagaragazwa kuri system zacu zose kunyungu yo kugira bibone ababishyigikira no kubimenyakanisha.</p>
                <p>Ntabwo ibihangano bizakoreshwa muzindi nyungu zitari izavuzwe haruguru, ariko hagize igihinduka muri aya mabwiriza, tuzabamenyesha.</p>


                <h2 class="title">Ninde ufatwa nku muhanzi wa muzika kuri MNI Ltd</h2>
                <p>Umuhanzi n’umuntu wese ufite indirimbo (nibura imwe) yasohoye mu buryo bwemewe kandi akaba ayifiteho uburenganzira bwo kuyibyaza inyungu mu buryo busesuye.</p>

                <h2 class="title">Ibahangano byemewe.</h2>
                <p>Nubwo MNI Ltd ifite intego zo kugira uruhare mu iterambere ry’amuzika kandi mubwisanzure, ariko hari ibihangano bitazemererwa kuba mu irushanwa mu gihe byagaragazwa n’inzego zibishinzwe ko byaciwe.</p>

                <h2 class="title">Izindi porogaramu zabandi bantu</h2>
                <p>Murwego rwo kunoza akazi no kugirango hubaharizwe imikorere inoze ya MNI. MNI ikoresha izindi porogaramu zabandi bantu gusa tukaba tubasaba gusoma amabwiriza agenga imikoreshereze ndetse n’amabwiriza ku kutamena  amabanga  yabo ukareba ko wemeranya nabo. Mugihe hari ibyo utemeranyije nabo MNI ntabwo izabibazwa.</p>

                <h2 class="title">Umusozo</h2>
                <p>Aya mabwiriza niyo tugenderaho, hagize impinduka zibaho, zigomba kumenyeshwa buri ruhande kandi buri wese asabwe gusura urubuga rwa MNI Ltd kugira ngo amenye ibyaba byarahindutse.</p>
                <p>Hari igitekerezo cyangwa ikibazo cyaba ikijyanye naya mabwiriza cyangwa ikindi icyo ari cyose cyadufasha kugera kuntego zacu watwandikira kuri email:info@mni.rw cyangwa ukaduhamagara kuri Tel:250 780 008 883</p>
            </div> <!-- /.theme-title-three -->
        </div> <!-- /.container -->
    </div> <!-- /.about-us-standard -->



    @include('layouts.newfooter')
@endsection
