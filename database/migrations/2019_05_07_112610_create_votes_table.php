<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('starting_date');
            $table->string('ending_date');
            $table->string('voterphonenumber');
            $table->string('vote');
            $table->string('voter_artist');
            $table->string('voter_artist_song');
            $table->string('voter_status');
            $table->unsignedInteger('song_id');
            $table->foreign('song_id')->references('id')->on('songinfo')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votes');
    }
}
