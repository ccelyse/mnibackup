<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaylistinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('playlistinfo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('starting_date');
            $table->string('ending_date');
            $table->string('playlist_name');
            $table->string('playlist_cate');
            $table->string('playlist_payment_status');
            $table->string('playlist_code');
            $table->string('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('playlistinfo');
    }
}
