<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artist_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('names');
            $table->string('artistic_names');
            $table->string('artist_number');
            $table->string('artist_email')->unique();
            $table->string('artist_ID');
            $table->string('artist_modeofpayment')->nullable();
            $table->string('artist_mobilemoney_number')->nullable();
            $table->string('artist_mobilemoney_names')->nullable();
            $table->string('artist_bankaccount_number')->nullable();
            $table->string('artist_bankaccount_names')->nullable();
            $table->string('artist_bank_name')->nullable();
            $table->string('artist_facebook')->nullable();
            $table->string('artist_instagram')->nullable();
            $table->string('artist_twitter')->nullable();
            $table->string('artist_youtube')->nullable();
            $table->string('artist_account_status');
            $table->longText('artist_description')->nullable();
            $table->string('artist_category')->nullable();
            $table->string('picturename');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artist_accounts');
    }
}
