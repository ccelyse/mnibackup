<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMomotransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('momotransaction', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone');
            $table->string('voter_id');
            $table->string('transactionid');
            $table->string('status');
            $table->string('assignedid');
            $table->string('company_name');
            $table->string('code');
            $table->string('amount');
            $table->string('artist_name');
            $table->string('artist_song');
            $table->string('payment_code');
            $table->string('external_payment_code');
            $table->string('payment_status');
            $table->string('payment_type');
            $table->string('callback_url');
            $table->string('momodeleted_at');
            $table->string('momocreated_at');
            $table->string('momoupdated_at');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('momotransaction');
    }
}
