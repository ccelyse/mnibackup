<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSonginfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('songinfo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('starting_date');
            $table->string('ending_date');
            $table->string('playlist_name');
            $table->string('playlist_cate');
            $table->string('playlist_payment_status');
            $table->string('playlist_code');
            $table->string('song_artist');
            $table->string('song_name');
            $table->longText('song_youtube_link');
            $table->string('song_number');
            $table->string('song_cover_picture');
            $table->string('user_id');
            $table->string('archives_status');
            $table->string('promotion_status')->nullable();
            $table->string('promotion_period')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('songinfo');
    }
}
